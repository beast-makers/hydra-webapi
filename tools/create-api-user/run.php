#!/usr/bin/php
<?php
declare(strict_types=1);

use BeastMakers\SecurityApi\Acl\Action\Cli\CreateApiUser;

require_once __DIR__ . '/../../src/_application/bootstrap.php';

$createApiUserAction = new CreateApiUser();
$createApiUserAction->run();
