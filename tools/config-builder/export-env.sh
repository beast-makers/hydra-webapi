#!/usr/bin/env bash
set -euo pipefail

CONSUL_DB_MASTER_HOST='postgresql.service.consul'
CONSUL_DB_MASTER_PORT=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/postgresql" | jq -r .[0].ServicePort)
export CONSUL_DB_MASTER_HOST=$CONSUL_DB_MASTER_HOST
export CONSUL_DB_MASTER_PORT=$CONSUL_DB_MASTER_PORT

CONSUL_REDIS_MASTER_HOST=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/webapi-redis" | jq -r .[0].Address)
CONSUL_REDIS_MASTER_PORT=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/webapi-redis" | jq -r .[0].ServicePort)
export CONSUL_REDIS_MASTER_HOST=$CONSUL_REDIS_MASTER_HOST
export CONSUL_REDIS_MASTER_PORT=$CONSUL_REDIS_MASTER_PORT
