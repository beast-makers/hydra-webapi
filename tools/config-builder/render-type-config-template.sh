#!/usr/bin/env bash
set -euo pipefail

source includes/require-app-type.sh

printf "Building [${APP_TYPE}] config\n"

INPUT_TEMPLATE_FILE="/var/webapi/config/env/${APP_TYPE}/config.tmpl.json"
OUTPUT_CONFIG_FILE="/var/webapi/config/_generated/config-type.json"

IS_ENCRYPTED=$(cat $INPUT_TEMPLATE_FILE | jq . | grep . || echo '1')
if [[ $IS_ENCRYPTED == '1' ]]; then
  CONTENT=$(cat $INPUT_TEMPLATE_FILE | git-crypt smudge --key-file=/tmp/repo-key | jq .)
else
  CONTENT=$(cat $INPUT_TEMPLATE_FILE)
fi

printf "$CONTENT" | envsubst > ${OUTPUT_CONFIG_FILE}
