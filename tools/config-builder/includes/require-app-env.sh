#!/usr/bin/env bash
set -euo pipefail

APP_ENV=${APP_ENV:-}

if [[ -z "${APP_ENV}" ]]; then
  printf "APP_ENV environment variable is not set. Examples [dev|prod|qa01]\n\n"
  exit 1
fi
