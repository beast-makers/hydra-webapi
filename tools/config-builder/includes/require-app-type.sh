#!/usr/bin/env bash
set -euo pipefail

APP_TYPE=${APP_TYPE:-}

if [[ "${APP_TYPE}" != @(testing|staging|production) ]]; then
  printf "APP_TYPE environment variable is required to be one of [testing|staging|production]\n\n"
  exit 1
fi
