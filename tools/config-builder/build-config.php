#!/usr/bin/php
<?php
define('PROJECT_DIR', '/var/webapi/');
define('TOOLS_DIR', PROJECT_DIR . 'tools/');
define('CONFIG_DIR', PROJECT_DIR . 'config/');

$mergedConfig = mergeConfigs();
renderFinalConfigFiles($mergedConfig);

/**
 * @return array
 */
function mergeConfigs(): array
{
  $defaultConfig = json_decode(file_get_contents(CONFIG_DIR . '_generated/config-default.json'), true);
  $modeConfig = json_decode(file_get_contents(CONFIG_DIR . '_generated/config-type.json'), true);
  $envConfig = json_decode(file_get_contents(CONFIG_DIR . "_generated/config-env.json"), true);

  return array_replace_recursive($defaultConfig, $modeConfig, $envConfig);
}

/**
 * @param array $config
 *
 * @return void
 */
function renderFinalConfigFiles(array $config): void
{
  $mergedConfigOutput = var_export($config, true);
  $mergedConfigFinalContent = <<<CONFIG
<?php
return {$mergedConfigOutput};
CONFIG;

  file_put_contents(CONFIG_DIR . '_generated/config.php', $mergedConfigFinalContent);
  file_put_contents(CONFIG_DIR . '_generated/config.json', json_encode($config));
}
