#!/usr/bin/env bash
set -euo pipefail

source includes/require-app-type.sh
. ./export-env.sh

curl -s --header "X-Vault-Token: ${VAULT_TMP_TOKEN}" ${VAULT_ADDR}/v1/secret/${APP_TYPE}/repo-key \
  | jq -r .data.key \
  | base64 --decode \
  | tee /tmp/repo-key > /dev/null

./render-default-config-template.sh
./render-type-config-template.sh
./render-env-config-template.sh
./build-config.php
