#!/bin/bash

sh -c "cd config-builder && ./run.sh"
sh -c "cd build-route-cache && ./run.sh"
sh -c "cd liquibase && ./run.php update"

printf "Create api user [admin] user";
sh -c "cd create-api-user && cat first_admin.json | APP_STORE=DE ./run.php"

printf "\n\n"

printf "Create api user [read-only] user";
sh -c "cd create-api-user && cat read-only_user.json | APP_STORE=DE ./run.php"
