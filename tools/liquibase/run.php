#!/usr/bin/php
<?php
declare(strict_types=1);

define('PROJECT_DIR', __DIR__ . '/../../');

require PROJECT_DIR . 'vendor/autoload.php';

use BeastMakers\Shared\Config\Config;

function runLiquibase(string $command, array $modules): void
{
  $config = loadConfiguration();
  $connectionConfig = $config->getConfig('database.webapi');

  $host = $connectionConfig->getString('host');
  $port = $connectionConfig->getString('port');
  $dbName = $connectionConfig->getString('database');
  $user = $connectionConfig->getString('user');
  $password = $connectionConfig->getString('password');

  $connectionPath = "{$host}:{$port}/{$dbName}";
  $changeLogPaths = readChangelogPaths($modules);
  verifyChangelogPaths($changeLogPaths);

  foreach ($changeLogPaths as $path) {
    echo coloredString("Run migration for [{$path}]:\n");
    echo "-----------------\n\n";

    system(<<<CMD
liquibase --driver=org.postgresql.Driver \
  --changeLogFile={$path} \
  --url="jdbc:postgresql://{$connectionPath}?connect_timeout=2" \
  --username={$user} \
  --password={$password} \
  --liquibaseSchemaName=public \
  {$command}
CMD
    , $exitCode);
  }
}

function loadConfiguration(): Config
{
  $configuration = include(PROJECT_DIR . 'config/_generated/config.php');
  return new Config($configuration);
}

function validateInput(array $argv): bool
{
  if (!isset($argv[1])) {
    return false;
  }

  return true;
}

function readChangelogPaths(array $modules): array
{
  $projectDir = PROJECT_DIR;
  $dirPathTemplate = $projectDir . 'src/%s/_database/changelog.xml';

  $changelogPaths = array_map(static function (string $moduleName) use ($dirPathTemplate){
    return sprintf($dirPathTemplate, $moduleName);
  }, $modules);

  return $changelogPaths;
}

/**
 * @param array $changelogPaths
 *
 * @return void
 * @throws Exception
 */
function verifyChangelogPaths(array $changelogPaths): void
{
  $invalidPaths = [];

  foreach ($changelogPaths as $path) {
    if (!file_exists($path)) {
      $invalidPaths[] = $path;
    }
  }

  if (!empty($invalidPaths)) {
    throw new Exception("Following changelog files do not exist:\n" . implode("\n", $invalidPaths) . "\n\n");
  }
}

function coloredString(string $input): string {
  return "\033[0;32m". $input . "\033[0m";
}

$modules = [
  'DatabaseInit',
  'DatabaseAuditLog',
  'SecurityApi',
  'Catalog',
  'Clinic',
];

if (validateInput($argv)) {
  $liquibaseCommand = $argv[1];
  runLiquibase($liquibaseCommand, $modules);
} else {
  echo "Validation error\n";
  echo "Possible command 'updateSQL' 'update'\n";
}
