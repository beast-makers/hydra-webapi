<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Shared\DatabaseConnector;

use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;
use BeastMakers\Shared\DatabaseConnector\QueryRunnerPool;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;

class PostgreSQLQueryRunnerTest extends \Codeception\Test\Unit
{
  private QueryRunner $queryRunner;

  protected function _before(): void
  {
    /** @var QueryRunnerPool $queryRunnerPool */
    $queryRunnerPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL);
    $this->queryRunner = $queryRunnerPool->get(QueryRunnerCatalog::WEBAPI);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testQueryExecution(): void
  {
    $result = $this->queryRunner->runQuery("SELECT 'some_value';", __FUNCTION__);
    $retrievedValue = pg_fetch_row($result)[0];

    $this->assertSame('some_value', $retrievedValue);
  }
}
