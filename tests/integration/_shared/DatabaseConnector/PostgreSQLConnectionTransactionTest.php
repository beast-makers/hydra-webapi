<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Shared\DatabaseConnector;

use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;
use BeastMakers\Shared\DatabaseConnector\QueryRunnerPool;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;

class PostgreSQLConnectionTransactionTest extends \Codeception\Test\Unit
{
  public const INITIAL_VALUE = 2;

  private QueryRunner $queryRunner;

  protected function _before(): void
  {
    /** @var QueryRunnerPool $queryRunnerPool */
    $queryRunnerPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL);
    $this->queryRunner = $queryRunnerPool->get(QueryRunnerCatalog::WEBAPI);
  }

  protected function _after(): void
  {
    $this->truncateTempTable();
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testTransactionCommit(): void
  {
    $conn = $this->queryRunner->connection();
    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->commitTransaction();

    $this->assertDatabaseValueEquals(4);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testNestedTransactionCommit(): void
  {
    $conn = $this->queryRunner->connection();
    $storedValue = 6;

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->startTransaction();
    $this->queryRunner->runQuery("UPDATE temp_transaction_test SET val=${storedValue};", __FUNCTION__);
    $conn->commitTransaction();
    $conn->commitTransaction();

    $this->assertDatabaseValueEquals($storedValue);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testTransactionRollback(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->rollbackTransaction();

    $this->assertDatabaseValueEquals(self::INITIAL_VALUE);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testNestedTransactionRollback(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=6;', __FUNCTION__);
    $conn->rollbackTransaction();
    $conn->rollbackTransaction();

    $this->assertDatabaseValueEquals(self::INITIAL_VALUE);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testRolledbackTransactionCommit(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=6;', __FUNCTION__);
    $conn->rollbackTransaction();
    $conn->commitTransaction();

    $this->assertDatabaseValueEquals(self::INITIAL_VALUE);
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testCommittedTransactionRollback(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
    $conn->startTransaction();
    $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=6;', __FUNCTION__);
    $conn->commitTransaction();
    $conn->rollbackTransaction();

    $this->assertDatabaseValueEquals(self::INITIAL_VALUE);
  }

  /**
   * @return void
   * @throws DatabaseException
   * @throws \Throwable
   */
  public function testRunInCommittedTransactionScope(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);
    $storedValue = 6;

    $conn->runInTransaction(function () use ($storedValue): void {
      $this->queryRunner->runQuery("UPDATE temp_transaction_test SET val={$storedValue};", __FUNCTION__);
    });

    $this->assertDatabaseValueEquals($storedValue);
  }

  /**
   * @return void
   * @throws DatabaseException
   * @throws \Throwable
   */
  public function testRunInRolledbackTransactionScope(): void
  {
    $conn = $this->queryRunner->connection();

    $this->createTemporaryTableWithValue(self::INITIAL_VALUE);

    $this->expectException(DatabaseException::class);
    $conn->runInTransaction(function (): void {
      $this->queryRunner->runQuery('UPDATE temp_transaction_test SET val=4;', __FUNCTION__);
      $this->queryRunner->runQuery('force error', __FUNCTION__);
    });

    $this->assertDatabaseValueEquals(self::INITIAL_VALUE);
  }

  /**
   * @param int $value
   *
   * @return void
   * @throws DatabaseException
   */
  private function createTemporaryTableWithValue(int $value): void
  {
    $this->queryRunner->runQuery(
      'CREATE TEMPORARY TABLE IF NOT EXISTS temp_transaction_test (val INTEGER);',
      __FUNCTION__
    );
    $this->queryRunner->runQuery(
      "INSERT INTO temp_transaction_test VALUES(${value});",
      __FUNCTION__
    );
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  private function truncateTempTable(): void
  {
    $this->queryRunner->runQuery('TRUNCATE temp_transaction_test;', __FUNCTION__);
  }

  /**
   * @param int $expectedValue
   *
   * @return void
   * @throws DatabaseException
   */
  private function assertDatabaseValueEquals(int $expectedValue): void
  {
    $queryResult = $this->queryRunner->runQuery('SELECT val FROM temp_transaction_test;', __FUNCTION__);
    $storedValue = pg_fetch_row($queryResult)[0];
    $this->assertSame($expectedValue, intval($storedValue));
  }
}
