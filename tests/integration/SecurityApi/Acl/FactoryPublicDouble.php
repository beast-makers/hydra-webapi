<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\SecurityApi\Acl;

use BeastMakers\SecurityApi\Acl\Config;
use BeastMakers\SecurityApi\Acl\DependencyProvider;
use BeastMakers\SecurityApi\Acl\Factory;
use BeastMakers\SecurityApi\Acl\Infra\Repository\WriteRepository;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return WriteRepository
   */
  public function shareAclWriteRepository(): WriteRepository
  {
    return parent::shareAclWriteRepository();
  }
}
