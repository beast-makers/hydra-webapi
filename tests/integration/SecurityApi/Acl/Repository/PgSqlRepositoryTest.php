<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\SecurityApi\Acl\Repository;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakersTest\Integration\SecurityApi\Acl\FactoryPublicDouble;
use BeastMakersTest\IntegrationTester;
use BeastMakersTest\TestCase\DatabaseTest;

class PgSqlRepositoryTest extends DatabaseTest
{
  protected IntegrationTester $tester;

  private static FactoryPublicDouble $moduleFactory;

  public static function setUpBeforeClass(): void
  {
    self::$moduleFactory = new FactoryPublicDouble();
  }


  /**
   * @return void
   * @throws DatabaseException
   */
  public function testCreateApiUser(): void
  {
    $writeRepository = self::$moduleFactory->shareAclWriteRepository();
    $readRepository = self::$moduleFactory->shareAclReadRepository();
    $faker = $this->tester->faker();

    $user = new ApiUser();
    $user->setUsername("{$faker->firstName}.{$faker->lastName}");
    $user->setIsActive(true);
    $user->setSecret($faker->password);
    $user->setSeed($faker->uuid);
    /** @psalm-suppress PossiblyInvalidArgument */
    $user->setRoles($faker->words());

    $writeRepository->createApiUser($user);
    $readUser = $readRepository->readApiUser($user->getUsername());

    $this->assertSame($user->getUsername(), $readUser->getUsername());
    $this->assertSame($user->getIsActive(), $readUser->getIsActive());
    $this->assertSame($user->getSecret(), $readUser->getSecret());
    $this->assertSame($user->getSeed(), $readUser->getSeed());
    $this->assertSame($user->getRoles(), $readUser->getRoles());
  }
}
