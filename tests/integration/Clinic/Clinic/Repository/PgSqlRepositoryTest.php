<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Clinic\Clinic\Repository;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakersTest\Integration\Clinic\Clinic\FactoryPublicDouble;
use BeastMakersTest\IntegrationTester;
use BeastMakersTest\TestCase\DatabaseTest;

class PgSqlRepositoryTest extends DatabaseTest
{
  protected IntegrationTester $tester;

  private static FactoryPublicDouble $moduleFactory;

  public static function setUpBeforeClass(): void
  {
    self::$moduleFactory = new FactoryPublicDouble();
  }

  /**
   * @return void
   */
  public function testReadNonExistingClinic(): void
  {
    $readRepository = self::$moduleFactory->shareClinicReadRepository();
    $clinic = $readRepository->findClinicById('non-existing-id');
    $this->assertTrue($clinic->isEmpty());
    $this->assertEmpty($clinic->getClinicId());
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testCreateUpdateClinic(): void
  {
    $writeRepository = self::$moduleFactory->shareClinicWriteRepository();
    $readRepository = self::$moduleFactory->shareClinicReadRepository();

    $faker = $this->tester->faker();
    $clinicId = 'clinic_' . $faker->randomNumber(3);
    $clinicDisplayName = $faker->word;
    $clinic = $this->createReducedClinic($clinicId, $clinicDisplayName);

    $writeRepository->createClinic($clinic);
    $this->tester->expect('clinic is created');

    $dbClinic = $readRepository->findClinicById($clinic->getClinicId());
    $this->assertSame($clinicId, $dbClinic->getClinicId());
    $this->assertSame($clinicDisplayName, $dbClinic->getDisplayName());

    $newClinicName = $clinicDisplayName . '-updated';
    $clinic->setDisplayName($newClinicName);
    $writeRepository->updateClinic($clinic);
    $this->tester->expect('clinic is updated');

    $dbClinic = $readRepository->findClinicById($clinicId);
    $this->assertSame($newClinicName, $dbClinic->getDisplayName());
  }

  /**
   * @return void
   * @throws DatabaseException
   */
  public function testCreateFullClinic(): void
  {
    $writeRepository = self::$moduleFactory->shareClinicWriteRepository();
    $readRepository = self::$moduleFactory->shareClinicReadRepository();

    $clinic = $this->createFullClinic();
    $writeRepository->createClinic($clinic);

    $dbClinic = $readRepository->findClinicById($clinic->getClinicId());
    $this->assertEquals($clinic, $dbClinic);
  }

  /**
   * @param string $clinicId
   * @param string $displayName
   *
   * @return Clinic
   */
  private function createReducedClinic(string $clinicId, string $displayName): Clinic
  {
    $clinic = new Clinic();
    $clinic->setClinicId($clinicId);
    $clinic->setDisplayName($displayName);
    $clinic->setIsActive(false);

    return $clinic;
  }

  /**
   * @return Clinic
   */
  private function createFullClinic(): Clinic
  {
    $faker = $this->tester->faker();

    $clinic = new Clinic();
    $clinic->setClinicId($faker->uuid);
    $clinic->setDisplayName($faker->name);
    $clinic->setIsActive(true);
    $clinic->setCity($faker->city);
    $clinic->setZip($faker->postcode);
    $clinic->setStreet($faker->streetName);
    $clinic->setCountryCode($faker->countryCode);
    $clinic->setLatitude($faker->latitude);
    $clinic->setLongitude($faker->longitude);
    $clinic->setRoles([Clinic::ROLE_3D_SCAN]);

    return $clinic;
  }
}
