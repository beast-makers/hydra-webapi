<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Clinic\Clinic;

use BeastMakers\Clinic\Clinic\Config;
use BeastMakers\Clinic\Clinic\DependencyProvider;
use BeastMakers\Clinic\Clinic\Factory;
use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Clinic\Infra\Repository\WriteRepository;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return ReadRepository
   */
  public function shareClinicReadRepository(): ReadRepository
  {
    return parent::shareClinicReadRepository();
  }

  /**
   * @return WriteRepository
   */
  public function shareClinicWriteRepository(): WriteRepository
  {
    return parent::shareClinicWriteRepository();
  }
}
