<?php
declare(strict_types=1);

namespace BeastMakersTest\Catalog\RawProduct;

use BeastMakersTest\ApiTester;

class RawProductCest
{
  private array $productProperties = [];

  /**
   * @param ApiTester $i
   *
   * @retun void
   * @throws \Exception
   */
  public function createRawProductSuccess(ApiTester $i): void
  {
    $this->productProperties = $this->sampleProductProperties($i);

    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/raw-product/create', $this->productProperties);
    $i->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
  }

  /**
   * @depends createRawProductSuccess
   *
   * @param ApiTester $i
   *
   * @retun void
   * @throws \Exception
   */
  public function updateRawProductSuccess(ApiTester $i): void
  {
    $newDeRegionUrl = '/the_thing6-updated';

    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/raw-product/update', [
      'product_id' => $this->productProperties['product_id'],
      'regions' => [
        'de' => [
          'url' => $newDeRegionUrl,
        ],
      ],
    ]);

    $this->productProperties['regions']['de']['url'] = $newDeRegionUrl;
    $i->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
  }

  /**
   * @depends createRawProductSuccess
   *
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function createAlreadyExistingRawProductNotAllowed(ApiTester $i): void
  {
    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/raw-product/create', [
      'product_id' => $this->productProperties['product_id'],
      'name' => $i->faker()->word,
      'type' => 'simple',
    ]);

    $i->seeResponseCodeIs(\Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
  }

  /**
   * @depends updateRawProductSuccess
   *
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function getExistingRawProduct(ApiTester $i): void
  {
    $i->loginAsApiReadOnlyUser();
    $i->sendGET('/raw-product/get/' . $this->productProperties['product_id']);
    $i->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);

    $rawProduct = json_decode($i->grabResponse(), true);
    foreach (array_keys($rawProduct['regions']) as $regionName) {
      if ($regionName !== 'de') {
        unset($rawProduct['regions'][$regionName]);
      }
    }
    $i->assertEquals($this->productProperties, $rawProduct);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function getNotExistingRawProduct(ApiTester $i): void
  {
    $i->loginAsApiReadOnlyUser();
    $i->sendGET('/raw-product/get/unknown');
    $i->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
  }

  /**
   * @param ApiTester $i
   *
   * @return array
   */
  private function sampleProductProperties(ApiTester $i): array
  {
    return [
      'product_id' => $i->faker()->uuid,
      'name' => "{$i->faker()->word}_" . uniqid(),
      'type' => 'simple',
      'regions' => [
        'de' => [
          'url' => '/the_thing6',
          'raw_price' => 2000000, //200
        ],
      ],
    ];
  }
}
