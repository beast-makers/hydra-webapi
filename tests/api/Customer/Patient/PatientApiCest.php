<?php
declare(strict_types=1);

namespace BeastMakersTest\Customer\Patient;

use BeastMakersTest\ApiTester;
use Codeception\Util\HttpCode;

class PatientApiCest
{
  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function validatePatientSuccess(ApiTester $i): void
  {
    $faker = $i->faker();

    $i->loginAsApiReadOnlyUser();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/customer/validate', [
      'first_name' => $faker->firstName,
      'last_name' => $faker->lastName,
      'email' => $faker->email,
      'birth_date' => $faker->date('d-m-Y'),
    ]);

    $i->seeResponseCodeIs(HttpCode::NO_CONTENT);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function validatePatientBadRequest(ApiTester $i): void
  {
    $faker = $i->faker();

    $i->loginAsApiReadOnlyUser();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/customer/validate', [
      'firstName' => $faker->firstName, //bad first_name param
      'last_name' => $faker->lastName,
      'email' => $faker->email,
      'birth_date' => $faker->date('d-m-Y'),
    ]);

    $i->seeResponseCodeIs(HttpCode::BAD_REQUEST);
  }
}
