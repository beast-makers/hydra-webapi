<?php
declare(strict_types=1);

namespace BeastMakersTest\Customer\Address;

use BeastMakersTest\ApiTester;
use Codeception\Util\HttpCode;

class AddressApiCest
{
  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function validateAddressSuccess(ApiTester $i): void
  {
    $faker = $i->faker();

    $i->loginAsApiReadOnlyUser();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('customer/validate-address', [
      'first_name' => $faker->firstName,
      'last_name' => $faker->lastName,
      'country_code' => $faker->countryCode,
      'street' => $faker->streetAddress,
      'address_extension' => 'basement',
      'city' => $faker->city,
      'zip' => $faker->postcode,
    ]);

    $i->seeResponseCodeIs(HttpCode::NO_CONTENT);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function validateAddressBadRequest(ApiTester $i): void
  {
    $faker = $i->faker();

    $i->loginAsApiReadOnlyUser();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/customer/validate-address', [
      'firstName' => $faker->firstName, //bad first_name param
      'last_name' => $faker->lastName,
      'country_code' => $faker->countryCode,
      'street' => $faker->streetAddress,
      'address_extension' => 'basement',
      'city' => $faker->city,
      'zip' => $faker->postcode,
    ]);

    $i->seeResponseCodeIs(HttpCode::BAD_REQUEST);
  }
}
