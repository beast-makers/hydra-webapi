<?php
declare(strict_types=1);

namespace BeastMakersTest\SecurityApi\ApiAuth;

use BeastMakers\SecurityApi\Shared\ErrorCode;
use BeastMakersTest\ApiTester;
use Codeception\Util\HttpCode;

class ApiAuthenticationCest
{
  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function adminAuthenticationSuccess(ApiTester $i): void
  {
    $i->loginAsApiAdmin();
    $i->seeResponseCodeIs(HttpCode::OK);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function readOnlyAuthenticationSuccess(ApiTester $i): void
  {
    $i->loginAsApiReadOnlyUser();
    $i->seeResponseCodeIs(HttpCode::OK);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function authenticationWrongCredentials(ApiTester $i): void
  {
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/api-auth/login', [
      'username' => 'unknown',
      'password' => 'unknown',
    ]);
    $i->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
    $i->dontSeeResponseJsonMatchesJsonPath('$.token');
    $errorCode = $i->grabDataFromResponseByJsonPath('$..[0].errorCode')[0];
    $i->assertSame(ErrorCode::INVALID_CREDENTIALS, $errorCode);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function authenticationWrongInput(ApiTester $i): void
  {
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST('/api-auth/login', []);
    $i->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    $i->dontSeeResponseJsonMatchesJsonPath('$.token');
    $errorCode = $i->grabDataFromResponseByJsonPath('$..[0].errorCode')[0];
    $i->assertSame(ErrorCode::INVALID_INPUT_DATA, $errorCode);
  }
}
