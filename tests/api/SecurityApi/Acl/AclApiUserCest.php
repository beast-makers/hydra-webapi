<?php
declare(strict_types=1);

namespace BeastMakersTest\SecurityApi\ApiUser;

use BeastMakersTest\ApiTester;
use Codeception\Util\HttpCode;

class AclApiUserCest
{
  /**
   * @var string
   */
  private $username = '';

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function createApiUserSuccess(ApiTester $i): void
  {
    $faker = $i->faker();
    $this->username = "Test.{$faker->firstName}.{$faker->lastName}";
    $randomRoles = $faker->randomElements(['admin', 'read-only', 'editor', 'creator'], random_int(1, 3));

    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST("/acl/create-api-user/{$this->username}", [
      'is_active' => true,
      'password' => $faker->password,
      'roles' => $randomRoles,
    ]);

    $i->seeResponseCodeIs(HttpCode::CREATED);
  }

  /**
   * @depends createApiUserSuccess
   *
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function createAlreadyExistingUserNotAllowed(ApiTester $i): void
  {
    $faker = $i->faker();
    $randomRoles = $faker->randomElements(['admin', 'read-only', 'editor', 'creator'], random_int(1, 3));

    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST("/acl/create-api-user/{$this->username}", [
      'is_active' => false,
      'password' => $faker->password,
      'roles' => $randomRoles,
    ]);

    $i->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
  }

  /**
   * @param ApiTester $i
   *
   * @return void
   * @throws \Exception
   */
  public function createApiUserInvalidRequest(ApiTester $i): void
  {
    $faker = $i->faker();
    $username = "Test.{$faker->firstName}.{$faker->lastName}";
    $randomRoles = $faker->randomElements(['admin', 'read-only', 'editor', 'creator'], random_int(1, 3));

    $i->loginAsApiAdmin();
    $i->haveHttpHeader('Content-Type', 'application/json');
    $i->sendPOST("/acl/create-api-user/{$this->username}", [
      'pass' => $faker->password,
      'roles' => $randomRoles,
    ]);

    $i->seeResponseCodeIs(HttpCode::BAD_REQUEST);
  }
}
