<?php
declare(strict_types=1);

namespace BeastMakersTest\Helper;

use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Exception;

class ArrayValidationHelper extends \Codeception\Module
{
  /**
   * @param \Closure $closure
   *
   * @return array
   * @throws Exception
   */
  public function catchArrayValidationErrors(\Closure $closure): array
  {
    try {
      $closure();
    } catch (ArrayValidationException $e) {
      return $e->getErrors();
    }

    throw new Exception('Expected exception');
  }
}
