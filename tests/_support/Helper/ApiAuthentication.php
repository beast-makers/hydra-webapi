<?php
declare(strict_types=1);

namespace BeastMakersTest\Helper;

use Codeception\Module\REST;

class ApiAuthentication extends \Codeception\Module
{
  /**
   * @return void
   * @throws \Exception
   */
  public function loginAsApiAdmin(): void
  {
    $this->loginAsApiUser('first_admin', 'thefirst');
  }

  /**
   * @return void
   * @throws \Exception
   */
  public function loginAsApiReadOnlyUser(): void
  {
    $this->loginAsApiUser('read-only', 'thefirst');
  }

  /**
   * @param string $username
   * @param string $password
   *
   * @return void
   * @throws \Exception
   */
  public function loginAsApiUser(string $username, string $password): void
  {
    /** @var REST $restModule */
    $restModule = $this->getModule('REST');
    $restModule->haveHttpHeader('Content-Type', 'application/json');
    $restModule->sendPOST('/api-auth/login', [
      'username' => $username,
      'password' => $password,
    ]);
    $restModule->seeResponseCodeIs(200);
    $authToken = $restModule->grabDataFromResponseByJsonPath('$.con_id')[0];

    $restModule->amBearerAuthenticated($authToken);
    $restModule->deleteHeader('Content-Type');
  }
}
