<?php
declare(strict_types=1);

namespace BeastMakersTest\TestCase;

use BeastMakersTest\HelperTrait\WebApiDatabaseTransaction;

class DatabaseTest extends \Codeception\Test\Unit
{
  use WebApiDatabaseTransaction;

  protected function _before(): void
  {
    $this->startTransaction();
  }

  protected function _after(): void
  {
    $this->rollbackTransaction();
  }
}
