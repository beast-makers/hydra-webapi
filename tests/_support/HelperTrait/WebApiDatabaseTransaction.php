<?php
declare(strict_types=1);

namespace BeastMakersTest\HelperTrait;

use BeastMakers\Shared\DatabaseConnector\QueryRunner;
use BeastMakers\Shared\DatabaseConnector\QueryRunnerPool;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;

trait WebApiDatabaseTransaction
{
  private QueryRunner $queryRunner;

  /**
   * @return void
   */
  private function startTransaction(): void
  {
    /** @var QueryRunnerPool $queryRunnerPool */
    $queryRunnerPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL);
    $this->queryRunner = $queryRunnerPool->get(QueryRunnerCatalog::WEBAPI);
    $this->queryRunner->connection()->startTransaction();
  }

  /**
   * @return void
   */
  private function rollbackTransaction(): void
  {
    $this->queryRunner->connection()->rollbackTransaction();
  }

  /**
   * @return void
   */
  private function commitTransaction(): void
  {
    $this->queryRunner->connection()->commitTransaction();
  }
}
