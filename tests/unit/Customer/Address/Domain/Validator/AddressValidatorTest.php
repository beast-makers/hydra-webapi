<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Patient\Address\Domain\Validator;

use BeastMakers\Customer\Address\Domain\Validator\AddressValidator;
use BeastMakers\Customer\Shared\Domain\Address;
use BeastMakersTest\Unit\Customer\Address\FactoryPublicDouble;
use BeastMakersTest\UnitTester;

class AddressValidatorTest extends \Codeception\Test\Unit
{
  protected UnitTester $tester;

  private static AddressValidator $addressValidator;

  /**
   * @return void
   */
  public static function setUpBeforeClass(): void
  {
    $moduleFactory = new FactoryPublicDouble();
    self::$addressValidator = $moduleFactory->newAddressValidator();
  }

  /**
   * @return void
   */
  public function testRequiredFields(): void
  {
    $address = new Address();

    $validationErrors = $this->catchAddressValidationErrors($address);

    $requiredFields = [
      Address::ATTR_FIRST_NAME,
      Address::ATTR_LAST_NAME,
      Address::ATTR_COUNTRY_CODE,
      Address::ATTR_STREET,
      Address::ATTR_CITY,
      Address::ATTR_ZIP,
    ];

    $this->assertSame($requiredFields, array_keys($validationErrors));
    $this->assertAllRequired($validationErrors);
  }

  /**
   * @dataProvider validFirstNames
   *
   * @param string $firstName
   *
   * @return void
   */
  public function testValidFirstName(string $firstName): void
  {
    $address = Address::createFromArray([
      Address::ATTR_FIRST_NAME => $firstName,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayNotHasKey(Address::ATTR_FIRST_NAME, $validationErrors);
  }

  /**
   * @return array
   */
  public function validFirstNames(): array
  {
    return [
      'Konrad' => ['Konrad'],
      'Kon rad' => ['Kon rad'],
      'Konrad-Junior' => ['Konrad-Junior'],
    ];
  }

  /**
   * @dataProvider invalidFirstNames
   *
   * @param string $firstName
   *
   * @return void
   */
  public function testInvalidFirstName(string $firstName): void
  {
    $address = Address::createFromArray([
      Address::ATTR_FIRST_NAME => $firstName,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayHasKey(Address::ATTR_FIRST_NAME, $validationErrors);
    $this->assertRule('not_regex', $validationErrors[Address::ATTR_FIRST_NAME]);
  }

  /**
   * @return array
   */
  public function invalidFirstNames(): array
  {
    return [
      'Kon_rad' => ['Kon_rad'],
      'Konrad@' => ['Konrad@'],
      'Konrad 2nd' => ['Konrad 2nd'],
    ];
  }

  /**
   * @dataProvider validLastNames
   *
   * @param string $lastName
   *
   * @return void
   */
  public function testValidLastName(string $lastName): void
  {
    $address = Address::createFromArray([
      Address::ATTR_LAST_NAME => $lastName,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayNotHasKey(Address::ATTR_LAST_NAME, $validationErrors);
  }

  /**
   * @return array
   */
  public function validLastNames(): array
  {
    return [
      'Smith' => ['Smith'],
      'Smith Thomas' => ['Smith Thomas'],
      'Smith-Thomas' => ['Smith-Thomas'],
    ];
  }

  /**
   * @dataProvider invalidLastNames
   *
   * @param string $lastName
   *
   * @return void
   */
  public function testInvalidLastName(string $lastName): void
  {
    $address = Address::createFromArray([
      Address::ATTR_LAST_NAME => $lastName,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayHasKey(Address::ATTR_LAST_NAME, $validationErrors);
    $this->assertRule('not_regex', $validationErrors[Address::ATTR_LAST_NAME]);
  }

  /**
   * @return array
   */
  public function invalidLastNames(): array
  {
    return [
      'Sm_ith' => ['Sm_ith'],
      'Smith@' => ['Smith@'],
      'Smith 2nd' => ['Smith 2nd'],
    ];
  }

  /**
   * @dataProvider invalidCountryCodes
   *
   * @param string $countryCode
   *
   * @return void
   */
  public function testInvalidCountryCode(string $countryCode): void
  {
    $address = Address::createFromArray([
      Address::ATTR_COUNTRY_CODE => $countryCode,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayHasKey(Address::ATTR_COUNTRY_CODE, $validationErrors);
    $this->assertRule('country_code_alpha2', $validationErrors[Address::ATTR_COUNTRY_CODE]);
  }

  /**
   * @return array
   */
  public function invalidCountryCodes(): array
  {
    return [
      'pl' => ['pl'],
      'Pl' => ['Pl'],
      'POL' => ['POL'],
      'pol' => ['pol'],
      'poland' => ['poland'],
    ];
  }

  /**
   * @dataProvider validCountryCodes
   *
   * @param string $countryCode
   *
   * @return void
   */
  public function testValidCountryCodes(string $countryCode): void
  {
    $address = Address::createFromArray([
      Address::ATTR_COUNTRY_CODE => $countryCode,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayNotHasKey(Address::ATTR_COUNTRY_CODE, $validationErrors);
  }

  /**
   * @return array
   */
  public function validCountryCodes(): array
  {
    return [
      'DE' => ['DE'],
    ];
  }

  /**
   * @dataProvider validCities
   *
   * @param string $city
   *
   * @return void
   */
  public function testValidCity(string $city): void
  {
    $address = Address::createFromArray([
      Address::ATTR_CITY => $city,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayNotHasKey(Address::ATTR_CITY, $validationErrors);
  }

  /**
   * @return array
   */
  public function validCities(): array
  {
    return [
      'Berlin' => ['Berlin'],
      'Sao Paolo' => ['Sao Paolo'],
      'Saint-Tropez' => ['Saint-Tropez'],
    ];
  }

  /**
   * @dataProvider invalidCities
   *
   * @param string $city
   *
   * @return void
   */
  public function testInvalidCity(string $city): void
  {
    $address = Address::createFromArray([
      Address::ATTR_CITY => $city,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayHasKey(Address::ATTR_CITY, $validationErrors);
    $this->assertRule('not_regex', $validationErrors[Address::ATTR_CITY]);
  }

  /**
   * @return array
   */
  public function invalidCities(): array
  {
    return [
      'Saint_Tropez' => ['Saint_Tropez'],
      '#Berlin' => ['#Berlin'],
      'Area 21' => ['Area 21'],
    ];
  }

  /**
   * @dataProvider validZip
   *
   * @param string $zip
   *
   * @return void
   */
  public function testValidZip(string $zip): void
  {
    $address = Address::createFromArray([
      Address::ATTR_ZIP => $zip,
    ]);

    $validationErrors = $this->catchAddressValidationErrors($address);
    $this->assertArrayNotHasKey(Address::ATTR_ZIP, $validationErrors);
  }

  /**
   * @return array
   */
  public function validZip(): array
  {
    return [
      '12345' => ['12345'],
      '32-123' => ['32-123'],
      'A1234' => ['A1234'],
    ];
  }

  /**
   * @param Address $addressDto
   *
   * @return array
   */
  private function catchAddressValidationErrors(Address $addressDto): array
  {
    return $this->tester->catchArrayValidationErrors(
      fn () => self::$addressValidator->validate($addressDto)
    );
  }

  /**
   * @param array $errors
   *
   * @return void
   */
  private function assertAllRequired(array $errors): void
  {
    $flatArray = [];
    foreach ($errors as $fieldName => $errorEntries) {
      $flatArray = array_merge($flatArray, $errorEntries);
    }
    $allBrokenRules = array_column($flatArray, null, 'rule');
    $this->assertCount(1, $allBrokenRules);
    $this->assertSame('required', array_key_first($allBrokenRules));
  }

  /**
   * @param string $ruleName
   * @param array $errors
   *
   * @return void
   */
  private function assertRule(string $ruleName, array $errors): void
  {
    $this->assertSame($ruleName, current($errors)['rule']);
  }
}
