<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Customer\Address;

use BeastMakers\Customer\Address\Config;
use BeastMakers\Customer\Address\DependencyProvider;
use BeastMakers\Customer\Address\Domain\Validator\AddressValidator;
use BeastMakers\Customer\Address\Factory;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return AddressValidator
   */
  public function newAddressValidator(): AddressValidator
  {
    return parent::newAddressValidator();
  }
}
