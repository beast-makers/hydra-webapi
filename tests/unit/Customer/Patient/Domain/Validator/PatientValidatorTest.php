<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Customer\Patient\Domain\Validator;

use BeastMakers\Customer\Patient\Domain\Validator\PatientValidator;
use BeastMakers\Customer\Shared\Domain\Patient;
use BeastMakersTest\Unit\Customer\Patient\FactoryPublicDouble;
use BeastMakersTest\UnitTester;
use Rakit\Validation\RuleNotFoundException;

class PatientValidatorTest extends \Codeception\Test\Unit
{
  protected UnitTester $tester;

  private static PatientValidator $patientValidator;

  /**
   * @return void
   * @throws RuleNotFoundException
   */
  public static function setUpBeforeClass(): void
  {
    $moduleFactory = new FactoryPublicDouble();
    self::$patientValidator = $moduleFactory->newPatientValidator();
  }

  /**
   * @return void
   */
  public function testRequiredFields(): void
  {
    $patient = new Patient();
    $validationErrors = $this->catchPatientValidationErrors($patient);

    $requiredFields = [
      Patient::ATTR_FIRST_NAME,
      Patient::ATTR_LAST_NAME,
      Patient::ATTR_EMAIL,
      Patient::ATTR_BIRTH_DATE,
    ];

    $this->assertSame($requiredFields, array_keys($validationErrors));
    $this->assertAllRequired($validationErrors);
  }

  /**
   * @dataProvider validFirstNames
   *
   * @param string $firstName
   *
   * @return void
   */
  public function testValidFirstName(string $firstName): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_FIRST_NAME => $firstName,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayNotHasKey(Patient::ATTR_FIRST_NAME, $validationErrors);
  }

  /**
   * @return array
   */
  public function validFirstNames(): array
  {
    return [
      'Konrad' => ['Konrad'],
      'Kon rad' => ['Kon rad'],
      'Konrad-Junior' => ['Konrad-Junior'],
    ];
  }

  /**
   * @dataProvider invalidFirstNames
   *
   * @param string $firstName
   *
   * @return void
   */
  public function testInvalidFirstName(string $firstName): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_FIRST_NAME => $firstName,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayHasKey(Patient::ATTR_FIRST_NAME, $validationErrors);
    $this->assertRule('not_regex', $validationErrors[Patient::ATTR_FIRST_NAME]);
  }

  /**
   * @return array
   */
  public function invalidFirstNames(): array
  {
    return [
      'Kon_rad' => ['Kon_rad'],
      'Konrad@' => ['Konrad@'],
      'Konrad 2nd' => ['Konrad 2nd'],
    ];
  }

  /**
   * @dataProvider validLastNames
   *
   * @param string $lastName
   *
   * @return void
   */
  public function testValidLastName(string $lastName): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_LAST_NAME => $lastName,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayNotHasKey(Patient::ATTR_LAST_NAME, $validationErrors);
  }

  /**
   * @return array
   */
  public function validLastNames(): array
  {
    return [
      'Smith' => ['Smith'],
      'Smith Thomas' => ['Smith Thomas'],
      'Smith-Thomas' => ['Smith-Thomas'],
    ];
  }

  /**
   * @dataProvider invalidLastNames
   *
   * @param string $lastName
   *
   * @return void
   */
  public function testInvalidLastName(string $lastName): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_LAST_NAME => $lastName,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayHasKey(Patient::ATTR_LAST_NAME, $validationErrors);
    $this->assertRule('not_regex', $validationErrors[Patient::ATTR_LAST_NAME]);
  }

  /**
   * @return array
   */
  public function invalidLastNames(): array
  {
    return [
      'Sm_ith' => ['Sm_ith'],
      'Smith@' => ['Smith@'],
      'Smith 2nd' => ['Smith 2nd'],
    ];
  }

  /**
   * @dataProvider validEmails
   *
   * @param string $email
   *
   * @return void
   */
  public function testValidEmail(string $email): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_EMAIL => $email,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayNotHasKey(Patient::ATTR_EMAIL, $validationErrors);
  }

  /**
   * @return array
   */
  public function validEmails(): array
  {
    return [
      'Konrad@plusdental.de' => ['Konrad@plusdental.de'],
      'konr-ad@plusdental.de' => ['konr-ad@plusdental.de'],
      'konr_ad.gaw@plusdental.de' => ['konr_ad.gaw@plusdental.de'],
    ];
  }

  /**
   * @dataProvider invalidEmails
   *
   * @param string $email
   *
   * @return void
   */
  public function testInvalidEmail(string $email): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_EMAIL => $email,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayHasKey(Patient::ATTR_EMAIL, $validationErrors);
    $this->assertRule('email', $validationErrors[Patient::ATTR_EMAIL]);
  }

  /**
   * @return array
   */
  public function invalidEmails(): array
  {
    return [
      'konrad@' => ['konrad@'],
      'konrad@plusdental' => ['konrad@plusdental'],
      'konrad' => ['konrad'],
    ];
  }

  /**
   * @dataProvider validBirthDates
   *
   * @param string $birthDate
   *
   * @return void
   */
  public function testValidBirthDate(string $birthDate): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_BIRTH_DATE => $birthDate,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayNotHasKey(Patient::ATTR_BIRTH_DATE, $validationErrors);
  }

  /**
   * @return array
   */
  public function validBirthDates(): array
  {
    return [
      '11-10-2012' => ['11-10-2012'],
      '01-01-2012' => ['01-01-2012'],
      '29-02-2000' => ['29-02-2000'], //leap year
    ];
  }

  /**
   * @dataProvider invalidBirthDates
   *
   * @param string $birthDate
   * @param string $constraintName
   *
   * @return void
   */
  public function testInvalidBirthDates(string $birthDate, string $constraintName): void
  {
    $patient = Patient::createFromArray([
      Patient::ATTR_BIRTH_DATE => $birthDate,
    ]);

    $validationErrors = $this->catchPatientValidationErrors($patient);
    $this->assertArrayHasKey(Patient::ATTR_BIRTH_DATE, $validationErrors);
    $this->assertRule($constraintName, $validationErrors[Patient::ATTR_BIRTH_DATE]);
  }

  /**
   * @return array
   */
  public function invalidBirthDates(): array
  {
    return [
      '1-1-2012' => ['1-1-2012', 'date_format'],
      '2012-01-01' => ['03-16-2012', 'date_format'],
      '29-02-2011' => ['29-02-2011', 'date_format'], //not a leap year
      '03-16-2012' => ['03-16-2012', 'date_format'],
      'aa-01-2012' => ['aa-01-2012', 'date_format'],
    ];
  }

  /**
   * @param Patient $patientDto
   *
   * @return array
   */
  private function catchPatientValidationErrors(Patient $patientDto): array
  {
    return $this->tester->catchArrayValidationErrors(
      fn () => self::$patientValidator->validate($patientDto)
    );
  }

  /**
   * @param array $errors
   *
   * @return void
   */
  private function assertAllRequired(array $errors): void
  {
    $flatArray = [];
    foreach ($errors as $fieldName => $errorEntries) {
      $flatArray = array_merge($flatArray, $errorEntries);
    }
    $allBrokenRules = array_column($flatArray, null, 'rule');
    $this->assertCount(1, $allBrokenRules);
    $this->assertSame('required', array_key_first($allBrokenRules));
  }

  /**
   * @param string $ruleName
   * @param array $errors
   *
   * @return void
   */
  private function assertRule(string $ruleName, array $errors): void
  {
    $this->assertSame($ruleName, current($errors)['rule']);
  }
}
