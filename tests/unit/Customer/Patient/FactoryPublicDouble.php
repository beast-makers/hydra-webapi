<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Customer\Patient;

use BeastMakers\Customer\Patient\Config;
use BeastMakers\Customer\Patient\DependencyProvider;
use BeastMakers\Customer\Patient\Domain\Validator\PatientValidator;
use BeastMakers\Customer\Patient\Factory;
use Rakit\Validation\RuleNotFoundException;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return PatientValidator
   * @throws RuleNotFoundException
   */
  public function newPatientValidator(): PatientValidator
  {
    return parent::newPatientValidator();
  }
}
