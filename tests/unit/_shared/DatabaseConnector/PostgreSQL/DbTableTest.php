<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\DatabaseConnector\PostgreSql;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;
use BeastMakersTest\Unit\Shared\DatabaseConnector\PostgreSql\DbTableFixture as tf;

class DbTableTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public static function setUpBeforeClass(): void
  {
    $queryRunnerPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL);
    $queryRunner = $queryRunnerPool->get(QueryRunnerCatalog::WEBAPI);
    $queryRunner->connection()->connectOnce();
  }

  /**
   * @dataProvider escapeLiteralDP
   * @param mixed $literal
   * @param string $columnName
   * @param mixed $expected
   *
   * @return void
   */
  public function testEscapeLiteral($literal, string $columnName, $expected): void
  {
    $escaped = tf::escapeLiteral($literal, $columnName);
    $this->assertEquals($expected, $escaped);
  }

  /**
   * @return array
   */
  public function escapeLiteralDP(): array
  {
    return [
      '0 => 0' => [0, tf::COLUMN_AGE, 0],
      '21 => 21' => [21, tf::COLUMN_AGE, 21],
      "'0' => 0" => ['0', tf::COLUMN_AGE, 0],
      "'21' => 21" => ['21', tf::COLUMN_AGE, 21],
      "konrad => 'konrad'" => ['konrad', tf::COLUMN_NAME, "'konrad'"],
      "konrad gawlinski => 'konrad gawlinski'" => ['konrad gawlinski', tf::COLUMN_NAME, "'konrad gawlinski'"],
      "true => 'true'" => [true, tf::COLUMN_IS_ACTIVE, 'true'],
      "false => 'false'" => [false, tf::COLUMN_IS_ACTIVE, 'false'],
      "'true' => 'true'" => ['true', tf::COLUMN_IS_ACTIVE, 'true'],
      "'false' => 'false'" => ['false', tf::COLUMN_IS_ACTIVE, 'false'],
      "[foo, bar, 69] => ARRAY['foo', 'bar', '69']" => [['foo', 'bar', 69], tf::COLUMN_ROLES, "ARRAY['foo', 'bar', '69']"],
    ];
  }

  /**
   * @return void
   */
  public function testPrepareInsertList(): void
  {
    $pairList = tf::prepareInsertLists([
      tf::COLUMN_NAME => 'foobar',
      tf::COLUMN_IS_ACTIVE => true,
      tf::COLUMN_AGE => 69,
    ]);

    $this->assertEquals('"name","is_active","age"', $pairList->getIdentifiers());
    $this->assertEquals("'foobar',true,69", $pairList->getValues());
  }

  /**
   * @return void
   */
  public function testPrepareUpdateList(): void
  {
    $updateStatement = tf::prepareUpdateStatementList([
      tf::COLUMN_NAME => 'foobar',
      tf::COLUMN_IS_ACTIVE => true,
      tf::COLUMN_AGE => 69,
    ]);

    $this->assertEquals('"name"=\'foobar\',"is_active"=true,"age"=69', $updateStatement);
  }

  /**
   * @dataProvider escapeIdentifierDP
   * @param mixed $literal
   * @param mixed $expected
   *
   * @return void
   */
  public function testEscapeIdentifier($literal, $expected): void
  {
    $escaped = tf::escapeIdentifier($literal);
    $this->assertSame($expected, $escaped);
  }

  /**
   * @return array
   */
  public function escapeIdentifierDP(): array
  {
    return [
      "'0' => 0" => ['0', '"0"'],
      "'konrad' => \"konrad\"" => ['konrad', '"konrad"'],
      "'konrad_gawlinski' => \"konrad_gawlinski\"" => ['konrad_gawlinski', '"konrad_gawlinski"'],
    ];
  }

  /**
   * @return void
   */
  public function testUniqId(): void
  {
    $idA = tf::uniqid();
    $idB = tf::uniqid();

    $this->assertNotSame($idA, $idB);
  }

  /**
   * @return void
   */
  public function testUniqIdLength(): void
  {
    $id = tf::uniqid();
    $this->assertSame(strlen($id), 12);

    $id = tf::uniqid(24);
    $this->assertSame(strlen($id), 24);
  }
}
