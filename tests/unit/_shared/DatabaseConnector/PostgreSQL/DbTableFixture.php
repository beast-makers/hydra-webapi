<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\DatabaseConnector\PostgreSql;

use BeastMakers\Shared\DatabaseConnector\PostgreSQL\DbTable;

class DbTableFixture extends DbTable
{
  public const COLUMN_FIXTURE_ID = 'fixture_id';
  public const COLUMN_IS_ACTIVE = 'is_active';
  public const COLUMN_NAME = 'name';
  public const COLUMN_AGE = 'age';
  public const COLUMN_ROLES = 'roles';

  protected const TYPES = [
    self::COLUMN_FIXTURE_ID => DbTable::TYPE_TEXT,
    self::COLUMN_IS_ACTIVE => DbTable::TYPE_BOOL,
    self::COLUMN_NAME => DbTable::TYPE_TEXT,
    self::COLUMN_AGE => DbTable::TYPE_NUMERIC,
    self::COLUMN_ROLES => DbTable::TYPE_TEXT_ARRAY,
  ];
}
