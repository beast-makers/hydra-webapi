<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\Kernel\ModuleResolver;

use BeastMakers\SecurityApi\ApiAuth\Factory;
use BeastMakers\Shared\Kernel\FactoryRegistry;

class FactoryRegistryTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public function testCorrectModuleResolution(): void
  {
    $factoryInstance = FactoryRegistry::createBeastMakersModuleFactory(Factory::class);
    $this->assertInstanceOf(Factory::class, $factoryInstance);
  }
}
