<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\TransferObject;

use BeastMakers\Shared\TransferObject\Strategy\MergeStrategy;
use BeastMakersTest\Unit\Shared\TransferObject\Fixture\ScalarFixture;

class MergeStrategyTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public function testScalarValueGivenString(): void
  {
    $expected = 'some string';
    $result = MergeStrategy::scalar($expected);

    $this->assertEquals($expected, $result);
  }

  /**
   * @return void
   */
  public function testScalarValueGivenInt(): void
  {
    $expected = 69;
    $result = MergeStrategy::scalar($expected);

    $this->assertEquals($expected, $result);
  }

  /**
   * @return void
   */
  public function testArray(): void
  {
    $arrayA = [1, 3, 5];
    $arrayB = [2, 3];

    $result = MergeStrategy::array($arrayA, $arrayB);
    $this->assertSame([1, 3, 5, 2, 3], $result);
  }

  /**
   * @return void
   */
  public function testUniqueArray(): void
  {
    $arrayA = [1, 3, 5];
    $arrayB = [2, 3];

    $result = MergeStrategy::arrayUnique($arrayA, $arrayB);
    $this->assertSame([1, 3, 5, 2], $result);
  }

  /**
   * @return void
   */
  public function testDtoReplacingAll(): void
  {
    $arrayA = $this->sampleScalarFixtureProperties('konrad', 21);
    $arrayB = $this->sampleScalarFixtureProperties('ala', 19);

    /** @var ScalarFixture $result */
    $result = MergeStrategy::dto($arrayA, ScalarFixture::class, $arrayB);
    $this->assertInstanceOf(ScalarFixture::class, $result);
    $this->assertEquals('ala', $result->getFirstName());
    $this->assertEquals(19, $result->getAge());
  }

  /**
   * @return void
   */
  public function testDtoIntersect(): void
  {
    $arrayA = $this->sampleScalarFixtureProperties('konrad');
    $arrayB = $this->sampleScalarFixtureProperties('', 21);

    /** @var ScalarFixture $result */
    $result = MergeStrategy::dto($arrayA, ScalarFixture::class, $arrayB);
    $this->assertInstanceOf(ScalarFixture::class, $result);
    $this->assertEquals('konrad', $result->getFirstName());
    $this->assertEquals(21, $result->getAge());
  }

  /**
   * @return void
   */
  public function testAssocArrayWithDto(): void
  {
    $arrayA = [
      'one' => $this->sampleScalarFixtureProperties('konrad'),
      'two' => $this->sampleScalarFixtureProperties('', 19),
    ];

    $arrayB = [
      'two' => $this->sampleScalarFixtureProperties('ala'),
      'three' => $this->sampleScalarFixtureProperties('', 21),
    ];

    $result = MergeStrategy::assocArrayWithDto($arrayA, ScalarFixture::class, $arrayB);
    $this->assertEquals(['one', 'two', 'three'], array_keys($result));
    $this->assertEquals($this->sampleScalarFixtureProperties('konrad'), $result['one']->toArray());
    $this->assertEquals($this->sampleScalarFixtureProperties('ala', 19), $result['two']->toArray());
    $this->assertEquals($this->sampleScalarFixtureProperties('', 21), $result['three']->toArray());
  }

  /**
   * @param string $firstName
   * @param int $age
   *
   * @return array
   */
  private function sampleScalarFixtureProperties(string $firstName = '', int $age = 0): array
  {
    $sample = [];
    $firstName !== '' && $sample[ScalarFixture::ATTR_FIRST_NAME] = $firstName;
    $age !== 0 && $sample[ScalarFixture::ATTR_AGE] = $age;

    return $sample;
  }
}
