<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\TransferObject;

use BeastMakers\Shared\TransferObject\Strategy\ToArrayStrategy;
use BeastMakersTest\Unit\Shared\TransferObject\Fixture\ScalarFixture;

class ToArrayStrategyTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public function testScalarValueGivenString(): void
  {
    $expected = 'some string';
    $result = ToArrayStrategy::scalar($expected);

    $this->assertSame($expected, $result);
  }

  /**
   * @return void
   */
  public function testScalarValueGivenInt(): void
  {
    $expected = 69;
    $result = ToArrayStrategy::scalar($expected);

    $this->assertSame($expected, $result);
  }

  /**
   * @return void
   */
  public function testAssocArrayWithDto(): void
  {
    $array = [
      'one' => $this->sampleScalarFixture('konrad', 69),
      'two' => $this->sampleScalarFixture('ala'),
    ];

    $expected = [
      'one' => $this->sampleScalarFixtureProperties('konrad', 69),
      'two' => $this->sampleScalarFixtureProperties('ala'),
    ];

    $result = ToArrayStrategy::assocArrayWithDto($array);
    $this->assertSame($result, $expected);
  }

  /**
   * @return void
   */
  public function testDto(): void
  {
    $dto = $this->sampleScalarFixture('konrad', 69);
    $expected = $this->sampleScalarFixtureProperties('konrad', 69);

    $result = ToArrayStrategy::dtoToArray($dto);
    $this->assertSame($result, $expected);
  }

  /**
   * @param string $firstName
   * @param int $age
   *
   * @return ScalarFixture
   */
  private function sampleScalarFixture(string $firstName = '', int $age = 0): ScalarFixture
  {
    $sample = new ScalarFixture();
    $firstName !== '' && $sample->setFirstName($firstName);
    $age !== 0 && $sample->setAge($age);

    return $sample;
  }

  /**
   * @param string $firstName
   * @param int $age
   *
   * @return array
   */
  private function sampleScalarFixtureProperties(string $firstName = '', int $age = 0): array
  {
    $sample = [];
    $firstName !== '' && $sample[ScalarFixture::ATTR_FIRST_NAME] = $firstName;
    $age !== 0 && $sample[ScalarFixture::ATTR_AGE] = $age;

    return $sample;
  }
}
