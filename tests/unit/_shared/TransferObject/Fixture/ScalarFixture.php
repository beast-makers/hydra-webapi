<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\TransferObject\Fixture;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ScalarFixture extends ArrayDto
{
  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_AGE = 'age';

  /**
   * @return string
   */
  public function getFirstName(): string
  {
    return $this->get(self::ATTR_FIRST_NAME, '');
  }

  /**
   * @param string $firstName
   *
   * @return ScalarFixture
   */
  public function setFirstName(string $firstName): self
  {
    $this->set(self::ATTR_FIRST_NAME, $firstName);

    return $this;
  }

  /**
   * @return int
   */
  public function getAge(): int
  {
    return $this->get(self::ATTR_AGE, 0);
  }

  /**
   * @param int $age
   *
   * @return ScalarFixture
   */
  public function setAge(int $age): self
  {
    $this->set(self::ATTR_AGE, $age);

    return $this;
  }

  /**
   * @inheritDoc
   */
  public function definition(): array
  {
    return [
      self::ATTR_FIRST_NAME => self::DEF_SCALAR,
      self::ATTR_AGE => self::DEF_SCALAR,
    ];
  }
}
