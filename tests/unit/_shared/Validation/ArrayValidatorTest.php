<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\Validation;

use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;

class ArrayValidatorTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public function testValidationResultContents(): void
  {
    $validator = new ArrayValidator();
    $rules = [
      'first_name' => 'required',
      'age' => 'required|min:7',
    ];

    $input = [
      'first_nam' => 'konrad',
      'age' => 1,
    ];

    $errors = [];
    try {
      $validator->validate($input, $rules);
    } catch (ArrayValidationException $e) {
      $errors = $e->getErrors();
    }

    $this->assertSame(
      [
        'first_name' => [$this->validationError('first_name', 'required', ':field_label is required', null)],
        'age' => [$this->validationError('age', 'min', 'The Age minimum is 7', 1)],
      ],
      $errors
    );
  }

  /**
   * @param string $field
   * @param string $rule
   * @param string $messageTemplate
   * @param mixed $invalidValue
   *
   * @return array
   */
  private function validationError(string $field, string $rule, string $messageTemplate, $invalidValue): array
  {
    return [
      'field' => $field,
      'rule' => $rule,
      'messageTemplate' => $messageTemplate,
      'invalidValue' => $invalidValue,
    ];
  }
}
