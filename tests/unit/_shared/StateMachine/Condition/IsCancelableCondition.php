<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine\Condition;

use BeastMakers\Shared\StateMachine\Condition;
use BeastMakers\Shared\StateMachine\Exception\MissingMetadataKeyException;
use BeastMakers\Shared\StateMachine\StateMachine;

class IsCancelableCondition extends Condition
{
  /**
   * @param StateMachine $stateMachine
   * @param array $arguments
   *
   * @return bool
   */
  public static function execute(StateMachine $stateMachine, array $arguments = []): bool
  {
    try {
      $isCancelable = $stateMachine->readMetadataBoolValue('is_cancelable');
    } catch (MissingMetadataKeyException $e) {
      $isCancelable = false;
    }

    if (!$isCancelable) {
      $stateMachine->setStatusCode('CANCEL_NOT_ALLOWED');
    }

    return $isCancelable;
  }
}
