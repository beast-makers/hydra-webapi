<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine\Condition;

use BeastMakers\Shared\StateMachine\Condition;
use BeastMakers\Shared\StateMachine\StateMachine;

class AlwaysTrueCondition extends Condition
{
  /**
   * @param StateMachine $stateMachine
   * @param array $arguments
   *
   * @return bool
   */
  public static function execute(StateMachine $stateMachine, array $arguments = []): bool
  {
    return true;
  }
}
