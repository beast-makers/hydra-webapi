<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\Exception\MissingMetadataKeyException;
use BeastMakers\Shared\StateMachine\StateMachineReader;
use BeastMakers\Shared\StateMachine\StateMachineRegistry;

class StateMachineReaderTest extends \Codeception\Test\Unit
{
  private static StateMachineReader $smReader;

  private const SM_ID = 'appointment';
  private const DEFINITION_DIR = __DIR__ . '/fixtures/appointment';

  /**
   * @throws \Exception
   */
  public static function setUpBeforeClass(): void
  {
    $smRegistry = new StateMachineRegistry();
    $smRegistry->register('base', self::DEFINITION_DIR . '/base_fsm.yml');
    $smRegistry->register('treatment_task', self::DEFINITION_DIR . '/treatment_task_fsm.yml');
    $smRegistry->register(self::SM_ID, self::DEFINITION_DIR . '/appointment_fsm.yml');

    self::$smReader = new StateMachineReader($smRegistry);
    self::$smReader->use(self::SM_ID);
  }

  /**
   * @return void
   */
  public function testReadStateGivenStateName(): void
  {
    $stateProperties = self::$smReader->readState('start');

    $this->assertIsArray($stateProperties);
    $this->assertNotEmpty($stateProperties);
  }

  /**
   * @return void
   */
  public function testActionExists(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 0);
    $existsAction = self::$smReader->actionExistsFor($transition);
    $this->assertTrue($existsAction);
  }

  /**
   * @return void
   */
  public function testActionDoesNotExist(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 1);
    $existsAction = self::$smReader->actionExistsFor($transition);
    $this->assertFalse($existsAction);
  }

  /**
   * @return void
   */
  public function testReadActionForStateGivenNoArgumentsFunction(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 0);
    [$className, $arguments] = self::$smReader->readActionFor($transition);

    $this->assertEquals('BeastMakers\Shared\StateMachine\Appointment\Actions\StartClinicCheckin', $className);
    $this->assertEquals([], $arguments);
  }

  /**
   * @return void
   */
  public function testReadActionForStateGivenFunctionWithArguments(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 3);
    [$className, $arguments] = self::$smReader->readActionFor($transition);

    $this->assertEquals('BeastMakers\Shared\StateMachine\Appointment\Actions\CreateCustomerServiceTask', $className);
    $this->assertEquals(['appointment_reminder'], $arguments);
  }

  /**
   * @return void
   */
  public function testReadConditionForTransitionGivenNoArgumentsFunction(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 2);
    [$className, $arguments] = self::$smReader->readConditionFor($transition);

    $this->assertEquals('BeastMakers\Shared\StateMachine\Appointment\Condition\IsExpired', $className);
    $this->assertEquals([], $arguments);
  }

  /**
   * @return void
   */
  public function testReadConditionForTransitionGivenFunctionWithArguments(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 3);
    [$className, $arguments] = self::$smReader->readConditionFor($transition);

    $this->assertEquals('BeastMakers\Shared\StateMachine\Appointment\Condition\Timeout', $className);
    $this->assertEquals(['1day'], $arguments);
  }

  /**
   * @retun void
   */
  public function testReadStateNameGivenTransition(): void
  {
    $transition = $this->readTransitionAtStateByIndex('start', 0);
    $targetStateName = self::$smReader->getStateNameFor($transition);

    $this->assertEquals('appointment_confirmation', $targetStateName);
  }

  /**
   * @retun void
   */
  public function testReadEventGivenTransition(): void
  {
    $transition = $this->readTransitionAtStateByIndex('appointment_confirmation', 0);
    $eventName = self::$smReader->getEventFor($transition);

    $this->assertEquals('event_appointment_confirmed', $eventName);
  }

  /**
   * @return void
   */
  public function testAllTransitionsGivenStateName(): void
  {
    $transitions = self::$smReader->allTransitionsByState('appointment_confirmation');
    $expectedStates = ['appointment_confirmed', 'appointment_canceled', 'appointment_expired', 'call_visitor'];

    $this->assertTransitionsState($transitions, $expectedStates);
  }

  /**
   * @return void
   */
  public function testAllUnconditionalTransitionsGivenStateName(): void
  {
    $transitions = self::$smReader->allUnconditionalTransitionsByState('start');
    $expectedStates = ['appointment_confirmation'];

    $this->assertTransitionsState($transitions, $expectedStates);
  }

  /**
   * @return void
   */
  public function testAllEventTriggeredTransitionsGivenStateName(): void
  {
    $transitions = self::$smReader->allEventTriggeredTransitionsByState('appointment_confirmation');
    $expectedStates = ['appointment_confirmed', 'appointment_canceled'];

    $this->assertTransitionsState($transitions, $expectedStates);
  }

  public function testGlobalEventTransition(): void
  {
    $transitions = self::$smReader->allGlobalEventTransitions();

    $this->assertArrayHasKey('cancel', $transitions);
  }

  /**
   * @return void
   */
  public function testAllMetadata(): void
  {
    $metadata = self::$smReader->allMetadataFor('appointment_expired');
    $expected = [
      'is_cancellable' => true, 'is_expired' => true,
    ];

    $this->assertEquals($expected, $metadata);
  }

  /**
   * @return void
   * @throws MissingMetadataKeyException
   */
  public function testReadMetadataByKey(): void
  {
    $isExpired = self::$smReader->readMetadataByStateByKey('appointment_expired', 'is_expired');

    $this->assertEquals('1', $isExpired);
  }

  /**
   * @return void
   * @throws MissingMetadataKeyException
   */
  public function testReadMetadataByNotFoundKey(): void
  {
    $this->expectException(MissingMetadataKeyException::class);
    $this->expectExceptionMessageMatches('/metadata key not found/');
    self::$smReader->readMetadataByStateByKey('appointment_expired', 'not_found');
  }

  /**
   * @return void
   * @throws MissingMetadataKeyException
   */
  public function testReadMetadataBoolValueByKey(): void
  {
    $isExpired = self::$smReader->readMetadataBoolValue('appointment_expired', 'is_expired');

    $this->assertTrue($isExpired);
  }

  /**
   * @param string $stateName
   * @param int $index
   *
   * @return array
   */
  private function readTransitionAtStateByIndex(string $stateName, int $index): array
  {
    $transitions = self::$smReader->allTransitionsByState($stateName);
    return $transitions[$index];
  }

  /**
   * @param array $transitions
   * @param array $expectedStates
   *
   * @return void
   */
  private function assertTransitionsState(array $transitions, array $expectedStates): void
  {
    $states = array_column($transitions, 'state');
    $this->assertEquals($expectedStates, $states);
  }
}
