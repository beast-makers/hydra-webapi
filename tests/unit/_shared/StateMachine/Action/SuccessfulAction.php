<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine\Action;

use BeastMakers\Shared\StateMachine\Action;
use BeastMakers\Shared\StateMachine\StateMachine;

class SuccessfulAction extends Action
{
  /**
   * @inheritDoc
   */
  public static function execute(StateMachine $stateMachine, array $arguments = []): bool
  {
    return true;
  }
}
