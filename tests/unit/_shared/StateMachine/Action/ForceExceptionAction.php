<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine\Action;

use BeastMakers\Shared\StateMachine\Action;
use BeastMakers\Shared\StateMachine\StateMachine;

class ForceExceptionAction extends Action
{
  /**
   * @inheritDoc
   *
   * @throws ActionException
   */
  public static function execute(StateMachine $stateMachine, array $arguments = []): bool
  {
    throw new ActionException();
  }
}
