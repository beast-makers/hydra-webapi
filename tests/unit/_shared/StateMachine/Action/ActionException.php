<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine\Action;

class ActionException extends \Exception
{
}
