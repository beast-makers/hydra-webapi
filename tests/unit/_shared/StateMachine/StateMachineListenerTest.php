<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\StateMachine;
use BeastMakers\Shared\StateMachine\StateMachineListener;
use BeastMakers\Shared\StateMachine\StateMachineReader;
use BeastMakers\Shared\StateMachine\StateMachineRegistry;

class StateMachineListenerTest extends \Codeception\Test\Unit
{
  private static StateMachineReader $smReader;

  private const SM_ID = 'testing';
  private const DEFINITION_DIR = __DIR__ . '/fixtures/abstract_sm';

  /**
   * @throws \Exception
   */
  public static function setUpBeforeClass(): void
  {
    $smRegistry = new StateMachineRegistry();
    $smRegistry->register('force_fsm_properties', self::DEFINITION_DIR . '/force_fsm_properties.yml');
    $smRegistry->register('sub_flow', self::DEFINITION_DIR . '/sub_flow.yml');
    $smRegistry->register(self::SM_ID, self::DEFINITION_DIR . '/fsm.yml');
    self::$smReader = new StateMachineReader($smRegistry);
    self::$smReader->use(self::SM_ID);
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenTriggeredEvent(): void
  {
    $listenerMock = $this->createMock(StateMachineListener::class);
    $listenerMock->expects($this->once())
      ->method('triggeredEvent')
      ->with($this->isInstanceOf(StateMachine::class), 'trigger_event');
    $sm = $this->createStateMachine($listenerMock);

    $sm->setActiveStateName('wait_for_event');
    $sm->triggerEvent('trigger_event');
  }

  /**
   * @return void
   */
  public function testMoveForwardUnconditionally(): void
  {
    $listenerMock = $this->createMock(StateMachineListener::class);
    $listenerMock->expects($this->exactly(2))
      ->method('changedState')
      ->withConsecutive(
        [$this->isInstanceOf(StateMachine::class), 'start', 'goto_final'],
        [$this->isInstanceOf(StateMachine::class), 'goto_final', 'state_final'],
      );
    $sm = $this->createStateMachine($listenerMock);

    $sm->setActiveStateName('start');
    $sm->moveForward();
  }

  /**
   * @return void
   */
  public function testMoveForwardConditionally(): void
  {
    $listenerMock = $this->createMock(StateMachineListener::class);
    $listenerMock->expects($this->exactly(2))
      ->method('executedCondition')
      ->withConsecutive(
        [$this->isInstanceOf(StateMachine::class), $this->conditionClass('AlwaysFalseCondition'), [], false],
        [$this->isInstanceOf(StateMachine::class), $this->conditionClass('AlwaysTrueCondition'), [], true],
      );
    $sm = $this->createStateMachine($listenerMock);

    $sm->setActiveStateName('start_condition');
    $sm->moveForward();
  }

  /**
   * @return void
   */
  public function testMoveForwardConditionallyGivenConditionArgument(): void
  {
    $listenerMock = $this->createMock(StateMachineListener::class);
    $listenerMock->expects($this->exactly(2))
      ->method('executedCondition')
      ->withConsecutive(
        [$this->isInstanceOf(StateMachine::class), $this->conditionClass('ForceCondition'), ['false'], false],
        [$this->isInstanceOf(StateMachine::class), $this->conditionClass('ForceCondition'), ['true'], true],
      );
    $sm = $this->createStateMachine($listenerMock);

    $sm->setActiveStateName('start_condition_with_argument');
    $sm->moveForward();
  }

  /**
   * @return void
   */
  public function testMoveForwardExecutingAction(): void
  {
    $listenerMock = $this->createMock(StateMachineListener::class);
    $listenerMock->expects($this->once())
      ->method('executedAction')
      ->with($this->isInstanceOf(StateMachine::class), $this->actionClass('SuccessfulAction'), [], true);
    $sm = $this->createStateMachine($listenerMock);

    $sm->setActiveStateName('start_execute_action');
    $sm->moveForward();
  }

  /**
   * @param StateMachineListener $listener
   *
   * @return StateMachine
   */
  private function createStateMachine(StateMachineListener $listener): StateMachine
  {
    return new StateMachine(self::$smReader, $listener, uniqid(self::SM_ID, true));
  }

  /**
   * @param string $className
   *
   * @return string
   */
  private function conditionClass(string $className): string
  {
    return "BeastMakersTest\Unit\Shared\StateMachine\Condition\\{$className}";
  }

  /**
   * @param string $className
   *
   * @return string
   */
  private function actionClass(string $className): string
  {
    return "BeastMakersTest\Unit\Shared\StateMachine\Action\\{$className}";
  }
}
