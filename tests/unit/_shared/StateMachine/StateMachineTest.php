<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\Exception\MissingMetadataKeyException;
use BeastMakers\Shared\StateMachine\StateMachine;
use BeastMakers\Shared\StateMachine\StateMachineListener;
use BeastMakers\Shared\StateMachine\StateMachineReader;
use BeastMakers\Shared\StateMachine\StateMachineRegistry;
use BeastMakersTest\Unit\Shared\StateMachine\Action\ActionException;

class StateMachineTest extends \Codeception\Test\Unit
{
  private static StateMachine $sm;
  private static StateMachineReader $smReader;

  private const SM_ID = 'testing';
  private const DEFINITION_DIR = __DIR__ . '/fixtures/abstract_sm';

  /**
   * @throws \Exception
   */
  public static function setUpBeforeClass(): void
  {
    $smRegistry = new StateMachineRegistry();
    $smRegistry->register('force_fsm_properties', self::DEFINITION_DIR . '/force_fsm_properties.yml');
    $smRegistry->register('sub_flow', self::DEFINITION_DIR . '/sub_flow.yml');
    $smRegistry->register(self::SM_ID, self::DEFINITION_DIR . '/fsm.yml');
    self::$smReader = new StateMachineReader($smRegistry);
    self::$smReader->use(self::SM_ID);

    self::$sm = new StateMachine(self::$smReader, new StateMachineListener(), uniqid(self::SM_ID, true));
  }

  /**
   * @return void
   */
  public function testUnconditionalNextState(): void
  {
    self::$sm->setActiveStateName('start');
    $transition = self::$sm->findNextOpenTransition();
    $this->assertEquals('goto_final', self::$smReader->getStateNameFor($transition));
  }

  /**
   * @return void
   */
  public function testMoveForwardUnconditionally(): void
  {
    self::$sm->setActiveStateName('start');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardConditionally(): void
  {
    self::$sm->setActiveStateName('start_condition');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardConditionallyGivenConditionArgument(): void
  {
    self::$sm->setActiveStateName('start_condition_with_argument');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenTriggeredEvent(): void
  {
    self::$sm->setActiveStateName('wait_for_event');
    $landedStateName = self::$sm->triggerEvent('trigger_event');
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenTriggeredBlockedEvent(): void
  {
    self::$sm->setActiveStateName('wait_for_blocked_event');
    $landedStateName = self::$sm->triggerEvent('trigger_blocked_event');
    $this->assertEquals('wait_for_blocked_event', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenTriggeredConditionalEvent(): void
  {
    self::$sm->setActiveStateName('wait_for_conditional_event');
    $landedStateName = self::$sm->triggerEvent('trigger_conditional_event');
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardConditionAlternative(): void
  {
    self::$sm->setActiveStateName('start_alternative_path');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardExecutingAction(): void
  {
    self::$sm->setActiveStateName('start_execute_action');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardBlockExecutingAction(): void
  {
    self::$sm->setActiveStateName('start_blocked_action_execution');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('start_blocked_action_execution', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardExecutingFatalAction(): void
  {
    $this->expectException(ActionException::class);
    self::$sm->setActiveStateName('start_fatal_action_execution');
    self::$sm->moveForward();
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenExtendedFlow(): void
  {
    self::$sm->setActiveStateName('start_subflow');
    $landedStateName = self::$sm->moveForward();
    $this->assertEquals('state_final', $landedStateName);
  }

  /**
   * @return void
   * @throws MissingMetadataKeyException
   */
  public function testReadMetadataBool(): void
  {
    self::$sm->setActiveStateName('to_be_canceled');
    $isCancellable = self::$sm->readMetadataBoolValue('is_cancelable');
    $this->assertTrue($isCancellable);
  }

  /**
   * @return void
   */
  public function testMoveForwardGivenGlobalEvent(): void
  {
    self::$sm->setActiveStateName('cancelable');
    $landedStateName = self::$sm->triggerEvent('global_cancel');
    $this->assertEquals('canceled', $landedStateName);
  }

  /**
   * @return void
   */
  public function testMoveForwardBlockedGivenGlobalEvent(): void
  {
    self::$sm->setActiveStateName('not_cancelable');
    $landedStateName = self::$sm->triggerEvent('global_cancel');
    $this->assertEquals('not_cancelable', $landedStateName);
  }

  /**
   * @return void
   */
  public function testStateMachineStatusCodeGivenBlockedTransitionCondition(): void
  {
    self::$sm->setActiveStateName('not_cancelable');
    self::$sm->triggerEvent('global_cancel');
    $this->assertEquals('CANCEL_NOT_ALLOWED', self::$sm->getStatusCode());
  }
}
