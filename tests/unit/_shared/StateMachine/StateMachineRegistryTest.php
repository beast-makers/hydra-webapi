<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\StateMachineRegistry;

class StateMachineRegistryTest extends \Codeception\Test\Unit
{
  private static StateMachineRegistry $smRegistry;

  private const SM_ID = 'appointment';
  private const DEFINITION_FILE_PATH = __DIR__ . '/fixtures/appointment/appointment_fsm.yml';

  public static function setUpBeforeClass(): void
  {
    self::$smRegistry = new StateMachineRegistry();
    self::$smRegistry->register(self::SM_ID, self::DEFINITION_FILE_PATH);
  }

  /**
   * @return void
   * @throws \Exception
   */
  public function testReadStateMachine(): void
  {
    $appointmentSM = self::$smRegistry->read(self::SM_ID);

    $this->assertIsArray($appointmentSM);
    $this->assertArrayHasKey('flow', $appointmentSM);
    $this->assertArrayHasAllKeys([
      'start',
      'appointment_confirmation',
      'appointment_confirmed',
      'appointment_canceled',
      'appointment_expired',
      'call_visitor',
    ], $appointmentSM['flow']);
  }

  /*
   * @return void
   */
  public function testDefinitionNotFound(): void
  {
    $this->expectException(\Exception::class);
    self::$smRegistry->read('unknown');
  }

  /**
   * @param array $keys
   * @param array $input
   *
   * @return void
   */
  private function assertArrayHasAllKeys(array $keys, array $input): void
  {
    foreach ($keys as $key) {
      $this->assertArrayHasKey($key, $input);
    }
  }
}
