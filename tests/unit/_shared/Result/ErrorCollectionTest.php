<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\Result;

use BeastMakers\Shared\Result\CommonErrorCodes;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\ErrorCollection;

class ErrorCollectionTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   */
  public function testNoErrorAdded(): void
  {
    $errorCollection = new ErrorCollection();

    $this->assertEmpty($errorCollection->getErrors());
    $this->assertFalse($errorCollection->hasErrors());
  }

  /**
   * @return void
   */
  public function testErrorAdded(): void
  {
    $errorCollection = new ErrorCollection();

    $errorCollection->addError(new Error(CommonErrorCodes::INVALID_INPUT_DATA));

    $this->assertNotEmpty($errorCollection->getErrors());
    $this->assertTrue($errorCollection->hasErrors());
    $this->assertSame(CommonErrorCodes::INVALID_INPUT_DATA, $errorCollection->getFirstError()->errorCode);
  }

  /**
   * @return void
   */
  public function testCollectionMerge(): void
  {
    $errorCollectionA = new ErrorCollection();
    $errorCollectionA->addError(new Error('errorA1'));
    $errorCollectionA->addError(new Error('errorA2'));

    $errorCollectionB = new ErrorCollection();
    $errorCollectionB->addError(new Error('errorB1'));

    $errorCollectionA->merge($errorCollectionB);

    $this->assertNotEmpty($errorCollectionA->getErrors());
    $this->assertTrue($errorCollectionA->hasErrors());
    $this->assertCount(3, $errorCollectionA->getErrors());

    $expectedErrorCodes = ['errorA1', 'errorA2', 'errorB1'];
    $errorCodesAfterMerge = array_map(function (Error $error) {
      return $error->errorCode;
    }, $errorCollectionA->getErrors());

    $this->assertSame($expectedErrorCodes, $errorCodesAfterMerge);
  }
}
