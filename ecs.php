<?php
declare(strict_types=1);

use PhpCsFixer\Fixer as PhpCsFixer;
use PHP_CodeSniffer\Standards\Generic\Sniffs as CodeSnifferSniffs;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\ValueObject\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;
use Symplify\CodingStandard\Fixer as SymplifyFixer;

return static function (ContainerConfigurator $containerConfigurator): void {
  // A. standalone rule
  //$services = $containerConfigurator->services();
  //$services->set(ArraySyntaxFixer::class)
  //->call('configure', [[
  //'syntax' => 'short',
  //]]);

  // B. full sets
  $parameters = $containerConfigurator->parameters();
  $parameters->set(Option::SETS, [SetList::COMMON, SetList::CLEAN_CODE, SetList::PSR_12, SetList::PHP_71]);

  $parameters->set(Option::SKIP, [
    PhpCsFixer\ClassNotation\SelfAccessorFixer::class,
    PhpCsFixer\Operator\UnaryOperatorSpacesFixer::class,
    PhpCsFixer\Operator\NotOperatorWithSuccessorSpaceFixer::class,
    PhpCsFixer\Phpdoc\NoSuperfluousPhpdocTagsFixer::class,
    PhpCsFixer\Phpdoc\PhpdocLineSpanFixer::class,
    PhpCsFixer\Phpdoc\PhpdocNoEmptyReturnFixer::class,
    PhpCsFixer\PhpTag\BlankLineAfterOpeningTagFixer::class,
    PhpCsFixer\ClassNotation\ClassAttributesSeparationFixer::class,
    PhpCsFixer\ClassNotation\OrderedClassElementsFixer::class,
    PhpCsFixer\Strict\StrictParamFixer::class,
    PhpCsFixer\PhpUnit\PhpUnitStrictFixer::class,
    CodeSnifferSniffs\CodeAnalysis\AssignmentInConditionSniff::class,
    SymplifyFixer\ArrayNotation\ArrayListItemNewlineFixer::class,

    __DIR__ . '/tests/_support/_generated/',
  ]);

  $parameters->set(Option::INDENTATION, '  ');
};
