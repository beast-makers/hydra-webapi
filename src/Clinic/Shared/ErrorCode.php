<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Shared;

use BeastMakers\Shared\Result\CommonErrorCodes;

interface ErrorCode extends CommonErrorCodes
{
  public const CLINIC_NOT_FOUND = 'CLINIC_NOT_FOUND';
  public const CLINIC_WITH_THIS_ID_ALREADY_EXISTS = 'CLINIC_WITH_THIS_ID_ALREADY_EXISTS';
  public const CLINIC_VALIDATION = 'CLINIC_VALIDATION';
}
