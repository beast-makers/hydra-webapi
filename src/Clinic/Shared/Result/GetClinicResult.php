<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Shared\Result;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\Result\Result;

class GetClinicResult extends Result
{
  private ?Clinic $clinic = null;

  /**
   * @return Clinic|null
   */
  public function getClinic(): ?Clinic
  {
    return $this->clinic;
  }

  /**
   * @param Clinic $clinic
   */
  public function setClinic(Clinic $clinic): void
  {
    $this->clinic = $clinic;
  }
}
