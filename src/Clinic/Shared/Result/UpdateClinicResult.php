<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Shared\Result;

use BeastMakers\Shared\Result\Result;

class UpdateClinicResult extends Result
{
}
