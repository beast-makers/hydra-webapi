<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class Clinic extends ArrayDto
{
  public const ATTR_CLINIC_ID = 'clinic_id';
  public const ATTR_IS_ACTIVE = 'is_active';
  public const ATTR_DISPLAY_NAME = 'display_name';
  public const ATTR_CITY = 'city';
  public const ATTR_ZIP = 'zip';
  public const ATTR_STREET = 'street';
  public const ATTR_COUNTRY_CODE = 'country_code';
  public const ATTR_LATITUDE = 'latitude';
  public const ATTR_LONGITUDE = 'longitude';
  public const ATTR_ROLES = 'roles';

  public const ROLE_IPR = 'ipr';
  public const ROLE_3D_SCAN = '3d_scan';

  /**
   * @return string
   */
  public function getClinicId(): string
  {
    return $this->get(self::ATTR_CLINIC_ID, '');
  }

  /**
   * @param string $clinicId
   *
   * @return self
   */
  public function setClinicId(string $clinicId): self
  {
    $this->set(self::ATTR_CLINIC_ID, $clinicId);

    return $this;
  }

  /**
   * @return bool
   */
  public function getIsActive(): bool
  {
    return $this->get(self::ATTR_IS_ACTIVE, false);
  }

  /**
   * @param bool $isActive
   *
   * @return self
   */
  public function setIsActive(bool $isActive): self
  {
    $this->set(self::ATTR_IS_ACTIVE, $isActive);

    return $this;
  }

  /**
   * @return string
   */
  public function getDisplayName(): string
  {
    return $this->get(self::ATTR_DISPLAY_NAME, '');
  }

  /**
   * @param string $displayName
   *
   * @return self
   */
  public function setDisplayName(string $displayName): self
  {
    $this->set(self::ATTR_DISPLAY_NAME, $displayName);

    return $this;
  }

  /**
   * @return string
   */
  public function getCity(): string
  {
    return $this->get(self::ATTR_CITY, '');
  }

  /**
   * @param string $city
   *
   * @return self
   */
  public function setCity(string $city): self
  {
    $this->set(self::ATTR_CITY, $city);

    return $this;
  }

  /**
   * @return string
   */
  public function getZip(): string
  {
    return $this->get(self::ATTR_ZIP, '');
  }

  /**
   * @param string $zip
   *
   * @return self
   */
  public function setZip(string $zip): self
  {
    $this->set(self::ATTR_ZIP, $zip);

    return $this;
  }

  /**
   * @return string
   */
  public function getStreet(): string
  {
    return $this->get(self::ATTR_STREET, '');
  }

  /**
   * @param string $street
   *
   * @return self
   */
  public function setStreet(string $street): self
  {
    $this->set(self::ATTR_STREET, $street);

    return $this;
  }

  /**
   * @return string
   */
  public function getCountryCode(): string
  {
    return $this->get(self::ATTR_COUNTRY_CODE, '');
  }

  /**
   * @param string $countryCode
   *
   * @return self
   */
  public function setCountryCode(string $countryCode): self
  {
    $this->set(self::ATTR_COUNTRY_CODE, $countryCode);

    return $this;
  }

  /**
   * @return float
   */
  public function getLatitude(): float
  {
    return $this->get(self::ATTR_LATITUDE, 0);
  }

  /**
   * @param float $latitude
   *
   * @return self
   */
  public function setLatitude(float $latitude): self
  {
    $this->set(self::ATTR_LATITUDE, $latitude);

    return $this;
  }

  /**
   * @return float
   */
  public function getLongitude(): float
  {
    return $this->get(self::ATTR_LONGITUDE, 0);
  }

  /**
   * @param float $longitude
   *
   * @return self
   */
  public function setLongitude(float $longitude): self
  {
    $this->set(self::ATTR_LONGITUDE, $longitude);

    return $this;
  }

  /**
   * @return array
   */
  public function getRoles(): array
  {
    return $this->get(self::ATTR_ROLES, []);
  }

  /**
   * @param array $roles
   *
   * @return self
   */
  public function setRoles(array $roles): self
  {
    $this->set(self::ATTR_ROLES, $roles);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_CLINIC_ID => self::DEF_SCALAR,
      self::ATTR_IS_ACTIVE => self::DEF_BOOL,
      self::ATTR_DISPLAY_NAME => self::DEF_SCALAR,
      self::ATTR_CITY => self::DEF_SCALAR,
      self::ATTR_ZIP => self::DEF_SCALAR,
      self::ATTR_STREET => self::DEF_SCALAR,
      self::ATTR_COUNTRY_CODE => self::DEF_SCALAR,
      self::ATTR_LATITUDE => self::DEF_FLOAT,
      self::ATTR_LONGITUDE => self::DEF_FLOAT,
      self::ATTR_ROLES => self::DEF_ARRAY_UNIQUE,
    ];
  }
}
