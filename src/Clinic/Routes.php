<?php
declare(strict_types=1);

namespace BeastMakers\Clinic;

use BeastMakers\Clinic\Clinic\Action\Http\CreateClinic;
use BeastMakers\Clinic\Clinic\Action\Http\GetClinic;
use BeastMakers\Clinic\Clinic\Action\Http\UpdateClinic;
use BeastMakers\Shared\Kernel\Middleware\IsValidJsonMiddleware;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_GET = 'clinic.get';
  public const ROUTE_CREATE = 'clinic.create';
  public const ROUTE_UPDATE = 'clinic.update';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $isValidJsonMiddleware = new IsValidJsonMiddleware();

    $app->get('/clinic/{clinic_id}', GetClinic::class)
      ->setName(self::ROUTE_GET);

    $app->post('/clinic/{clinic_id}', CreateClinic::class)
      ->setName(self::ROUTE_CREATE)
      ->add($isValidJsonMiddleware);

    $app->put('/clinic/{clinic_id}', UpdateClinic::class)
      ->setName(self::ROUTE_UPDATE)
      ->add($isValidJsonMiddleware);
  }
}
