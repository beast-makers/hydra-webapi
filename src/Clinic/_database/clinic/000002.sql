--liquibase formatted sql

--changeset konradg:001
CREATE SEQUENCE IF NOT EXISTS clinic.seq_clinic AS BIGINT
    INCREMENT 3 START 1;
SELECT setval('clinic.seq_clinic', 1);

--changeset konradg:002
CREATE TABLE IF NOT EXISTS clinic.clinic (
  dbid BIGINT PRIMARY KEY DEFAULT nextval('clinic.seq_clinic'),
  clinic_id VARCHAR UNIQUE NOT NULL,
  is_active BOOLEAN NOT NULL,
  display_name VARCHAR,
  city VARCHAR,
  zip VARCHAR,
  street VARCHAR,
  country_code CHAR(2),
  latitude DOUBLE PRECISION,
  longitude DOUBLE PRECISION,
  roles text[],
  created_at TIMESTAMPTZ DEFAULT now(),
  updated_at TIMESTAMPTZ DEFAULT now()
);

--changeset konradg:003
ALTER SEQUENCE clinic.seq_clinic OWNED BY clinic.clinic.dbid;

--changeset konradg:004
CREATE INDEX IF NOT EXISTS clinic__clinic_id ON clinic.clinic USING BTREE (clinic_id);

--changset konradg:005
CREATE TRIGGER update_clinic_updated_at
    BEFORE UPDATE ON clinic.clinic
    FOR EACH ROW
EXECUTE PROCEDURE public.updated_at_timestamp_column();
