<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Storage;

use BeastMakers\Shared\DatabaseConnector\PostgreSQL\DbTable;

class ClinicTable extends DbTable
{
  public const COLUMN_CLINIC_ID = 'clinic_id';
  public const COLUMN_IS_ACTIVE = 'is_active';
  public const COLUMN_DISPLAY_NAME = 'display_name';
  public const COLUMN_CITY = 'city';
  public const COLUMN_ZIP = 'zip';
  public const COLUMN_STREET = 'street';
  public const COLUMN_COUNTRY_CODE = 'country_code';
  public const COLUMN_LATITUDE = 'latitude';
  public const COLUMN_LONGITUDE = 'longitude';
  public const COLUMN_ROLES = 'roles';

  protected const TYPES = [
    self::COLUMN_CLINIC_ID => self::TYPE_TEXT,
    self::COLUMN_IS_ACTIVE => self::TYPE_BOOL,
    self::COLUMN_DISPLAY_NAME => self::TYPE_TEXT,
    self::COLUMN_CITY => self::TYPE_TEXT,
    self::COLUMN_ZIP => self::TYPE_TEXT,
    self::COLUMN_STREET => self::TYPE_TEXT,
    self::COLUMN_COUNTRY_CODE => self::TYPE_TEXT,
    self::COLUMN_LATITUDE => self::TYPE_NUMERIC,
    self::COLUMN_LONGITUDE => self::TYPE_NUMERIC,
    self::COLUMN_ROLES => self::TYPE_TEXT_ARRAY,
  ];
}
