<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action;

use BeastMakers\Clinic\Clinic\Domain\Validator\UpdateClinicValidator;
use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Clinic\Infra\Repository\WriteRepository;
use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\Clinic\Shared\Result\UpdateClinicResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use Rakit\Validation\RuleNotFoundException;

class UpdateClinicService
{
  private UpdateClinicValidator $updateClinicValidator;

  private WriteRepository $clinicWriteRepository;

  private ReadRepository $clinicReadRepository;

  /**
   * @param UpdateClinicValidator $updateClinicValidator
   * @param ReadRepository $clinicReadRepository
   * @param WriteRepository $clinicWriteRepository
   */
  public function __construct(
    UpdateClinicValidator $updateClinicValidator,
    ReadRepository $clinicReadRepository,
    WriteRepository $clinicWriteRepository
  ) {
    $this->updateClinicValidator = $updateClinicValidator;
    $this->clinicReadRepository = $clinicReadRepository;
    $this->clinicWriteRepository = $clinicWriteRepository;
  }

  /**
   * @param Clinic $clinic
   *
   * @return UpdateClinicResult
   * @throws RuleNotFoundException
   * @throws \Throwable
   */
  public function updateClinic(Clinic $clinic): UpdateClinicResult
  {
    $result = new UpdateClinicResult();

    try {
      $existingClinic = $this->readExistingClinic($clinic->getClinicId());
      $mergedClinic = $existingClinic->merge($clinic);
      $this->updateClinicValidator->validateClinic($mergedClinic);
    } catch (ResultException $e) {
      $result->mergeWith($e->getResult());

      return $result;
    }

    $this->clinicWriteRepository->updateClinic($mergedClinic);
    $result->setIsSuccess(true);

    return $result;
  }

  /**
   * @param string $clinicId
   *
   * @return Clinic
   * @throws ResultException
   */
  private function readExistingClinic(string $clinicId): Clinic
  {
    $result = new Result();
    $existingClinic = $this->clinicReadRepository->findClinicById($clinicId);
    if (empty($existingClinic->getClinicId())) {
      $result->addError(new Error(ErrorCode::CLINIC_NOT_FOUND));
      throw new ResultException($result);
    }

    return $existingClinic;
  }
}
