<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action;

use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\Clinic\Shared\Result\GetClinicResult;
use BeastMakers\Shared\Result\Error;

class GetClinicService
{
  private ReadRepository $clinicReadRepository;

  /**
   * @param ReadRepository $clinicReadRepository
   */
  public function __construct(
    ReadRepository $clinicReadRepository
  ) {
    $this->clinicReadRepository = $clinicReadRepository;
  }

  /**
   * @param string $productId
   *
   * @return GetClinicResult
   */
  public function fetchClinic(string $productId): GetClinicResult
  {
    $result = new GetClinicResult();

    $clinic = $this->clinicReadRepository->findClinicById($productId);
    if ($clinic->isEmpty()) {
      $result->addError(new Error(ErrorCode::CLINIC_NOT_FOUND));
      return $result;
    }

    $result->setClinic($clinic);
    $result->setIsSuccess(true);

    return $result;
  }
}
