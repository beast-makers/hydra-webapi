<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action;

use BeastMakers\Clinic\Clinic\Domain\Validator\CreateClinicValidator;
use BeastMakers\Clinic\Clinic\Infra\Repository\WriteRepository;
use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Shared\Result\CreateClinicResult;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Result\ResultException;
use Rakit\Validation\RuleNotFoundException;

class CreateClinicService
{
  private CreateClinicValidator $createClinicValidator;

  private WriteRepository $clinicWriteRepository;

  /**
   * @param CreateClinicValidator $createClinicValidator
   * @param WriteRepository $clinicWriteRepository
   */
  public function __construct(
    CreateClinicValidator $createClinicValidator,
    WriteRepository $clinicWriteRepository
  ) {
    $this->createClinicValidator = $createClinicValidator;
    $this->clinicWriteRepository = $clinicWriteRepository;
  }

  /**
   * @param Clinic $clinic
   *
   * @return CreateClinicResult
   * @throws RuleNotFoundException
   * @throws DatabaseException
   */
  public function createClinic(Clinic $clinic): CreateClinicResult
  {
    $result = new CreateClinicResult();

    try {
      $this->createClinicValidator->validateClinic($clinic);
    } catch (ResultException $e) {
      $result->mergeWith($e->getResult());

      return $result;
    }

    $this->clinicWriteRepository->createClinic($clinic);
    $result->setIsSuccess(true);

    return $result;
  }
}
