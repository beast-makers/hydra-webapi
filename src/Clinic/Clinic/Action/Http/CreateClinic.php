<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action\Http;

use BeastMakers\Clinic\Clinic\Factory;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\RuleNotFoundException;

/**
 * @property Factory $factory
 */
class CreateClinic extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_ADMIN];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws DatabaseException
   * @throws ArrayValidationException
   * @throws RuleNotFoundException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $clinic = $this->factory->createClinicFromCreateClinicRequest($request);
    $service = $this->factory->newCreateClinicService();
    $result = $service->createClinic($clinic);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];
    $body = Stream::create('');

    return new Response(201, $headers, $body);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::INVALID_JSON_FORMAT => 400,
      ErrorCode::CLINIC_WITH_THIS_ID_ALREADY_EXISTS => 422,
      ErrorCode::CLINIC_VALIDATION => 400,
    ];
  }
}
