<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action\Http;

use BeastMakers\Clinic\Clinic\Factory;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class GetClinic extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_READ_ONLY];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $service = $this->factory->newGetClinicService();
    $clinicId = $request->getAttribute('clinic_id', '');
    $result = $service->fetchClinic($clinicId);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];
    /** @psalm-suppress PossiblyNullReference */
    $body = Stream::create(json_encode($result->getClinic()->toArray()));

    return new Response(200, $headers, $body);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::CLINIC_NOT_FOUND => 404,
    ];
  }
}
