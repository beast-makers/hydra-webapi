<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Action\Http;

use BeastMakers\Clinic\Clinic\Factory;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\RuleNotFoundException;

/**
 * @property Factory $factory
 */
class UpdateClinic extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_ADMIN];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws ArrayValidationException
   * @throws RuleNotFoundException
   * @throws \Throwable
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $clinic = $this->factory->createClinicFromUpdatedClinicRequest($request);
    $service = $this->factory->newUpdateClinicService();
    $result = $service->updateClinic($clinic);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];
    $body = Stream::create('');

    return new Response(200, $headers, $body);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::INVALID_INPUT_DATA => 400,
      ErrorCode::CLINIC_NOT_FOUND => 404,
      ErrorCode::CLINIC_VALIDATION => 400,
    ];
  }
}
