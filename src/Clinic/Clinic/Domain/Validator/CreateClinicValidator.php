<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Domain\Validator;

use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\Clinic\Shared\Result\ClinicValidationResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class CreateClinicValidator
{
  private ClinicValidator $clinicValidator;

  private ReadRepository $clinicReadRepository;

  /**
   * @param ClinicValidator $clinicValidator
   * @param ReadRepository $clinicReadRepository
   */
  public function __construct(
    ClinicValidator $clinicValidator,
    ReadRepository $clinicReadRepository
  ) {
    $this->clinicValidator = $clinicValidator;
    $this->clinicReadRepository = $clinicReadRepository;
  }

  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws ResultException
   * @throws RuleNotFoundException
   */
  public function validateClinic(Clinic $clinic): void
  {
    $validateClinicResult = new ClinicValidationResult();

    try {
      $this->verifyClinicIsUnique($clinic);
      $this->clinicValidator->validate($clinic);
    } catch (ArrayValidationException $e) {
      $validateClinicResult->addError(new Error(ErrorCode::CLINIC_VALIDATION, $e->getErrors()));
      throw new ResultException($validateClinicResult);
    }
  }

  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws ResultException
   */
  private function verifyClinicIsUnique(Clinic $clinic): void
  {
    $result = new Result();

    $existingClinic = $this->clinicReadRepository->findClinicById($clinic->getClinicId());
    if (!empty($existingClinic->getClinicId())) {
      $result->addError(new Error(ErrorCode::CLINIC_WITH_THIS_ID_ALREADY_EXISTS));

      throw new ResultException($result);
    }
  }
}
