<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Domain\Validator;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class ClinicValidator
{
  private ArrayValidator $validator;

  private array $activeRules = [];
  private array $inActiveRules = [];

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws ArrayValidationException
   * @throws RuleNotFoundException
   */
  public function validate(Clinic $clinic): void
  {
    $clinicAsArray = $clinic->toArray();

    if ($clinic->getIsActive()) {
      $rules = $this->defineValidationRules_Active();
    } else {
      $rules = $this->defineValidationRules_Inactive();
    }

    $this->validator->validate($clinicAsArray, $rules);
  }

  /**
   * @return array
   * @throws RuleNotFoundException
   */
  private function defineValidationRules_Active(): array
  {
    if (empty($this->activeRules)) {
      $v = $this->validator->rakit();

      $invalidCharacters = self::invalidCharacters();
      $allowedRoles = self::allowedRoles();

      $this->activeRules = [
        Clinic::ATTR_CLINIC_ID => 'required|alpha_dash',
        Clinic::ATTR_IS_ACTIVE => 'required|boolean',
        Clinic::ATTR_DISPLAY_NAME => 'required',
        Clinic::ATTR_CITY => ['required', $v('not_regex', $invalidCharacters)],
        Clinic::ATTR_ZIP => 'required',
        Clinic::ATTR_STREET => 'required',
        Clinic::ATTR_COUNTRY_CODE => 'required|country_code_alpha2',
        Clinic::ATTR_LATITUDE => 'required|numeric',
        Clinic::ATTR_LONGITUDE => 'required|numeric',
        Clinic::ATTR_ROLES => "required|array|all_in:{$allowedRoles}",
      ];
    }

    return $this->activeRules;
  }

  /**
   * @return array
   * @throws RuleNotFoundException
   */
  private function defineValidationRules_Inactive(): array
  {
    if (empty($this->inActiveRules)) {
      $v = $this->validator->rakit();

      $invalidCharacters = self::invalidCharacters();
      $allowedRoles = self::allowedRoles();

      $this->inActiveRules = [
        Clinic::ATTR_CLINIC_ID => 'required|alpha_dash',
        Clinic::ATTR_CITY => $v('not_regex', $invalidCharacters),
        Clinic::ATTR_COUNTRY_CODE => 'country_code_alpha2',
        Clinic::ATTR_LATITUDE => 'numeric',
        Clinic::ATTR_LONGITUDE => 'numeric',
        Clinic::ATTR_ROLES => "array|all_in:{$allowedRoles}",
      ];
    }

    return $this->inActiveRules;
  }

  /**
   * @return string
   */
  private function invalidCharacters(): string
  {
    return '/[0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]/';
  }

  /**
   * @return string
   */
  private static function allowedRoles(): string
  {
    return implode(',', [
      Clinic::ROLE_IPR,
      Clinic::ROLE_3D_SCAN,
    ]);
  }
}
