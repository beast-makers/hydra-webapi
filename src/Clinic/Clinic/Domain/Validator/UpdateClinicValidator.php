<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Domain\Validator;

use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Shared\ErrorCode;
use BeastMakers\Clinic\Shared\Result\ClinicValidationResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class UpdateClinicValidator
{
  private ClinicValidator $clinicValidator;

  private ReadRepository $clinicReadRepository;

  /**
   * @param ClinicValidator $clinicValidator
   * @param ReadRepository $clinicReadRepository
   */
  public function __construct(
    ClinicValidator $clinicValidator,
    ReadRepository $clinicReadRepository
  ) {
    $this->clinicValidator = $clinicValidator;
    $this->clinicReadRepository = $clinicReadRepository;
  }

  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws ResultException
   * @throws RuleNotFoundException
   */
  public function validateClinic(Clinic $clinic): void
  {
    $validateClinicResult = new ClinicValidationResult();

    try {
      $this->verifyClinicExists($clinic->getClinicId());
      $this->clinicValidator->validate($clinic);
    } catch (ArrayValidationException $e) {
      $validateClinicResult->addError(new Error(ErrorCode::CLINIC_VALIDATION, $e->getErrors()));
      throw new ResultException($validateClinicResult);
    }
  }

  /**
   * @param string $clinicId
   *
   * @return void
   * @throws ResultException
   */
  public function verifyClinicExists(string $clinicId): void
  {
    $result = new Result();

    $existingClinic = $this->clinicReadRepository->findClinicById($clinicId);
    if ($existingClinic->isEmpty()) {
      $result->addError(new Error(ErrorCode::CLINIC_NOT_FOUND));

      throw new ResultException($result);
    }
  }
}
