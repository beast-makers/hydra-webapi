<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic;

use BeastMakers\Clinic\Clinic\Action\CreateClinicService;
use BeastMakers\Clinic\Clinic\Action\GetClinicService;
use BeastMakers\Clinic\Clinic\Action\UpdateClinicService;
use BeastMakers\Clinic\Clinic\Domain\Validator\ClinicValidator;
use BeastMakers\Clinic\Clinic\Domain\Validator\CreateClinicValidator;
use BeastMakers\Clinic\Clinic\Domain\Validator\UpdateClinicValidator;
use BeastMakers\Clinic\Clinic\Infra\Repository\ClinicReadRepository;
use BeastMakers\Clinic\Clinic\Infra\Repository\ClinicWriteRepository;
use BeastMakers\Clinic\Clinic\Infra\Repository\ReadRepository;
use BeastMakers\Clinic\Clinic\Infra\Repository\WriteRepository;
use BeastMakers\Clinic\Clinic\Infra\Request\CreateClinicRequest;
use BeastMakers\Clinic\Clinic\Infra\Request\UpdateClinicRequest;
use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @param ServerRequestInterface $request
   * @return Clinic
   *
   * @throws ArrayValidationException
   */
  public function createClinicFromCreateClinicRequest(ServerRequestInterface $request): Clinic
  {
    $createClinicRequest = new CreateClinicRequest($this->dependencyProvider->shareArrayValidator());

    return $createClinicRequest->createClinicFromRequest($request);
  }

  /**
   * @param ServerRequestInterface $request
   * @return Clinic
   *
   * @throws ArrayValidationException
   */
  public function createClinicFromUpdatedClinicRequest(ServerRequestInterface $request): Clinic
  {
    $updateClinicRequest = new UpdateClinicRequest($this->dependencyProvider->shareArrayValidator());

    return $updateClinicRequest->createClinicFromRequest($request);
  }

  /**
   * @return CreateClinicService
   */
  public function newCreateClinicService(): CreateClinicService
  {
    return new CreateClinicService(
      $this->shareCreateClinicValidator(),
      $this->shareClinicWriteRepository()
    );
  }

  /**
   * @return GetClinicService
   */
  public function newGetClinicService(): GetClinicService
  {
    return new GetClinicService(
      $this->shareClinicReadRepository()
    );
  }

  /**
   * @return UpdateClinicService
   */
  public function newUpdateClinicService(): UpdateClinicService
  {
    return new UpdateClinicService(
      $this->shareUpdateClinicValidator(),
      $this->shareClinicReadRepository(),
      $this->shareClinicWriteRepository()
    );
  }

  /**
   * @return CreateClinicValidator
   */
  protected function newCreateClinicValidator(): CreateClinicValidator
  {
    return new CreateClinicValidator($this->shareClinicValidator(), $this->shareClinicReadRepository());
  }

  /**
   * @return UpdateClinicValidator
   */
  protected function newUpdateClinicValidator(): UpdateClinicValidator
  {
    return new UpdateClinicValidator($this->shareClinicValidator(), $this->shareClinicReadRepository());
  }

  /**
   * @return ClinicValidator
   */
  protected function newClinicValidator(): ClinicValidator
  {
    return new ClinicValidator($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return ReadRepository
   */
  protected function newClinicReadRepository(): ReadRepository
  {
    return new ClinicReadRepository($this->dependencyProvider->loadWebApiQueryRunner());
  }

  /**
   * @return WriteRepository
   */
  protected function newClinicWriteRepository(): WriteRepository
  {
    return new ClinicWriteRepository($this->dependencyProvider->loadWebApiQueryRunner());
  }

  /**
   * @return CreateClinicValidator
   */
  protected function shareCreateClinicValidator(): CreateClinicValidator
  {
    return $this->shareInstance(CreateClinicValidator::class, static function (Factory $factory) {
      return $factory->newCreateClinicValidator();
    });
  }

  /**
   * @return UpdateClinicValidator
   */
  protected function shareUpdateClinicValidator(): UpdateClinicValidator
  {
    return $this->shareInstance(UpdateClinicValidator::class, static function (Factory $factory) {
      return $factory->newUpdateClinicValidator();
    });
  }

  /**
   * @return ClinicValidator
   */
  protected function shareClinicValidator(): ClinicValidator
  {
    return $this->shareInstance(ClinicValidator::class, static function (Factory $factory) {
      return $factory->newClinicValidator();
    });
  }

  /**
   * @return ReadRepository
   */
  protected function shareClinicReadRepository(): ReadRepository
  {
    return $this->shareInstance(ClinicReadRepository::class, static function (Factory $factory) {
      return $factory->newClinicReadRepository();
    });
  }

  /**
   * @return WriteRepository
   */
  protected function shareClinicWriteRepository(): WriteRepository
  {
    return $this->shareInstance(ClinicWriteRepository::class, static function (Factory $factory) {
      return $factory->newClinicWriteRepository();
    });
  }
}
