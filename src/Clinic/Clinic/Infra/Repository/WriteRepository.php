<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Repository;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;

interface WriteRepository
{
  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws DatabaseException
   */
  public function createClinic(Clinic $clinic): void;

  /**
   * @param Clinic $clinic
   *
   * @return void
   * @throws DatabaseException
   */
  public function updateClinic(Clinic $clinic): void;
}
