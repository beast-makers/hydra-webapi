<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Repository;

use BeastMakers\Clinic\Shared\Domain\Clinic;

interface ReadRepository
{
  /**
   * @param string $clinicId
   *
   * @return Clinic
   */
  public function findClinicById(string $clinicId): Clinic;
}
