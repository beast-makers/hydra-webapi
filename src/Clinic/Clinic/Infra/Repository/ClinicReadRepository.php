<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Repository;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Storage\ClinicTable as ct;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class ClinicReadRepository implements ReadRepository
{
  private QueryRunner $queryRunner;

  public function __construct(QueryRunner $queryRunner)
  {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->connection()->connectOnce();
  }

  /**
   * @param string $clinicId
   *
   * @return Clinic
   * @throws DatabaseException
   */
  public function findClinicById(string $clinicId): Clinic
  {
    $sql = <<<'SQL'
SELECT clinic.*
    FROM clinic.clinic clinic
    WHERE clinic.clinic_id = :clinic_id
SQL;

    /** @var resource $result */
    $result = $this->queryRunner->runQuery($sql, 'find clinic by clinicId', [
      'clinic_id' => ct::escapeLiteral($clinicId, ct::COLUMN_CLINIC_ID),
    ]);

    return $this->mapFindClinicResult($result);
  }

  /**
   * @param $queryResult
   *
   * @return Clinic
   */
  private function mapFindClinicResult($queryResult): Clinic
  {
    $clinic = new Clinic();
    $dbRecord = pg_fetch_assoc($queryResult);

    if ($dbRecord) {
      $clinic->mapArrayToDto($dbRecord, $this->columnsToClinicMap());

      if (!empty($dbRecord['roles'])) {
        $clinic->setRoles(ct::dbArrayToPhpArray($dbRecord['roles']));
      }
    }

    return $clinic;
  }

  /**
   * @return array
   */
  private function columnsToClinicMap(): array
  {
    return [
      ct::COLUMN_CLINIC_ID => Clinic::ATTR_CLINIC_ID,
      ct::COLUMN_IS_ACTIVE => Clinic::ATTR_IS_ACTIVE,
      ct::COLUMN_DISPLAY_NAME => Clinic::ATTR_DISPLAY_NAME,
      ct::COLUMN_CITY => Clinic::ATTR_CITY,
      ct::COLUMN_ZIP => Clinic::ATTR_ZIP,
      ct::COLUMN_STREET => Clinic::ATTR_STREET,
      ct::COLUMN_COUNTRY_CODE => Clinic::ATTR_COUNTRY_CODE,
      ct::COLUMN_ROLES => Clinic::ATTR_ROLES,
      ct::COLUMN_LATITUDE => Clinic::ATTR_LATITUDE,
      ct::COLUMN_LONGITUDE => Clinic::ATTR_LONGITUDE,
    ];
  }
}
