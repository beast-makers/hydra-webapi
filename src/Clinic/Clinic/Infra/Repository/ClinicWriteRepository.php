<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Repository;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Clinic\Storage\ClinicTable as ct;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class ClinicWriteRepository implements WriteRepository
{
  private QueryRunner $queryRunner;

  /**
   * @param QueryRunner $queryRunner
   */
  public function __construct(QueryRunner $queryRunner)
  {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->useWriteConnection();
  }

  /**
   * @inheritDoc
   * @throws \Throwable
   */
  public function createClinic(Clinic $clinic): void
  {
    $this->queryRunner->connection()->runInTransaction(function () use ($clinic): void {
      $this->createClinicRecord($clinic);
    });
  }

  /**
   * @inheritDoc
   * @throws \Throwable
   */
  public function updateClinic(Clinic $clinic): void
  {
    $this->queryRunner->connection()->runInTransaction(function () use ($clinic): void {
      $this->updateClinicRecord($clinic);
    });
  }

  /**
   * @param Clinic $clinic
   *
   * @return string
   * @throws DatabaseException
   */
  private function createClinicRecord(Clinic $clinic): string
  {
    $columnValueMap = $clinic->mapDtoToArray($this->clinicToColumnsMap());
    $listPair = ct::prepareInsertLists($columnValueMap);

    $sql = <<<SQL
INSERT INTO clinic.clinic ({$listPair->getIdentifiers()})
    VALUES ({$listPair->getValues()})
RETURNING dbid;
SQL;
    $result = $this->queryRunner->runQuery($sql, 'create clinic');

    return pg_fetch_assoc($result)['dbid'];
  }

  /**
   * @param Clinic $clinic
   *
   * @return string
   * @throws DatabaseException
   */
  private function updateClinicRecord(Clinic $clinic): string
  {
    $columnValueMap = $clinic->mapDtoToArray($this->clinicToColumnsMap());
    $updateList = ct::prepareUpdateStatementList($columnValueMap);

    $sql = "UPDATE clinic.clinic SET {$updateList} WHERE clinic_id=:clinic_id RETURNING dbid;";
    $result = $this->queryRunner->runQuery($sql, 'update clinic', [
      'clinic_id' => ct::escapeLiteral($clinic->getClinicId(), ct::COLUMN_CLINIC_ID),
    ]);

    return pg_fetch_assoc($result)['dbid'];
  }

  /**
   * @return array
   */
  private function clinicToColumnsMap(): array
  {
    return [
      Clinic::ATTR_CLINIC_ID => ct::COLUMN_CLINIC_ID,
      Clinic::ATTR_IS_ACTIVE => ct::COLUMN_IS_ACTIVE,
      Clinic::ATTR_DISPLAY_NAME => ct::COLUMN_DISPLAY_NAME,
      Clinic::ATTR_CITY => ct::COLUMN_CITY,
      Clinic::ATTR_ZIP => ct::COLUMN_ZIP,
      Clinic::ATTR_STREET => ct::COLUMN_STREET,
      Clinic::ATTR_COUNTRY_CODE => ct::COLUMN_COUNTRY_CODE,
      Clinic::ATTR_ROLES => ct::COLUMN_ROLES,
      Clinic::ATTR_LATITUDE => ct::COLUMN_LATITUDE,
      Clinic::ATTR_LONGITUDE => ct::COLUMN_LONGITUDE,
    ];
  }
}
