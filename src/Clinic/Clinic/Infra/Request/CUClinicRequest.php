<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Request;

use BeastMakers\Clinic\Shared\Domain\Clinic;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use GuzzleHttp\Utils;
use Psr\Http\Message\ServerRequestInterface;

abstract class CUClinicRequest
{
  public const PARAM_CLINIC_ID = 'clinic_id';
  public const PARAM_IS_ACTIVE = 'is_active';
  public const PARAM_DISPLAY_NAME = 'display_name';
  public const PARAM_CITY = 'city';
  public const PARAM_ZIP = 'zip';
  public const PARAM_STREET = 'street';
  public const PARAM_COUNTRY_CODE = 'country_code';
  public const PARAM_LATITUDE = 'latitude';
  public const PARAM_LONGITUDE = 'longitude';
  public const PARAM_ROLES = 'roles';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return Clinic
   * @throws ArrayValidationException
   */
  public function createClinicFromRequest(ServerRequestInterface $request): Clinic
  {
    /** @var array $params */
    $params = Utils::jsonDecode($request->getBody()->getContents(), true);
    $params[self::PARAM_CLINIC_ID] = $request->getAttribute(self::PARAM_CLINIC_ID);

    $request->getBody()->rewind();

    return $this->createClinicFromArray($params);
  }

  /**
   * @param array $input
   *
   * @return Clinic
   * @throws ArrayValidationException
   */
  private function createClinicFromArray(array $input): Clinic
  {
    $this->validateParams($input);
    $map = $this->defineRequestParamsToClinicMap();

    return Clinic::mapArrayToNewDto($input, $map);
  }

  /**
   * @param array $params
   *
   * @return void
   * @throws ArrayValidationException
   */
  private function validateParams(array $params): void
  {
    $this->validator->validate($params, $this->defineValidationRules());
  }

  /**
   * @return array
   */
  abstract protected function defineValidationRules(): array;

  /**
   * @return array
   */
  private function defineRequestParamsToClinicMap(): array
  {
    return [
      self::PARAM_CLINIC_ID => Clinic::ATTR_CLINIC_ID,
      self::PARAM_IS_ACTIVE => Clinic::ATTR_IS_ACTIVE,
      self::PARAM_DISPLAY_NAME => Clinic::ATTR_DISPLAY_NAME,
      self::PARAM_CITY => Clinic::ATTR_CITY,
      self::PARAM_ZIP => Clinic::ATTR_ZIP,
      self::PARAM_STREET => Clinic::ATTR_STREET,
      self::PARAM_COUNTRY_CODE => Clinic::ATTR_COUNTRY_CODE,
      self::PARAM_LATITUDE => Clinic::ATTR_LATITUDE,
      self::PARAM_LONGITUDE => Clinic::ATTR_LONGITUDE,
      self::PARAM_ROLES => Clinic::ATTR_ROLES,
    ];
  }
}
