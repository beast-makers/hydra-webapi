<?php
declare(strict_types=1);

namespace BeastMakers\Clinic\Clinic\Infra\Request;

class CreateClinicRequest extends CUClinicRequest
{
  /**
   * @inheritDoc
   */
  protected function defineValidationRules(): array
  {
    return [
      self::PARAM_CLINIC_ID => 'required',
      self::PARAM_IS_ACTIVE => 'nullable',
      self::PARAM_DISPLAY_NAME => 'nullable',
      self::PARAM_CITY => 'nullable',
      self::PARAM_ZIP => 'nullable',
      self::PARAM_STREET => 'nullable',
      self::PARAM_COUNTRY_CODE => 'nullable',
      self::PARAM_LATITUDE => 'nullable',
      self::PARAM_LONGITUDE => 'nullable',
      self::PARAM_ROLES => 'nullable|array',
    ];
  }
}
