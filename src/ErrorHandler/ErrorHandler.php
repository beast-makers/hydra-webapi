<?php
declare(strict_types=1);

namespace BeastMakers\ErrorHandler;

use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use Throwable;

class ErrorHandler
{
  /**
   * @param ServerRequestInterface $request
   * @param Throwable $exception
   * @param bool $displayErrorDetails
   * @param bool $logErrors
   * @param bool $logErrorDetails
   *
   * @return ResponseInterface
   * @throws Throwable
   */
  public static function handleError(
    ServerRequestInterface $request,
    Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
  ): ResponseInterface {
    if ($exception instanceof HttpNotFoundException) {
      return self::createNotFoundResponse();
    }

    if ($exception instanceof ArrayValidationException) {
      return self::createArrayValidationErrorResponse($exception);
    }

    throw $exception;
  }

  /**
   * @return Response
   */
  private static function createNotFoundResponse(): Response
  {
    $body = Stream::create('Not Found');

    return new Response(404, [], $body);
  }

  /**
   * @param ArrayValidationException $exception
   *
   * @return Response
   */
  private static function createArrayValidationErrorResponse(ArrayValidationException $exception): Response
  {
    $headers = [
      'Content-type' => 'application/json',
    ];

    $body = Stream::create(json_encode($exception->getErrors()));

    return new Response(400, $headers, $body);
  }
}
