<?php
declare(strict_types=1);

namespace BeastMakers\Customer;

use BeastMakers\Customer\Address\Action\Http\ValidateAddress;
use BeastMakers\Customer\Patient\Action\Http\ValidatePatient;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_VALIDATE_CUSTOMER = 'customer.validate';
  public const ROUTE_VALIDATE_ADDRESS = 'address.validate';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $app->post('/customer/validate', ValidatePatient::class)
      ->setName(self::ROUTE_VALIDATE_CUSTOMER);

    $app->post('/customer/validate-address', ValidateAddress::class)
      ->setName(self::ROUTE_VALIDATE_ADDRESS);
  }
}
