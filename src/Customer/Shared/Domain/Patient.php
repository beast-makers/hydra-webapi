<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;
use DateTime;

class Patient extends ArrayDto
{
  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_LAST_NAME = 'last_name';
  public const ATTR_EMAIL = 'email';
  public const ATTR_BIRTH_DATE = 'birth_date';

  /**
   * @return string
   */
  public function getFirstName(): string
  {
    return $this->get(self::ATTR_FIRST_NAME, '');
  }

  /**
   * @param string $firstName
   *
   * @return self
   */
  public function setFirstName(string $firstName): self
  {
    $this->set(self::ATTR_FIRST_NAME, $firstName);

    return $this;
  }

  /**
   * @return string
   */
  public function getLastName(): string
  {
    return $this->get(self::ATTR_LAST_NAME, '');
  }

  /**
   * @param string $lastName
   *
   * @return self
   */
  public function setLastName(string $lastName): self
  {
    $this->set(self::ATTR_LAST_NAME, $lastName);

    return $this;
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->get(self::ATTR_EMAIL, '');
  }

  /**
   * @param string $email
   *
   * @return self
   */
  public function setEmail(string $email): self
  {
    $this->set(self::ATTR_EMAIL, $email);

    return $this;
  }

  /**
   * @return DateTime|null
   */
  public function getBirthdate(): ?DateTime
  {
    return $this->get(self::ATTR_BIRTH_DATE, null);
  }

  /**
   * @param string $birthdate
   *
   * @return self
   */
  public function setBirthdate(string $birthdate): self
  {
    $this->set(self::ATTR_BIRTH_DATE, $birthdate);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_FIRST_NAME => self::DEF_SCALAR,
      self::ATTR_LAST_NAME => self::DEF_SCALAR,
      self::ATTR_EMAIL => self::DEF_SCALAR,
      self::ATTR_BIRTH_DATE => self::DEF_SCALAR,
    ];
  }
}
