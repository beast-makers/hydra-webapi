<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class Address extends ArrayDto
{
  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_LAST_NAME = 'last_name';
  public const ATTR_COUNTRY_CODE = 'country_code';
  public const ATTR_STREET = 'street';
  public const ATTR_ADDRESS_EXTENSION = 'address_extension';
  public const ATTR_CITY = 'city';
  public const ATTR_ZIP = 'zip';

  /**
   * @return string
   */
  public function getFirstName(): string
  {
    return $this->get(self::ATTR_FIRST_NAME, '');
  }

  /**
   * @param string $firstName
   *
   * @return Address
   */
  public function setFirstName(string $firstName): self
  {
    $this->set(self::ATTR_FIRST_NAME, $firstName);

    return $this;
  }

  /**
   * @return string
   */
  public function getLastName(): string
  {
    return $this->get(self::ATTR_LAST_NAME, '');
  }

  /**
   * @param string $lastName
   *
   * @return Address
   */
  public function setLastName(string $lastName): self
  {
    $this->set(self::ATTR_LAST_NAME, $lastName);

    return $this;
  }

  /**
   * @return string
   */
  public function getCountryCode(): string
  {
    return $this->get(self::ATTR_COUNTRY_CODE, '');
  }

  /**
   * @param string $countryCode
   *
   * @return Address
   */
  public function setCountryCode(string $countryCode): self
  {
    $this->set(self::ATTR_COUNTRY_CODE, $countryCode);

    return $this;
  }

  /**
   * @return string
   */
  public function getStreet(): string
  {
    return $this->get(self::ATTR_STREET, '');
  }

  /**
   * @param string $street
   *
   * @return Address
   */
  public function setStreet(string $street): self
  {
    $this->set(self::ATTR_STREET, $street);

    return $this;
  }

  /**
   * @return string
   */
  public function getAddressExtension(): string
  {
    return $this->get(self::ATTR_ADDRESS_EXTENSION, '');
  }

  /**
   * @param string $addressExtension
   *
   * @return Address
   */
  public function setAddressExtension(string $addressExtension): self
  {
    $this->set(self::ATTR_ADDRESS_EXTENSION, $addressExtension);

    return $this;
  }

  /**
   * @return string
   */
  public function getCity(): string
  {
    return $this->get(self::ATTR_CITY, '');
  }

  /**
   * @param string $city
   *
   * @return Address
   */
  public function setCity(string $city): self
  {
    $this->set(self::ATTR_CITY, $city);

    return $this;
  }

  /**
   * @return string
   */
  public function getZip(): string
  {
    return $this->get(self::ATTR_ZIP, '');
  }

  /**
   * @param string $zip
   *
   * @return Address
   */
  public function setZip(string $zip): self
  {
    $this->set(self::ATTR_ZIP, $zip);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_FIRST_NAME => self::DEF_SCALAR,
      self::ATTR_LAST_NAME => self::DEF_SCALAR,
      self::ATTR_COUNTRY_CODE => self::DEF_SCALAR,
      self::ATTR_STREET => self::DEF_SCALAR,
      self::ATTR_ADDRESS_EXTENSION => self::DEF_SCALAR,
      self::ATTR_CITY => self::DEF_SCALAR,
      self::ATTR_ZIP => self::DEF_SCALAR,
    ];
  }
}
