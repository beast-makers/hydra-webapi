<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Shared;

use BeastMakers\Shared\Result\CommonErrorCodes;

interface ErrorCode extends CommonErrorCodes
{
  public const ADDRESS_VALIDATION = 'ADDRESS_VALIDATION';
  public const PATIENT_VALIDATION = 'PATIENT_VALIDATION';
}
