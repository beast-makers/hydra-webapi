<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Address;

use BeastMakers\Customer\Address\Action\ValidateAddressService;
use BeastMakers\Customer\Address\Domain\Validator\AddressValidator;
use BeastMakers\Customer\Address\Infra\Request\ValidateAddressRequest;
use BeastMakers\Customer\Shared\Domain\Address;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return ValidateAddressService
   */
  public function newValidateAddressService(): ValidateAddressService
  {
    return new ValidateAddressService($this->shareAddressValidator());
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return Address
   * @throws ArrayValidationException
   */
  public function createAddressFromAddressValidationRequest(ServerRequestInterface $request): Address
  {
    $validateAddressRequest = new ValidateAddressRequest($this->dependencyProvider->shareArrayValidator());

    return $validateAddressRequest->createAddressFromRequest($request);
  }

  /**
   * @return AddressValidator
   */
  protected function newAddressValidator(): AddressValidator
  {
    return new AddressValidator($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return AddressValidator
   */
  protected function shareAddressValidator(): AddressValidator
  {
    return $this->shareInstance(AddressValidator::class, static function (Factory $factory) {
      return $factory->newAddressValidator();
    });
  }
}
