<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Address\Infra\Request;

use BeastMakers\Customer\Shared\Domain\Address;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class ValidateAddressRequest
{
  private const PARAM_FIRST_NAME = 'first_name';
  private const PARAM_LAST_NAME = 'last_name';
  private const PARAM_COUNTRY_CODE = 'country_code';
  private const PARAM_STREET = 'street';
  private const PARAM_ADDRESS_EXTENSION = 'address_extension';
  private const PARAM_CITY = 'city';
  private const PARAM_ZIP = 'zip';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return Address
   * @throws ArrayValidationException
   */
  public function createAddressFromRequest(ServerRequestInterface $request): Address
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $request->getBody()->rewind();

    $this->validateParams($params);
    $map = self::defineRequestParamsToAddressMap();

    return Address::mapArrayToNewDto($params, $map);
  }

  /**
   * @param array $params
   *
   * @return void
   * @throws ArrayValidationException
   */
  private function validateParams(array $params): void
  {
    $this->validator->validate($params, static::defineValidationRules());
  }

  /**
   * @return array
   */
  private static function defineValidationRules(): array
  {
    return [
      self::PARAM_FIRST_NAME => 'required',
      self::PARAM_LAST_NAME => 'required',
      self::PARAM_COUNTRY_CODE => 'required',
      self::PARAM_STREET => 'required',
      self::PARAM_CITY => 'required',
      self::PARAM_ZIP => 'required',
    ];
  }

  /**
   * @return array
   */
  private static function defineRequestParamsToAddressMap(): array
  {
    return [
      self::PARAM_FIRST_NAME => Address::ATTR_FIRST_NAME,
      self::PARAM_LAST_NAME => Address::ATTR_LAST_NAME,
      self::PARAM_COUNTRY_CODE => Address::ATTR_COUNTRY_CODE,
      self::PARAM_STREET => Address::ATTR_STREET,
      self::PARAM_ADDRESS_EXTENSION => Address::ATTR_ADDRESS_EXTENSION,
      self::PARAM_CITY => Address::ATTR_CITY,
      self::PARAM_ZIP => Address::ATTR_ZIP,
    ];
  }
}
