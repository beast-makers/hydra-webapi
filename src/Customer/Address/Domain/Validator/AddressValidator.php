<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Address\Domain\Validator;

use BeastMakers\Customer\Shared\Domain\Address;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class AddressValidator
{
  private ArrayValidator $validator;

  private array $rules = [];

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param Address $address
   *
   * @return void
   * @throws RuleNotFoundException
   * @throws ArrayValidationException
   */
  public function validate(Address $address): void
  {
    $addressAsArray = $address->asArray();

    $this->validator->validate($addressAsArray, $this->loadValidationRules());
  }

  /**
   * @return array
   * @throws RuleNotFoundException
   */
  private function loadValidationRules(): array
  {
    if (empty($this->rules)) {
      $v = $this->validator->rakit();
      $notAllowedCharacters = '/[0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]/';

      $this->rules = [
        Address::ATTR_FIRST_NAME => ['required', $v('not_regex', $notAllowedCharacters)],
        Address::ATTR_LAST_NAME => ['required', $v('not_regex', $notAllowedCharacters)],
        Address::ATTR_COUNTRY_CODE => 'required|country_code_alpha2',
        Address::ATTR_STREET => 'required',
        Address::ATTR_CITY => ['required', $v('not_regex', $notAllowedCharacters)],
        Address::ATTR_ZIP => 'required',
      ];
    }

    return $this->rules;
  }
}
