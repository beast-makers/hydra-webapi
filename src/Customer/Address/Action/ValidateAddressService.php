<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Address\Action;

use BeastMakers\Customer\Address\Domain\Validator\AddressValidator;
use BeastMakers\Customer\Shared\Domain\Address;
use BeastMakers\Customer\Shared\ErrorCode;
use BeastMakers\Customer\Shared\Result\ValidateAddressResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;

class ValidateAddressService
{
  private AddressValidator $addressValidator;

  /**
   * @param AddressValidator $addressValidator
   */
  public function __construct(AddressValidator $addressValidator)
  {
    $this->addressValidator = $addressValidator;
  }

  /**
   * @param Address $address
   *
   * @return ValidateAddressResult
   */
  public function validateAddress(Address $address): ValidateAddressResult
  {
    $addressValidationResult = new ValidateAddressResult();

    try {
      $this->addressValidator->validate($address);
      $addressValidationResult->setIsSuccess(true);
    } catch (ArrayValidationException $e) {
      $addressValidationResult->addError(new Error(ErrorCode::ADDRESS_VALIDATION, $e->getErrors()));
    }

    return $addressValidationResult;
  }
}
