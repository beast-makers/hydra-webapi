<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient\Action;

use BeastMakers\Customer\Patient\Domain\Validator\PatientValidator;
use BeastMakers\Customer\Shared\Domain\Patient;
use BeastMakers\Customer\Shared\ErrorCode;
use BeastMakers\Customer\Shared\Result\ValidatePatientResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class ValidatePatientService
{
  private PatientValidator $patientValidator;

  /**
   * @param PatientValidator $patientValidator
   */
  public function __construct(PatientValidator $patientValidator)
  {
    $this->patientValidator = $patientValidator;
  }

  /**
   * @param Patient $patient
   *
   * @return ValidatePatientResult
   * @throws RuleNotFoundException
   */
  public function validatePatient(Patient $patient): ValidatePatientResult
  {
    $patientValidationResult = new ValidatePatientResult();

    try {
      $this->patientValidator->validate($patient);
      $patientValidationResult->setIsSuccess(true);
    } catch (ArrayValidationException $e) {
      $patientValidationResult->addError(new Error(ErrorCode::PATIENT_VALIDATION, $e->getErrors()));
    }

    return $patientValidationResult;
  }
}
