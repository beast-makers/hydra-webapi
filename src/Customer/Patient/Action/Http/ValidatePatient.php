<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient\Action\Http;

use BeastMakers\Customer\Patient\Config;
use BeastMakers\Customer\Patient\Factory;
use BeastMakers\Customer\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\RuleNotFoundException;

/**
 * @property Factory $factory
 * @property Config $config
 */
class ValidatePatient extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_READ_ONLY];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws ArrayValidationException
   * @throws RuleNotFoundException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $patient = $this->factory->createPatientFromPatientValidationRequest($request);
    $validationResult = $this->factory->newValidatePatientService()->validatePatient($patient);

    if (!$validationResult->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($validationResult, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];

    return new Response(204, $headers, '');
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::INVALID_JSON_FORMAT => 400,
      ErrorCode::INVALID_INPUT_DATA => 400,
      ErrorCode::PATIENT_VALIDATION => 400,
    ];
  }
}
