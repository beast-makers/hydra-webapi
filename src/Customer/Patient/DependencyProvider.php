<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient;

use BeastMakers\Shared\DependencyLoader\OwnArrayValidator;
use BeastMakers\Shared\Kernel\BaseDependencyProvider;

class DependencyProvider extends BaseDependencyProvider
{
  use OwnArrayValidator;
}
