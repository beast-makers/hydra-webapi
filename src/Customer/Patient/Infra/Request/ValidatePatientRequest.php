<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient\Infra\Request;

use BeastMakers\Customer\Shared\Domain\Patient;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class ValidatePatientRequest
{
  private const PARAM_FIRST_NAME = 'first_name';
  private const PARAM_LAST_NAME = 'last_name';
  private const PARAM_EMAIL = 'email';
  private const PARAM_BIRTH_DATE = 'birth_date';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return Patient
   * @throws ArrayValidationException
   */
  public function createPatientFromRequest(ServerRequestInterface $request): Patient
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $request->getBody()->rewind();

    $this->validateParams($params);
    $map = self::defineRequestParamsToPatientMap();

    return Patient::mapArrayToNewDto($params, $map);
  }

  /**
   * @param array $params
   *
   * @return void
   * @throws ArrayValidationException
   */
  private function validateParams(array $params): void
  {
    $this->validator->validate($params, static::defineValidationRules());
  }

  /**
   * @return array
   */
  private static function defineValidationRules(): array
  {
    return [
      self::PARAM_FIRST_NAME => 'required',
      self::PARAM_LAST_NAME => 'required',
      self::PARAM_EMAIL => 'required',
      self::PARAM_BIRTH_DATE => 'required',
    ];
  }

  /**
   * @return array
   */
  private static function defineRequestParamsToPatientMap(): array
  {
    return [
      self::PARAM_FIRST_NAME => Patient::ATTR_FIRST_NAME,
      self::PARAM_LAST_NAME => Patient::ATTR_LAST_NAME,
      self::PARAM_EMAIL => Patient::ATTR_EMAIL,
      self::PARAM_BIRTH_DATE => Patient::ATTR_BIRTH_DATE,
    ];
  }
}
