<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient;

use BeastMakers\Customer\Patient\Action\ValidatePatientService;
use BeastMakers\Customer\Patient\Domain\Validator\PatientValidator;
use BeastMakers\Customer\Patient\Infra\Request\ValidatePatientRequest;
use BeastMakers\Customer\Shared\Domain\Patient;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return ValidatePatientService
   */
  public function newValidatePatientService(): ValidatePatientService
  {
    return new ValidatePatientService($this->sharePatientValidator());
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return Patient
   * @throws ArrayValidationException
   */
  public function createPatientFromPatientValidationRequest(ServerRequestInterface $request): Patient
  {
    $validatePatientRequest = new ValidatePatientRequest($this->dependencyProvider->shareArrayValidator());

    return $validatePatientRequest->createPatientFromRequest($request);
  }


  /**
   * @return PatientValidator
   */
  protected function newPatientValidator(): PatientValidator
  {
    return new PatientValidator($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return PatientValidator
   */
  protected function sharePatientValidator(): PatientValidator
  {
    return $this->shareInstance(PatientValidator::class, static function (Factory $factory) {
      return $factory->newPatientValidator();
    });
  }
}
