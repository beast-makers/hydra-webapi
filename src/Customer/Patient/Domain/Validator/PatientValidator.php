<?php
declare(strict_types=1);

namespace BeastMakers\Customer\Patient\Domain\Validator;

use BeastMakers\Customer\Shared\Domain\Patient;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class PatientValidator
{
  private ArrayValidator $validator;

  private array $rules = [];

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param Patient $patient
   *
   * @return void
   * @throws RuleNotFoundException
   * @throws ArrayValidationException
   */
  public function validate(Patient $patient): void
  {
    $patientAsArray = $patient->asArray();

    $this->validator->validate($patientAsArray, $this->loadValidationRules());
  }

  /**
   * @return array
   * @throws RuleNotFoundException
   */
  private function loadValidationRules(): array
  {
    if (empty($this->rules)) {
      $v = $this->validator->rakit();
      $notAllowedCharacters = '/[0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]/';

      $this->rules = [
        Patient::ATTR_FIRST_NAME => ['required', $v('not_regex', $notAllowedCharacters)],
        Patient::ATTR_LAST_NAME => ['required', $v('not_regex', $notAllowedCharacters)],
        Patient::ATTR_EMAIL => 'required|email',
        Patient::ATTR_BIRTH_DATE => 'required|date_format:d-m-Y',
      ];
    }

    return $this->rules;
  }
}
