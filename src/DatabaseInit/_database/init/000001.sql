--liquibase formatted sql

--changeset konradg:001
CREATE OR REPLACE FUNCTION public.updated_at_timestamp_column()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS '
    BEGIN
        NEW.updated_at = now();
        RETURN NEW;
    END;
';

--changeset konradg:002
CREATE OR REPLACE FUNCTION public.jsonb_minus ( arg1 jsonb, arg2 jsonb )
    RETURNS jsonb
AS $$
SELECT
    COALESCE(json_object_agg(key,
    CASE
        WHEN jsonb_typeof(value) = 'object' AND arg2 -> key IS NOT NULL THEN public.jsonb_minus(value, arg2 -> key)
        ELSE value
    END
    ), '{}')::jsonb
FROM jsonb_each(arg1)
WHERE arg1 -> key <> arg2 -> key OR arg2 -> key IS NULL
$$ LANGUAGE SQL;

CREATE OPERATOR - (
    PROCEDURE = public.jsonb_minus,
    LEFTARG   = jsonb,
    RIGHTARG  = jsonb
);
