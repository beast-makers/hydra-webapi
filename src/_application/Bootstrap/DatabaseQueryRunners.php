<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DatabaseConnector\ConnectionPair;
use BeastMakers\Shared\DatabaseConnector\PostgreSQL\Connection;
use BeastMakers\Shared\DatabaseConnector\PostgreSQL\QueryRunner;
use BeastMakers\Shared\DatabaseConnector\QueryRunnerPool;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;

class DatabaseQueryRunners
{
  /**
   * @return void
   */
  public function run(): void
  {
    $diContainer = DependencyContainer::getOwnInstance();

    $diContainer->registerConstructor(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL, function () use ($diContainer) {
      return $this->newQueryRunnerPool($diContainer->shareInstance(DependencyContainerItem::APPLICATION_CONFIG));
    });
  }

  /**
   * @param Config $config
   *
   * @return QueryRunnerPool
   * @throws MissingConfigOptionException
   */
  private function newQueryRunnerPool(Config $config): QueryRunnerPool
  {
    $queryRunnerPool = new QueryRunnerPool();

    $this->registerBackendDatabase($queryRunnerPool, $config->getConfig('database.webapi'));

    return $queryRunnerPool;
  }

  /**
   * @param QueryRunnerPool $queryRunnerPool
   * @param Config $catalogConfig
   * @throws MissingConfigOptionException
   *
   * @return void
   */
  private function registerBackendDatabase(QueryRunnerPool $queryRunnerPool, Config $catalogConfig): void
  {
    $dbConnection = new Connection(
      $catalogConfig->getString('host'),
      intval($catalogConfig->getString('port')),
      $catalogConfig->getString('database'),
      $catalogConfig->getString('user'),
      $catalogConfig->getString('password')
    );

    $connectionPair = new ConnectionPair($dbConnection, $dbConnection);
    $queryRunnerPool->register(new QueryRunner($connectionPair), QueryRunnerCatalog::WEBAPI);
  }
}
