<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Application\RoutesLoader as ApplicationRoutes;
use BeastMakers\ErrorHandler\ErrorHandler;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use Slim\App;

class Routes
{
  /**
   * @param App $app
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  public function run(App $app): void
  {
    $diContainer = DependencyContainer::getOwnInstance();
    $appConfig = $diContainer->shareInstance(DependencyContainerItem::APPLICATION_CONFIG);

    $routes = new ApplicationRoutes($appConfig);
    $routes->loadRoutes($app, $appConfig->getString('current_store'));

    $app->add(new ApiAccessGuardMiddleware());

    $app->addRoutingMiddleware();
    $errorMiddleware = $app->addErrorMiddleware(true, true, true);
    $errorMiddleware->setDefaultErrorHandler([ErrorHandler::class, 'handleError']);
  }
}
