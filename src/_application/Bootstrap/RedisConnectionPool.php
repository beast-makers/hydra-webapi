<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\RedisConnectionCatalog;
use BeastMakers\Shared\RedisConnector\ClientPool;
use BeastMakers\Shared\RedisConnector\Connection;
use BeastMakers\Shared\RedisConnector\ConnectionPair;
use BeastMakers\Shared\RedisConnector\RedisClient;

class RedisConnectionPool
{
  /**
   * @return void
   */
  public function run(): void
  {
    $diContainer = DependencyContainer::getOwnInstance();

    $diContainer->registerConstructor(DependencyContainerItem::REDIS_CONNECTION_POOL, function () use ($diContainer) {
      return $this->newClientPool($diContainer->shareInstance(DependencyContainerItem::APPLICATION_CONFIG));
    });
  }

  /**
   * @param Config $config
   *
   * @return ClientPool
   * @throws MissingConfigOptionException
   */
  private function newClientPool(Config $config): ClientPool
  {
    $clientPool = new ClientPool();

    $this->registerWebapiMaster($clientPool, $config->getConfig('redis.webapi_master'));

    return $clientPool;
  }

  /**
   * @param ClientPool $clientPool
   * @param Config $clientConfig
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function registerWebapiMaster(ClientPool $clientPool, Config $clientConfig): void
  {
    $redisConnection = new Connection(
      $clientConfig->getString('host'),
      $clientConfig->getInt('port'),
      $clientConfig->getInt('index_db'),
    );

    $connectionPair = new ConnectionPair($redisConnection, $redisConnection);
    $clientPool->register(new RedisClient($connectionPair), RedisConnectionCatalog::WEBAPI);
  }
}
