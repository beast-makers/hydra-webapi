<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\Action\ActionResponseBuilder;
use BeastMakers\Shared\Kernel\DependencyContainer as Di;
use Slim\App;

class DependencyContainer
{
  /**
   * @param App $app
   *
   * @return void
   */
  public function run(App $app): void
  {
    $diContainer = Di::getOwnInstance();
    $this->registerActionResponseBuilder($diContainer);
    $this->registerActionRouteParser($diContainer, $app);
  }

  /**
   * @param Di $diContainer
   *
   * @return void
   */
  private function registerActionResponseBuilder(Di $diContainer): void
  {
    $diContainer->registerConstructor(DependencyContainerItem::ACTION_RESPONSE_BUILDER, function () {
      return new ActionResponseBuilder();
    });
  }

  /**
   * @param Di $diContainer
   * @param App $app
   *
   * @return void
   */
  private function registerActionRouteParser(Di $diContainer, App $app): void
  {
    $diContainer->registerConstructor(DependencyContainerItem::ACTION_ROUTE_PARSER, function () use ($app) {
      return $app->getRouteCollector()->getRouteParser();
    });
  }
}
