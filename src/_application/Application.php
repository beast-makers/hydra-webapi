<?php
declare(strict_types=1);

namespace BeastMakers\Application;

use BeastMakers\Shared\Config\MissingConfigOptionException;
use Slim\App;
use Slim\Factory\AppFactory;

class Application
{
  /**
   * @return App
   * @throws MissingConfigOptionException
   */
  public function bootstrap(): App
  {
    $app = AppFactory::create();
    $this->runBootstrapStack($app);

    return $app;
  }

  /**
   * @param App $app
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function runBootstrapStack(App $app): void
  {
    (new Bootstrap\ApplicationConfig())->run();
    (new Bootstrap\DependencyContainer())->run($app);
    (new Bootstrap\DatabaseQueryRunners())->run();
    (new Bootstrap\RedisConnectionPool())->run();
    (new Bootstrap\Routes())->run($app);
  }
}
