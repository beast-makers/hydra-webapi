<?php
declare(strict_types=1);

if (!defined('PROJECT_DIR')) {
  define('PROJECT_DIR', __DIR__ . '/../../');
}

/** @psalm-suppress UnresolvableInclude */
require PROJECT_DIR . 'vendor/autoload.php';

return (new BeastMakers\Application\Application())->bootstrap();
