<?php
declare(strict_types=1);

namespace BeastMakers\Booking;

use BeastMakers\Booking\Ruler\Action\Http\DaysInYearToTimeframes;
use BeastMakers\Booking\Ruler\Action\Http\DaysInYearToTimeslots;
use BeastMakers\Booking\Ruler\Action\Http\MergeTimeframes;
use BeastMakers\Booking\Ruler\Action\Http\SchedulesTimeslots;
use BeastMakers\Booking\Ruler\Action\Http\WeekdaysToTimeframes;
use BeastMakers\Booking\Ruler\Action\Http\WeekdaysToTimeslots;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_WEEKDAYS_TO_TIMESLOTS = 'booking-ruler.weekdays-to-timeslots';
  public const ROUTE_DAYS_IN_YEAR_TO_TIMESLOTS = 'booking-ruler.days-in-year-to-timeslots';
  public const ROUTE_SCHEDULES_TIMESLOTS = 'booking-ruler.schedules-timeslots';

  public const ROUTE_DAYS_IN_YEAR_TO_TIMEFRAMES = 'booking-ruler.days-in-year-to-timeframes';
  public const ROUTE_WEEKDAYS_TO_TIMEFRAMES = 'booking-ruler.weekdays-to-timeframes';
  public const ROUTE_MERGE_TIMEFRAMES = 'booking-ruler.merge-timeframes';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $app->get('/booking/weekdays-slots', WeekdaysToTimeslots::class)
      ->setName(self::ROUTE_WEEKDAYS_TO_TIMESLOTS);

    $app->get('/booking/days-in-year-slots', DaysInYearToTimeslots::class)
      ->setName(self::ROUTE_DAYS_IN_YEAR_TO_TIMESLOTS);

    $app->get('/booking/schedules-timeslots', SchedulesTimeslots::class)
      ->setName(self::ROUTE_SCHEDULES_TIMESLOTS);

    $app->get('/booking/days-in-year-timeframes', DaysInYearToTimeframes::class)
      ->setName(self::ROUTE_DAYS_IN_YEAR_TO_TIMEFRAMES);

    $app->get('/booking/weekdays-timeframes', WeekdaysToTimeframes::class)
      ->setName(self::ROUTE_WEEKDAYS_TO_TIMEFRAMES);

    $app->get('/booking/merge-timeframes', MergeTimeframes::class)
      ->setName(self::ROUTE_MERGE_TIMEFRAMES);
  }
}
