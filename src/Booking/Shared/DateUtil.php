<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared;

class DateUtil
{
  /**
   * @param int $startTimestampA
   * @param int $endTimestampA
   * @param int $startTimestampB
   * @param int $endTimestampB
   *
   * @return bool
   */
  public static function isDateTimeOverlapping(
    int $startTimestampA,
    int $endTimestampA,
    int $startTimestampB,
    int $endTimestampB
  ): bool {
    if (($startTimestampA <= $endTimestampB) && ($startTimestampB < $endTimestampA)) {
      return true;
    }

    return false;
  }

  /**
   * @param int $dateTimestamp
   * @param int $startDateTimestamp
   * @param int $endDateTimestamp
   *
   * @return bool
   */
  public static function isDateOverlapping(int $dateTimestamp, int $startDateTimestamp, int $endDateTimestamp): bool
  {
    $dayForwardTimestamp = strtotime('+1 day', $dateTimestamp);

    return self::isDateTimeOverlapping($dateTimestamp, $dayForwardTimestamp, $startDateTimestamp, $endDateTimestamp);
  }
}
