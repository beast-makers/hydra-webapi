<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared\Exception;

class UndefinedRuleException extends \Exception
{
}
