<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared\Exception;

class UndefinedScheduleOperationException extends \Exception
{
}
