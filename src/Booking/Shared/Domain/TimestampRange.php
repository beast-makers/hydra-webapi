<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared\Domain;

class TimestampRange
{
  private int $fromTimestamp = -1;

  private int $toTimestamp = -1;

  public function __construct(int $fromTimestamp, int $toTimestamp)
  {
    $this->fromTimestamp = $fromTimestamp;
    $this->toTimestamp = $toTimestamp;
  }

  public function getFromTimestamp(): int
  {
    return $this->fromTimestamp;
  }

  public function getToTimestamp(): int
  {
    return $this->toTimestamp;
  }
}
