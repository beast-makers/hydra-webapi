<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared\Domain;

class TimeFinder
{
  /**
   * @param array $timeframe
   * @param int $durationInMinutes
   *
   * @return array
   */
  public function findTimesInTimeFrame(array $timeframe, int $durationInMinutes): array
  {
    $referenceDate = '2020-01-01'; //it's about hours, so date does not matter
    [$timestampFrom, $timestampTo] = $this->timeframeToTimestamps($timeframe, $referenceDate);

    $firstTimeslot = strtotime("+{$durationInMinutes} minutes", $timestampFrom);
    if ($firstTimeslot > $timestampTo) {
      return [];
    }

    $timeslots = [];
    $currentTimestamp = $timestampFrom;
    while ($currentTimestamp <= $timestampTo) {
      $timeslots[] = date('H:i', $currentTimestamp);
      $currentTimestamp = strtotime("+{$durationInMinutes} minutes", $currentTimestamp);
    }

    return $timeslots;
  }

  /**
   * @param array $timeframe
   * @param string $referenceDate
   *
   * @return array
   */
  private function timeframeToTimestamps(array $timeframe, string $referenceDate): array
  {
    [$from, $to] = $timeframe;
    $timestampFrom = strtotime("{$referenceDate} {$from}");
    $timestampTo = strtotime("{$referenceDate} {$to}");

    return [$timestampFrom, $timestampTo];
  }
}
