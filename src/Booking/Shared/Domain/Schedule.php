<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Shared\Domain;

class Schedule
{
  public const OPERATION_ADD = 'add';
  public const OPERATION_SUBTRACT = 'subtract';

  private string $name = '';

  private array $definition = [];

  private string $operation = '';

  /**
   * @param string $name
   * @param array $definition
   * @param string $operation
   */
  public function __construct(string $name, array $definition, string $operation)
  {
    $this->name = $name;
    $this->definition = $definition;
    $this->operation = $operation;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getOperation(): string
  {
    return $this->operation;
  }

  /**
   * @return array
   */
  public function getDefinition(): array
  {
    return $this->definition;
  }

  /**
   * @return bool
   */
  public function isAddOperation(): bool
  {
    return $this->getOperation() === self::OPERATION_ADD;
  }

  /**
   * @return bool
   */
  public function isSubtractOperation(): bool
  {
    return $this->getOperation() === self::OPERATION_SUBTRACT;
  }
}
