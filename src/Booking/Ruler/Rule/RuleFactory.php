<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Rule;

use BeastMakers\Booking\Ruler\Factory;
use BeastMakers\Booking\Shared\Exception\UndefinedRuleException;

class RuleFactory
{
  public const RULE_DAYS_IN_YEAR = 'days-in-year';
  public const RULE_WEEKDAYS = 'weekdays';

  private Factory $rulerFactory;

  /**
   * @var \Closure[]
   */
  private array $rulesConstructors;

  /**
   * Factory constructor.
   * @param Factory $rulerFactory
   */
  public function __construct(Factory $rulerFactory)
  {
    $this->rulerFactory = $rulerFactory;
    $this->rulesConstructors = $this->initRulesConstructors();
  }

  /**
   * @param string $ruleName
   *
   * @return Rule
   * @throws UndefinedRuleException
   */
  public function findRuleByName(string $ruleName): Rule
  {
    if (!isset($this->rulesConstructors[$ruleName])) {
      throw new UndefinedRuleException();
    }

    return ($this->rulesConstructors[$ruleName])();
  }

  /**
   * @return array
   */
  private function initRulesConstructors(): array
  {
    return [
      self::RULE_DAYS_IN_YEAR => function () {
        return $this->rulerFactory->shareDaysInYearRule();
      },

      self::RULE_WEEKDAYS => function () {
        return $this->rulerFactory->shareWeekdaysRule();
      },
    ];
  }
}
