<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Rule;

use BeastMakers\Booking\Shared\Domain\TimestampRange;

interface Rule
{
  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   * @param int $timeslotDurationMinutes
   *
   * @return array
   */
  public function listTimeslots(
    array $rule,
    TimestampRange $timestampRange,
    int $timeslotDurationMinutes
  ): array;
}
