<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Rule;

use BeastMakers\Booking\Shared\DateUtil;
use BeastMakers\Booking\Shared\Domain\TimeFinder;
use BeastMakers\Booking\Shared\Domain\TimestampRange;

class DaysInYearRule implements Rule
{
  private TimeFinder $timeFinder;

  /**
   * @param TimeFinder $timeFinder
   */
  public function __construct(TimeFinder $timeFinder)
  {
    $this->timeFinder = $timeFinder;
  }

  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   * @param int $timeslotDurationMinutes
   *
   * @return array
   */
  public function listTimeslots(
    array $rule,
    TimestampRange $timestampRange,
    int $timeslotDurationMinutes
  ): array {
    $dates = $this->discoverDates($rule, $timestampRange);

    return $this->findTimesInDates($rule, $dates, $timeslotDurationMinutes);
  }

  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  private function discoverDates(array $rule, TimestampRange $timestampRange): array
  {
    $dates = [];
    $fromTimestamp = $timestampRange->getFromTimestamp();
    $toTimestamp = $timestampRange->getToTimestamp();

    foreach ($rule as $dayInYear => $timeframes) {
      $timestamp = strtotime($dayInYear);
      if (DateUtil::isDateOverlapping($timestamp, $fromTimestamp, $toTimestamp)) {
        $dates[$dayInYear] = $timestamp;
      }
    }

    return $dates;
  }

  /**
   * @param array $rule
   * @param array $dates
   * @param int $durationInMinutes
   *
   * @return array
   */
  private function findTimesInDates(array $rule, array $dates, int $durationInMinutes): array
  {
    $timeslots = [];
    foreach ($dates as $date => $timestamp) {
      $timeframes = $rule[$date];
      $timeslots[$date] = [];
      foreach ($timeframes as $timeframe) {
        $timeslots[$date] = array_merge($timeslots[$date], $this->timeFinder->findTimesInTimeFrame($timeframe, $durationInMinutes));
      }

      if (empty($timeslots[$date])) {
        unset($timeslots[$date]);
      }
    }

    return $timeslots;
  }
}
