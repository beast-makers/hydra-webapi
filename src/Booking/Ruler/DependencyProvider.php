<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler;

use BeastMakers\Shared\Kernel\BaseDependencyProvider;

class DependencyProvider extends BaseDependencyProvider
{
}
