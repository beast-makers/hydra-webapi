<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\MergeRule;

use BeastMakers\Booking\Ruler\Factory;
use BeastMakers\Booking\Shared\Exception\UndefinedRuleException;

class MergeRuleFactory
{
  public const RULE_DAYS_IN_YEAR = 'days-in-year';
  public const RULE_WEEKDAYS = 'weekdays';

  private Factory $rulerFactory;

  /**
   * @var \Closure[]
   */
  private array $rulesConstructors;

  /**
   * Factory constructor.
   * @param Factory $rulerFactory
   */
  public function __construct(Factory $rulerFactory)
  {
    $this->rulerFactory = $rulerFactory;
    $this->rulesConstructors = $this->initRulesConstructors();
  }

  /**
   * @param string $ruleName
   *
   * @return MergeRule
   * @throws UndefinedRuleException
   */
  public function findRuleByName(string $ruleName): MergeRule
  {
    if (!isset($this->rulesConstructors[$ruleName])) {
      throw new UndefinedRuleException();
    }

    return ($this->rulesConstructors[$ruleName])();
  }

  /**
   * @return array
   */
  private function initRulesConstructors(): array
  {
    return [
      self::RULE_DAYS_IN_YEAR => function () {
        return $this->rulerFactory->shareDaysInYearMergeRule();
      },

      self::RULE_WEEKDAYS => function () {
        return $this->rulerFactory->shareWeekdaysMergeRule();
      },
    ];
  }
}
