<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\MergeRule;

use BeastMakers\Booking\Shared\Domain\TimestampRange;

class WeekdaysMergeRule implements MergeRule
{
  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  public function findTimeframes(
    array $rule,
    TimestampRange $timestampRange
  ): array {
    return $this->discoverDates($rule, $timestampRange);
  }

  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  private function discoverDates(array $rule, TimestampRange $timestampRange): array
  {
    $dates = [];
    $timestamp = $timestampRange->getFromTimestamp();
    $limitTimestamp = $timestampRange->getToTimestamp();
    while ($timestamp <= $limitTimestamp) {
      $weekDay = date('w', $timestamp);
      if (!empty($rule[$weekDay])) {
        $timeframes = $rule[$weekDay];
        $dayInYear = date('Y-m-d', $timestamp);
        foreach ($timeframes as $timeframe) {
          $dates[] = [
            strtotime("{$dayInYear} {$timeframe[0]}"),
            strtotime("{$dayInYear} {$timeframe[1]}"),
          ];
        }
      }
      $timestamp = strtotime('+1 day', $timestamp);
    }

    return $dates;
  }
}
