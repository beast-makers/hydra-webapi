<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\MergeRule;

use BeastMakers\Booking\Shared\DateUtil;
use BeastMakers\Booking\Shared\Domain\TimestampRange;

class DaysInYearMergeRule implements MergeRule
{
  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  public function findTimeframes(
    array $rule,
    TimestampRange $timestampRange
  ): array {
    return $this->discoverDates($rule, $timestampRange);
  }

  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  private function discoverDates(array $rule, TimestampRange $timestampRange): array
  {
    $dates = [];
    $fromTimestamp = $timestampRange->getFromTimestamp();
    $toTimestamp = $timestampRange->getToTimestamp();

    foreach ($rule as $dayInYear => $timeframes) {
      $timestamp = strtotime($dayInYear);
      if (DateUtil::isDateOverlapping($timestamp, $fromTimestamp, $toTimestamp)) {
        foreach ($timeframes as $timeframe) {
          $dates[] = [
            strtotime("{$dayInYear} {$timeframe[0]}"),
            strtotime("{$dayInYear} {$timeframe[1]}"),
          ];
        }
      }
    }

    return $dates;
  }
}
