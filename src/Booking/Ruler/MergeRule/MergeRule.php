<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\MergeRule;

use BeastMakers\Booking\Shared\Domain\TimestampRange;

interface MergeRule
{
  /**
   * @param array $rule
   * @param TimestampRange $timestampRange
   *
   * @return array
   */
  public function findTimeframes(
    array $rule,
    TimestampRange $timestampRange
  ): array;
}
