<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Action;

use BeastMakers\Booking\Ruler\Rule\RuleFactory;
use BeastMakers\Booking\Shared\Domain\Schedule;
use BeastMakers\Booking\Shared\Domain\TimestampRange;
use BeastMakers\Booking\Shared\Exception\UndefinedRuleException;
use BeastMakers\Booking\Shared\Exception\UndefinedScheduleOperationException;

class ListTimeSlotsCommand
{
  private RuleFactory $ruleFactory;

  /**
   * @param RuleFactory $ruleFactory
   */
  public function __construct(RuleFactory $ruleFactory)
  {
    $this->ruleFactory = $ruleFactory;
  }

  /**
   * @param Schedule[] $schedules
   * @param TimestampRange $timestampRange
   * @param int $timeslotDurationMinutes
   *
   * @return array
   * @throws UndefinedRuleException
   * @throws UndefinedScheduleOperationException
   */
  public function listTimeslots(
    array $schedules,
    TimestampRange $timestampRange,
    int $timeslotDurationMinutes
  ): array {
    $timeslots = [];
    foreach ($schedules as $schedule) {
      $rule = $this->ruleFactory->findRuleByName($schedule->getName());
      $foundTimeslots = $rule->listTimeslots($schedule->getDefinition(), $timestampRange, $timeslotDurationMinutes);
      $timeslots = $this->operate($timeslots, $foundTimeslots, $schedule);
    }

    return $timeslots;
  }

  /**
   * @param array $target
   * @param array $source
   * @param Schedule $schedule
   *
   * @return array
   * @throws UndefinedScheduleOperationException
   */
  private function operate(array $target, array $source, Schedule $schedule): array
  {
    if ($schedule->isAddOperation()) {
      return $this->addTimeslots($target, $source);
    }

    if ($schedule->isSubtractOperation()) {
      return $this->subtractTimeslots($target, $source);
    }

    throw new UndefinedScheduleOperationException();
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  private function addTimeslots(array $target, array $source): array
  {
    $timeslots = array_merge_recursive($target, $source);
    foreach ($timeslots as &$times) {
      $times = array_unique($times);
    }

    return $this->sortTimeslots($timeslots);
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  private function subtractTimeslots(array $target, array $source): array
  {
    $timeslots = $target;
    foreach ($source as $date => $times) {
      if (empty($target[$date])) {
        continue;
      }

      $timeslots[$date] = array_diff($target[$date], $source[$date]);
    }

    return array_filter($timeslots);
  }

  /**
   * @param array $timeslots
   *
   * @return array
   */
  private function sortTimeslots(array $timeslots): array
  {
    ksort($timeslots);

    foreach ($timeslots as &$times) {
      sort($times);
    }

    return $timeslots;
  }
}
