<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Action\Http;

use BeastMakers\Booking\Ruler\Factory;
use BeastMakers\Booking\Shared\Domain\TimestampRange;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class WeekdaysToTimeframes extends BaseHttpAction
{
  public const IS_AUTHENTICATION_REQUIRED = false;

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $rule = $this->factory->newWeekdaysMergeRule();

    $from = '2021-07-01';
    $fromTimestamp = strtotime($from);
    $toTimestamp = strtotime("{$from} + 9 days");

    $timestampRange = new TimestampRange($fromTimestamp, $toTimestamp);
    $timeframes = $rule->findTimeframes($this->rule(), $timestampRange);

    $responseContent = [
      'from' => date('Y-m-d', $fromTimestamp),
      'to' => date('Y-m-d', $toTimestamp),
      'timeframes' => $timeframes,
    ];

    $headers = [
      'Content-type' => 'application/json',
    ];
    /** @psalm-suppress PossiblyNullReference */
    $body = Stream::create(json_encode($responseContent));

    return new Response(200, $headers, $body);
  }

  /**
   * @return array
   */
  private function rule(): array
  {
    return [
      '1' => [ //monday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
      '2' => [ //tuesday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
      '3' => [ //wednesday
        ['08:00', '12:00'],
      ],
      '4' => [ //thursday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
      '5' => [ //friday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
    ];
  }
}
