<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Action\Http;

use BeastMakers\Booking\Ruler\Factory;
use BeastMakers\Booking\Shared\Domain\TimestampRange;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class DaysInYearToTimeslots extends BaseHttpAction
{
  public const IS_AUTHENTICATION_REQUIRED = false;

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $rule = $this->factory->newDaysInYearRule();

    $from = '2021-07-01';
    $fromTimestamp = strtotime($from);
    $toTimestamp = strtotime("{$from} + 9 days");

    $timestampRange = new TimestampRange($fromTimestamp, $toTimestamp);
    $timeslots = $rule->listTimeslots($this->rule(), $timestampRange, 30);

    $responseContent = [
      'from' => date('Y-m-d', $fromTimestamp),
      'to' => date('Y-m-d', $toTimestamp),
      'timeslots' => $timeslots,
    ];

    $headers = [
      'Content-type' => 'application/json',
    ];
    /** @psalm-suppress PossiblyNullReference */
    $body = Stream::create(json_encode($responseContent));

    return new Response(200, $headers, $body);
  }

  /**
   * @return array
   */
  private function rule(): array
  {
    return [
      '2021-07-01' => [
        ['08:00', '12:00'],
      ],
      '2021-07-10' => [
        ['08:00', '13:00'],
      ],
    ];
  }
}
