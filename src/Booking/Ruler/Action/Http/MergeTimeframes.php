<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Action\Http;

use BeastMakers\Booking\Ruler\Factory;
use BeastMakers\Booking\Ruler\Rule\RuleFactory;
use BeastMakers\Booking\Shared\Domain\Schedule;
use BeastMakers\Booking\Shared\Domain\TimestampRange;
use BeastMakers\Booking\Shared\Exception\UndefinedRuleException;
use BeastMakers\Booking\Shared\Exception\UndefinedScheduleOperationException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class MergeTimeframes extends BaseHttpAction
{
  public const IS_AUTHENTICATION_REQUIRED = false;

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws UndefinedRuleException
   * @throws UndefinedScheduleOperationException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $timeframesCommand = $this->factory->newMergeTimeframesCommand();

    $from = '2021-07-01';
    $fromTimestamp = strtotime($from);
    $toTimestamp = strtotime("{$from} +20 days");
    $timestampRange = new TimestampRange($fromTimestamp, $toTimestamp);

    $timeframes = $timeframesCommand->mergeTimeframes($this->schedules(), $timestampRange);

    $responseContent = [
      'from' => date('Y-m-d', $fromTimestamp),
      'to' => date('Y-m-d', $toTimestamp),
      'timeframes' => $timeframes,
    ];

    $headers = [
      'Content-type' => 'application/json',
    ];
    /** @psalm-suppress PossiblyNullReference */
    $body = Stream::create(json_encode($responseContent));

    return new Response(200, $headers, $body);
  }

  /**
   * @return Schedule[]
   */
  private function schedules(): array
  {
    return [
      new Schedule(RuleFactory::RULE_DAYS_IN_YEAR, $this->rulesA_D(), Schedule::OPERATION_ADD),
      new Schedule(RuleFactory::RULE_WEEKDAYS, $this->rulesB_W(), Schedule::OPERATION_ADD),
      //      new Schedule(RuleFactory::RULE_WEEKDAYS, $this->rulesC_WS(), Schedule::OPERATION_SUBTRACT),
      //      new Schedule(RuleFactory::RULE_DAYS_IN_YEAR, $this->rulesD_DS(), Schedule::OPERATION_SUBTRACT),
    ];
  }

  /**
   * @return array
   */
  private function rulesA_D(): array
  {
    return [
      '2021-07-01' => [
        ['06:00', '12:00'],
      ],
      '2021-07-03' => [
        ['15:00', '17:00'],
      ],
      '2021-07-05' => [
        ['15:00', '17:00'],
      ],
    ];
  }

  /**
   * @return array
   */
  private function rulesB_W(): array
  {
    return [
      '1' => [ //monday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
      '2' => [ //tuesday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
      '3' => [ //wednesday
        ['08:00', '12:00'],
      ],
      '4' => [ //thursday
        ['08:00', '12:30'],
        ['13:00', '16:00'],
      ],
      '5' => [ //friday
        ['08:00', '12:00'],
        ['13:00', '16:00'],
      ],
    ];
  }

  /**
   * @return array
   */
  private function rulesC_WS(): array
  {
    return [
      '5' => [ //friday
        ['15:00', '24:00'],
      ],
    ];
  }

  /**
   * @return array
   */
  private function rulesD_DS(): array
  {
    return [
      '2021-07-07' => [
        ['00:00', '24:00'],
      ],
    ];
  }
}
