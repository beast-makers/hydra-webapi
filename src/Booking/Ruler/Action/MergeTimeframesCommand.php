<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler\Action;

use BeastMakers\Booking\Ruler\MergeRule\MergeRuleFactory;
use BeastMakers\Booking\Shared\Domain\Schedule;
use BeastMakers\Booking\Shared\Domain\TimestampRange;
use BeastMakers\Booking\Shared\Exception\UndefinedRuleException;
use BeastMakers\Booking\Shared\Exception\UndefinedScheduleOperationException;

class MergeTimeframesCommand
{
  private MergeRuleFactory $mergeRuleFactory;

  /**
   * @param MergeRuleFactory $mergeRuleFactory
   */
  public function __construct(MergeRuleFactory $mergeRuleFactory)
  {
    $this->mergeRuleFactory = $mergeRuleFactory;
  }

  /**
   * @param Schedule[] $schedules
   * @param TimestampRange $timestampRange
   *
   * @return array
   * @throws UndefinedRuleException
   * @throws UndefinedScheduleOperationException
   */
  public function mergeTimeframes(
    array $schedules,
    TimestampRange $timestampRange
  ): array {
    $timeframes = [];
    foreach ($schedules as $schedule) {
      $rule = $this->mergeRuleFactory->findRuleByName($schedule->getName());
      $foundTimeframes = $rule->findTimeframes($schedule->getDefinition(), $timestampRange);
      $timeframes = $this->operate($timeframes, $foundTimeframes, $schedule);
    }

    return $this->timestampsToDates($timeframes);
  }

  /**
   * @param array $target
   * @param array $source
   * @param Schedule $schedule
   *
   * @return array
   * @throws UndefinedScheduleOperationException
   */
  private function operate(array $target, array $source, Schedule $schedule): array
  {
    if ($schedule->isAddOperation()) {
      return $this->addTimeframes($target, $source);
    }

    if ($schedule->isSubtractOperation()) {
      return $this->subtractTimeslots($target, $source);
    }

    throw new UndefinedScheduleOperationException();
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  private function addTimeframes(array $target, array $source): array
  {
    $ranges = array_merge($target, $source);
    $retVal = [];

    //sort date ranges by begin time
    usort($ranges, static function (array $timestampA, array $timestampB) {
      return $timestampA[0] - $timestampB[0];
    });

    $currentRange = [];
    foreach ($ranges as $range) {
      //fill in the first element
      if (empty($currentRange)) {
        $currentRange = $range;
        continue;
      }

      if ($currentRange[1] < $range[0]) {
        $retVal[] = $currentRange;
        $currentRange = $range;
      } elseif ($currentRange[1] < $range[1]) {
        $currentRange[1] = $range[1];
      }
    }

    if ($currentRange) {
      $retVal[] = $currentRange;
    }

    return $retVal;
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  private function subtractTimeslots(array $target, array $source): array
  {
    $timeslots = $target;
    foreach ($source as $date => $times) {
      if (empty($target[$date])) {
        continue;
      }

      $timeslots[$date] = array_diff($target[$date], $source[$date]);
    }

    return array_filter($timeslots);
  }

  /**
   * @param array $timeframes
   *
   * @return array
   */
  private function timestampsToDates(array $timeframes): array
  {
    $dateTimeframes = [];

    foreach ($timeframes as $timeframe) {
      $dateTimeframes[] = [
        date('Y-m-d H:i', $timeframe[0]),
        date('Y-m-d H:i', $timeframe[1]),
      ];
    }

    return $dateTimeframes;
  }
}
