<?php
declare(strict_types=1);

namespace BeastMakers\Booking\Ruler;

use BeastMakers\Booking\Ruler\Action\ListTimeSlotsCommand;
use BeastMakers\Booking\Ruler\Action\MergeTimeframesCommand;
use BeastMakers\Booking\Ruler\MergeRule\DaysInYearMergeRule;
use BeastMakers\Booking\Ruler\MergeRule\MergeRuleFactory;
use BeastMakers\Booking\Ruler\MergeRule\WeekdaysMergeRule;
use BeastMakers\Booking\Ruler\Rule\DaysInYearRule;
use BeastMakers\Booking\Ruler\Rule\RuleFactory;
use BeastMakers\Booking\Ruler\Rule\WeekdaysRule;
use BeastMakers\Booking\Shared\Domain\TimeFinder;
use BeastMakers\Shared\Kernel\BaseFactory;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return ListTimeSlotsCommand
   */
  public function newListTimeSlotsCommand(): ListTimeSlotsCommand
  {
    return new ListTimeSlotsCommand($this->newRuleFactory());
  }

  /**
   * @return MergeTimeframesCommand
   */
  public function newMergeTimeframesCommand(): MergeTimeframesCommand
  {
    return new MergeTimeframesCommand($this->newMergeRuleFactory());
  }

  /**
   * @return WeekdaysRule
   */
  public function shareWeekdaysRule(): WeekdaysRule
  {
    return $this->shareInstance(WeekdaysRule::class, static function (Factory $factory) {
      return $factory->newWeekdaysRule();
    });
  }

  /**
   * @return DaysInYearRule
   */
  public function shareDaysInYearRule(): DaysInYearRule
  {
    return $this->shareInstance(DaysInYearRule::class, static function (Factory $factory) {
      return $factory->newDaysInYearRule();
    });
  }

  /**
   * @return WeekdaysMergeRule
   */
  public function shareWeekdaysMergeRule(): WeekdaysMergeRule
  {
    return $this->shareInstance(WeekdaysMergeRule::class, static function (Factory $factory) {
      return $factory->newWeekdaysMergeRule();
    });
  }

  /**
   * @return DaysInYearMergeRule
   */
  public function shareDaysInYearMergeRule(): DaysInYearMergeRule
  {
    return $this->shareInstance(DaysInYearMergeRule::class, static function (Factory $factory) {
      return $factory->newDaysInYearMergeRule();
    });
  }

  /**
   * @return RuleFactory
   */
  protected function newRuleFactory(): RuleFactory
  {
    return new RuleFactory($this);
  }

  /**
   * @return MergeRuleFactory
   */
  protected function newMergeRuleFactory(): MergeRuleFactory
  {
    return new MergeRuleFactory($this);
  }

  /**
   * @return WeekdaysRule
   */
  public function newWeekdaysRule(): WeekdaysRule
  {
    return new WeekdaysRule($this->newTimeFinder());
  }

  /**
   * @return DaysInYearRule
   */
  public function newDaysInYearRule(): DaysInYearRule
  {
    return new DaysInYearRule($this->newTimeFinder());
  }

  /**
   * @return DaysInYearMergeRule
   */
  public function newDaysInYearMergeRule(): DaysInYearMergeRule
  {
    return new DaysInYearMergeRule();
  }

  /**
   * @return WeekdaysMergeRule
   */
  public function newWeekdaysMergeRule(): WeekdaysMergeRule
  {
    return new WeekdaysMergeRule();
  }

  /**
   * @return TimeFinder
   */
  protected function newTimeFinder(): TimeFinder
  {
    return new TimeFinder();
  }
}
