<?php
declare(strict_types=1);

namespace BeastMakers\Catalog;

use BeastMakers\Catalog\RawProduct\Action\Http\CreateRawProduct;
use BeastMakers\Catalog\RawProduct\Action\Http\GetRawProduct;
use BeastMakers\Catalog\RawProduct\Action\Http\UpdateRawProduct;
use BeastMakers\Shared\Kernel\Middleware\IsValidJsonMiddleware;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_GET = 'raw-product.get';
  public const ROUTE_CREATE = 'raw-product.create';
  public const ROUTE_UPDATE = 'raw-product.update';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $isValidJsonMiddleware = new IsValidJsonMiddleware();

    $app->get('/raw-product/get/{productId}', GetRawProduct::class)
      ->setName(self::ROUTE_GET);

    $app->post('/raw-product/create', CreateRawProduct::class)
      ->setName(self::ROUTE_CREATE)
      ->add($isValidJsonMiddleware);

    $app->post('/raw-product/update', UpdateRawProduct::class)
      ->setName(self::ROUTE_UPDATE)
      ->add($isValidJsonMiddleware);
  }
}
