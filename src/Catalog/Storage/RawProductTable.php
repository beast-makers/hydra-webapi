<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Storage;

use BeastMakers\Shared\DatabaseConnector\PostgreSQL\DbTable;

class RawProductTable extends DbTable
{
  public const COLUMN_PRODUCT_ID = 'product_id';
  public const COLUMN_TYPE = 'type';
  public const COLUMN_NAME = 'name';
  public const COLUMN_GLOBAL = 'global';
  public const COLUMN_DE = 'de';
  public const COLUMN_AT = 'at';
  public const COLUMN_FR = 'fr';
  public const COLUMN_DE_DE = 'de_de';
  public const COLUMN_FR_FR = 'fr_fr';
  public const COLUMN_AT_DE = 'at_de';

  protected const TYPES = [
    self::COLUMN_PRODUCT_ID => self::TYPE_TEXT,
    self::COLUMN_TYPE => self::TYPE_TEXT,
    self::COLUMN_NAME => self::TYPE_TEXT,
    self::COLUMN_GLOBAL => self::TYPE_JSONB,
    self::COLUMN_DE => self::TYPE_JSONB,
    self::COLUMN_AT => self::TYPE_JSONB,
    self::COLUMN_FR => self::TYPE_JSONB,
    self::COLUMN_DE_DE => self::TYPE_JSONB,
    self::COLUMN_FR_FR => self::TYPE_JSONB,
    self::COLUMN_AT_DE => self::TYPE_JSONB,
  ];
}
