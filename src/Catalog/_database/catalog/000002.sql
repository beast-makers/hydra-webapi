--liquibase formatted sql

--changeset konradg:001
CREATE SEQUENCE IF NOT EXISTS catalog.seq_raw_product AS BIGINT
    INCREMENT 3 START 1;
SELECT setval('catalog.seq_raw_product', 1);

--changeset konradg:002
CREATE TABLE IF NOT EXISTS catalog.raw_product (
  dbid BIGINT PRIMARY KEY DEFAULT nextval('catalog.seq_raw_product'),
  product_id VARCHAR UNIQUE NOT NULL,
  type VARCHAR(20) NOT NULL,
  name VARCHAR UNIQUE NOT NULL,
  global JSONB DEFAULT '{}' NOT NULL,
  de JSONB DEFAULT '{}' NOT NULL,
  at JSONB DEFAULT '{}' NOT NULL,
  fr JSONB DEFAULT '{}' NOT NULL,
  de_de JSONB DEFAULT '{}' NOT NULL,
  fr_fr JSONB DEFAULT '{}' NOT NULL,
  at_de JSONB DEFAULT '{}' NOT NULL,
  created_at TIMESTAMPTZ DEFAULT now(),
  updated_at TIMESTAMPTZ DEFAULT now()
);

--changeset konradg:003
ALTER SEQUENCE catalog.seq_raw_product OWNED BY catalog.raw_product.dbid;

--changeset konradg:004
CREATE INDEX IF NOT EXISTS raw_product__product_id ON catalog.raw_product USING BTREE (product_id);
ę
--changset konradg:005
CREATE TRIGGER update_raw_product_updated_at
    BEFORE UPDATE ON catalog.raw_product
    FOR EACH ROW
EXECUTE PROCEDURE public.updated_at_timestamp_column();
