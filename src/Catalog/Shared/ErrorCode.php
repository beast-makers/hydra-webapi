<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Shared;

use BeastMakers\Shared\Result\CommonErrorCodes;

interface ErrorCode extends CommonErrorCodes
{
  public const RAW_PRODUCT_NOT_FOUND = 'RAW_PRODUCT_NOT_FOUND';
  public const RAW_PRODUCT_WITH_THIS_ID_ALREADY_EXISTS = 'RAW_PRODUCT_WITH_THIS_ID_ALREADY_EXISTS';
  public const RAW_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS = 'RAW_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS';
  public const RAW_PRODUCT_VALIDATION = 'RAW_PRODUCT_VALIDATION';
}
