<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class RawProductRegion extends ArrayDto
{
  public const ATTR_URL = 'url';
  public const ATTR_DESCRIPTION = 'description';
  public const ATTR_RAW_PRICE = 'raw_price';

  /**
   * @return string
   */
  public function getUrl(): string
  {
    return $this->get(self::ATTR_URL, '');
  }

  /**
   * @param string $url
   *
   * @return RawProductRegion
   */
  public function setUrl(string $url): self
  {
    $this->set(self::ATTR_URL, $url);

    return $this;
  }

  /**
   * @return string
   */
  public function getDescription(): string
  {
    return $this->get(self::ATTR_DESCRIPTION, '');
  }

  /**
   * @param string $description
   *
   * @return RawProductRegion
   */
  public function setDescription(string $description): self
  {
    $this->set(self::ATTR_DESCRIPTION, $description);

    return $this;
  }

  /**
   * @return int
   */
  public function getRawPrice(): int
  {
    return $this->get(self::ATTR_RAW_PRICE, -1);
  }

  /**
   * @param int $rawPrice
   *
   * @return RawProductRegion
   */
  public function setRawPrice(int $rawPrice): self
  {
    $this->set(self::ATTR_RAW_PRICE, $rawPrice);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_URL => self::DEF_SCALAR,
      self::ATTR_DESCRIPTION => self::DEF_SCALAR,
      self::ATTR_RAW_PRICE => self::DEF_SCALAR,
    ];
  }
}
