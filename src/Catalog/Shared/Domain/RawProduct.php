<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class RawProduct extends ArrayDto
{
  public const ATTR_PRODUCT_ID = 'product_id';
  public const ATTR_NAME = 'name';
  public const ATTR_TYPE = 'type';
  public const ATTR_REGIONS = 'regions';

  public const PRODUCT_TYPE_SIMPLE = 'simple';

  /**
   * @return string
   */
  public function getProductId(): string
  {
    return $this->get(self::ATTR_PRODUCT_ID, '');
  }

  /**
   * @param string $productId
   *
   * @return RawProduct
   */
  public function setProductId(string $productId): self
  {
    $this->set(self::ATTR_PRODUCT_ID, $productId);

    return $this;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->get(self::ATTR_NAME, '');
  }

  /**
   * @param string $name
   *
   * @return RawProduct
   */
  public function setName(string $name): self
  {
    $this->set(self::ATTR_NAME, $name);

    return $this;
  }

  /**
   * @return string
   */
  public function getType(): string
  {
    return $this->get(self::ATTR_TYPE, '');
  }

  /**
   * @param string $type
   *
   * @return RawProduct
   */
  public function setType(string $type): self
  {
    $this->set(self::ATTR_TYPE, $type);

    return $this;
  }

  /**
   * @param string $regionName
   *
   * @return RawProductRegion
   */
  public function getRegion(string $regionName): RawProductRegion
  {
    $regions = $this->getRegions();
    if (empty($regions) || !array_key_exists($regionName, $regions)) {
      return new RawProductRegion();
    }

    return $regions[$regionName];
  }

  /**
   * @param string $regionName
   * @param RawProductRegion $rawProductRegion
   *
   * @return RawProduct
   */
  public function setRegion(string $regionName, RawProductRegion $rawProductRegion): self
  {
    $regions = $this->getRegions();
    $regions[$regionName] = $rawProductRegion;
    $this->set(self::ATTR_REGIONS, $regions);

    return $this;
  }

  /**
   * @return RawProductRegion[]
   */
  public function getRegions(): array
  {
    return $this->get(self::ATTR_REGIONS, []);
  }

  /**
   * @return array
   */
  public function regionsToArray(): array
  {
    $regions = [];
    foreach ($this->getRegions() as $regionName => $rawProductRegion) {
      $regions[$regionName] = $rawProductRegion->asArray();
    }

    return $regions;
  }

  /**
   * @inheritDoc
   */
  public function definition(): array
  {
    return [
      self::ATTR_PRODUCT_ID => self::DEF_SCALAR,
      self::ATTR_NAME => self::DEF_SCALAR,
      self::ATTR_TYPE => self::DEF_SCALAR,
      self::ATTR_REGIONS => [self::TYPE_ASSOCIATIVE_ARRAY, RawProductRegion::class],
    ];
  }
}
