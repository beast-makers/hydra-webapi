<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Shared\Result;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Shared\Result\Result;

class GetRawProductResult extends Result
{
  private ?RawProduct $rawProduct = null;

  /**
   * @return RawProduct|null
   */
  public function getRawProduct(): ?RawProduct
  {
    return $this->rawProduct;
  }

  /**
   * @param RawProduct $rawProduct
   */
  public function setRawProduct(RawProduct $rawProduct): void
  {
    $this->rawProduct = $rawProduct;
  }
}
