<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\Shared\Result;

use BeastMakers\Shared\Result\Result;

class RawProductValidationResult extends Result
{
}
