<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Request;

class UpdateRawProductRequest extends CuRawProductRequest
{
  /**
   * @return array
   */
  protected static function defineValidationRules(): array
  {
    return [
      self::ATTR_PRODUCT_ID => 'required',
      self::ATTR_NAME => 'nullable',
      self::ATTR_TYPE => 'nullable',
      self::ATTR_REGIONS => 'array',
    ];
  }
}
