<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Request;

class CreateRawProductRequest extends CuRawProductRequest
{
  /**
   * @return array
   */
  protected static function defineValidationRules(): array
  {
    return [
      self::ATTR_PRODUCT_ID => 'required',
      self::ATTR_NAME => 'required',
      self::ATTR_TYPE => 'required',
      self::ATTR_REGIONS => 'array',
    ];
  }
}
