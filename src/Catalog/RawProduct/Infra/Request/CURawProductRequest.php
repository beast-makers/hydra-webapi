<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Request;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\Domain\RawProductRegion;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

abstract class CuRawProductRequest
{
  public const ATTR_PRODUCT_ID = 'product_id';
  public const ATTR_NAME = 'name';
  public const ATTR_TYPE = 'type';
  public const ATTR_REGIONS = 'regions';

  public const ATTR_REGION_URL = 'url';
  public const ATTR_REGION_DESCRIPTION = 'description';
  public const ATTR_REGION_RAW_PRICE = 'raw_price';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return RawProduct
   * @throws ArrayValidationException
   */
  public function createRawProductFromRequest(ServerRequestInterface $request): RawProduct
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $request->getBody()->rewind();

    return $this->createRawProductFromArray($params);
  }

  /**
   * @param array $input
   *
   * @return RawProduct
   * @throws ArrayValidationException
   */
  private function createRawProductFromArray(array $input): RawProduct
  {
    $this->validator->validate($input, static::defineValidationRules());

    $rawProduct = RawProduct::mapArrayToNewDto($input, self::defineRequestParamsToRawProductMap());

    $requestParams2RegionMap = self::defineRequestParamsToRawProductRegionMap();
    if (!array_key_exists(self::ATTR_REGIONS, $input)) {
      return $rawProduct;
    }

    foreach ($input[self::ATTR_REGIONS] as $regionName => $regionProperties) {
      $rawProductRegion = RawProductRegion::mapArrayToNewDto($regionProperties, $requestParams2RegionMap);
      $rawProduct->setRegion($regionName, $rawProductRegion);
    }

    return $rawProduct;
  }

  /**
   * @return array
   */
  abstract protected static function defineValidationRules(): array;

  /**
   * @return array
   */
  private static function defineRequestParamsToRawProductMap(): array
  {
    return [
      self::ATTR_PRODUCT_ID => RawProduct::ATTR_PRODUCT_ID,
      self::ATTR_NAME => RawProduct::ATTR_NAME,
      self::ATTR_TYPE => RawProduct::ATTR_TYPE,
    ];
  }

  /**
   * @return array
   */
  private static function defineRequestParamsToRawProductRegionMap(): array
  {
    return [
      self::ATTR_REGION_URL => RawProductRegion::ATTR_URL,
      self::ATTR_REGION_DESCRIPTION => RawProductRegion::ATTR_DESCRIPTION,
      self::ATTR_REGION_RAW_PRICE => RawProductRegion::ATTR_RAW_PRICE,
    ];
  }
}
