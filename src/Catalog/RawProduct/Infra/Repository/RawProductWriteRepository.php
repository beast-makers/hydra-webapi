<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Repository;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Storage\RawProductTable as rpt;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class RawProductWriteRepository implements WriteRepository
{
  private QueryRunner $queryRunner;

  /**
   * @param QueryRunner $queryRunner
   */
  public function __construct(QueryRunner $queryRunner)
  {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->useWriteConnection();
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws DatabaseException
   */
  public function createRawProduct(RawProduct $rawProduct): void
  {
    $columnValueMap = $rawProduct->mapDtoToArray($this->rawProductToColumnsMap());
    $regionColumnValueMap = $this->prepareRegionProperties($rawProduct);
    $columnValueMap = array_merge($columnValueMap, $regionColumnValueMap);
    $listPair = rpt::prepareInsertLists($columnValueMap);

    $sql = <<<SQL
    INSERT INTO catalog.raw_product
        ({$listPair->getIdentifiers()})
        VALUES ({$listPair->getValues()});
SQL;

    $this->queryRunner->runQuery($sql, 'create raw product');
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws DatabaseException
   */
  public function updateRawProduct(RawProduct $rawProduct): void
  {
    $columnValueMap = $rawProduct->mapDtoToArray($this->rawProductToColumnsMap());
    $regionColumnValueMap = $this->prepareRegionProperties($rawProduct);
    $columnValueMap = array_merge($columnValueMap, $regionColumnValueMap);
    $updateList = rpt::prepareUpdateStatementList($columnValueMap);

    $sql = "UPDATE catalog.raw_product SET {$updateList} WHERE product_id=:product_id";
    $this->queryRunner->runQuery($sql, 'update raw product', [
      'product_id' => rpt::escapeLiteral($rawProduct->getProductId(), rpt::COLUMN_PRODUCT_ID),
    ]);
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return array
   */
  private function prepareRegionProperties(RawProduct $rawProduct): array
  {
    $regionsList = $rawProduct->regionsToArray();
    $regionsList = array_map(
      static fn ($regionProperties) => empty($regionProperties) ? '{}' : json_encode($regionProperties),
      $regionsList
    );

    return $regionsList;
  }

  /**
   * @return array
   */
  private function rawProductToColumnsMap(): array
  {
    return [
      RawProduct::ATTR_PRODUCT_ID => rpt::COLUMN_PRODUCT_ID,
      RawProduct::ATTR_TYPE => rpt::COLUMN_TYPE,
      RawProduct::ATTR_NAME => rpt::COLUMN_NAME,
    ];
  }
}
