<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Repository;

use BeastMakers\Catalog\Shared\Domain\RawProduct;

interface ReadRepository
{
  /**
   * @param string $productId
   *
   * @return RawProduct
   */
  public function findRawProductById(string $productId): RawProduct;

  /**
   * @param string $productName
   *
   * @return RawProduct
   */
  public function findRawProductByName(string $productName): RawProduct;
}
