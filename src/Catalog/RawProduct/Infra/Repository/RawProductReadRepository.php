<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Repository;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\Domain\RawProductRegion;
use BeastMakers\Catalog\Storage\RawProductTable as rpt;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class RawProductReadRepository implements ReadRepository
{
  private QueryRunner $queryRunner;

  public function __construct(QueryRunner $queryRunner)
  {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->connection()->connectOnce();
  }

  /**
   * @param string $productId
   *
   * @return RawProduct
   * @throws DatabaseException
   */
  public function findRawProductById(string $productId): RawProduct
  {
    $sql = 'SELECT * FROM catalog.raw_product WHERE product_id = :product_name;';

    /** @var resource $result */
    $result = $this->queryRunner->runQuery($sql, 'find product by productId', [
      'product_name' => rpt::escapeLiteral($productId, rpt::COLUMN_PRODUCT_ID),
    ]);

    return $this->mapFindRawProductResult($result);
  }

  /**
   * @param string $productName
   *
   * @return RawProduct
   * @throws DatabaseException
   */
  public function findRawProductByName(string $productName): RawProduct
  {
    $sql = 'SELECT * FROM catalog.raw_product WHERE name = :product_name;';

    /** @var resource $result */
    $result = $this->queryRunner->runQuery($sql, 'find product by name', [
      'product_name' => rpt::escapeLiteral($productName, rpt::COLUMN_NAME),
    ]);

    return $this->mapFindRawProductResult($result);
  }

  /**
   * @param $queryResult
   *
   * @return RawProduct
   */
  private function mapFindRawProductResult($queryResult): RawProduct
  {
    $rawProduct = new RawProduct();
    if ($dbRecord = pg_fetch_assoc($queryResult)) {
      $rawProduct->mapArrayToDto($dbRecord, $this->columnsToRawProductMap());

      foreach (Regions::ALL as $regionName) {
        if ($dbRecord[$regionName] !== '{}') {
          $regionProperties = json_decode($dbRecord[$regionName], true);
          $rawProductRegion = RawProductRegion::createFromArray($regionProperties);
          $rawProduct->setRegion($regionName, $rawProductRegion);
        }
      }
    }

    return $rawProduct;
  }

  /**
   * @return array
   */
  private function columnsToRawProductMap(): array
  {
    return [
      rpt::COLUMN_PRODUCT_ID => RawProduct::ATTR_PRODUCT_ID,
      rpt::COLUMN_TYPE => RawProduct::ATTR_TYPE,
      rpt::COLUMN_NAME => RawProduct::ATTR_NAME,
    ];
  }
}
