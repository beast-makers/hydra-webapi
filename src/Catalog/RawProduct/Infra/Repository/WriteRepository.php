<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Repository;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;

interface WriteRepository
{
  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws DatabaseException
   */
  public function createRawProduct(RawProduct $rawProduct): void;

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws DatabaseException
   */
  public function updateRawProduct(RawProduct $rawProduct): void;
}
