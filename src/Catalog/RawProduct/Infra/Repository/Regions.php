<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Infra\Repository;

interface Regions
{
  public const ALL = [
    'global',
    'de',
    'at',
    'fr',
    'de_de',
    'fr_fr',
    'at_de',
  ];
}
