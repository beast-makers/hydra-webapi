<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Domain\Validator;

use BeastMakers\Catalog\RawProduct\Infra\Repository\ReadRepository;
use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\Catalog\Shared\Result\RawProductValidationResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;

class CreateRawProductValidator
{
  private RawProductValidator $rawProductValidator;

  private ReadRepository $rawProductReadRepository;

  /**
   * @param RawProductValidator $rawProductValidator
   * @param ReadRepository $rawProductReadRepository
   */
  public function __construct(
    RawProductValidator $rawProductValidator,
    ReadRepository $rawProductReadRepository
  ) {
    $this->rawProductValidator = $rawProductValidator;
    $this->rawProductReadRepository = $rawProductReadRepository;
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws ResultException
   */
  public function validateRawProduct(RawProduct $rawProduct): void
  {
    $validateRawProductResult = new RawProductValidationResult();

    try {
      $this->verifyRawProductIsUnique($rawProduct);
      $this->rawProductValidator->validate($rawProduct);
    } catch (ArrayValidationException $e) {
      $validateRawProductResult->addError(new Error(ErrorCode::RAW_PRODUCT_VALIDATION, $e->getErrors()));
      throw new ResultException($validateRawProductResult);
    }
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws ResultException
   */
  private function verifyRawProductIsUnique(RawProduct $rawProduct): void
  {
    $result = new Result();

    $existingRawProduct = $this->rawProductReadRepository->findRawProductById($rawProduct->getProductId());
    if (!empty($existingRawProduct->getProductId())) {
      $result->addError(new Error(ErrorCode::RAW_PRODUCT_WITH_THIS_ID_ALREADY_EXISTS));

      throw new ResultException($result);
    }

    $existingRawProduct = $this->rawProductReadRepository->findRawProductByName($rawProduct->getName());
    if (!empty($existingRawProduct->getName())) {
      $result->addError(new Error(ErrorCode::RAW_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS));

      throw new ResultException($result);
    }
  }
}
