<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Domain\Validator;

use BeastMakers\Catalog\RawProduct\Infra\Repository\ReadRepository;
use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\Catalog\Shared\Result\RawProductValidationResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;

class UpdateRawProductValidator
{
  private RawProductValidator $rawProductValidator;

  private ReadRepository $rawProductReadRepository;

  /**
   * @param RawProductValidator $rawProductValidator
   * @param ReadRepository $rawProductReadRepository
   */
  public function __construct(
    RawProductValidator $rawProductValidator,
    ReadRepository $rawProductReadRepository
  ) {
    $this->rawProductValidator = $rawProductValidator;
    $this->rawProductReadRepository = $rawProductReadRepository;
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws ResultException
   */
  public function validateRawProduct(RawProduct $rawProduct): void
  {
    $validateRawProductResult = new RawProductValidationResult();

    try {
      $this->verifyRawProductExists($rawProduct->getProductId());
      $this->rawProductValidator->validate($rawProduct);
    } catch (ArrayValidationException $e) {
      $validateRawProductResult->addError(new Error(ErrorCode::RAW_PRODUCT_VALIDATION, $e->getErrors()));
      throw new ResultException($validateRawProductResult);
    }
  }

  /**
   * @param string $rawProductId
   *
   * @return void
   * @throws ResultException
   */
  public function verifyRawProductExists(string $rawProductId): void
  {
    $result = new Result();

    $existingRawProduct = $this->rawProductReadRepository->findRawProductById($rawProductId);
    if (empty($existingRawProduct->getProductId())) {
      $result->addError(new Error(ErrorCode::RAW_PRODUCT_NOT_FOUND));

      throw new ResultException($result);
    }
  }
}
