<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Domain\Validator;

use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;

class RawProductValidator
{
  private ArrayValidator $validator;

  private array $rules = [];

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return void
   * @throws ArrayValidationException
   */
  public function validate(RawProduct $rawProduct): void
  {
    $rawProductAsArray = $rawProduct->toArray();

    $this->initValidationRules();

    $this->validator->validate($rawProductAsArray, $this->rules);
  }

  /**
   * @return void
   */
  private function initValidationRules(): void
  {
    if (empty($this->rules)) {
      $allowedProductTypes = implode(',', [
        RawProduct::PRODUCT_TYPE_SIMPLE,
      ]);

      $this->rules = [
        RawProduct::ATTR_PRODUCT_ID => 'required|alpha_dash',
        RawProduct::ATTR_NAME => 'required',
        RawProduct::ATTR_TYPE => "required|in:{$allowedProductTypes}",
        RawProduct::ATTR_REGIONS => 'array',
      ];
    }
  }
}
