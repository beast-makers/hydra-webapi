<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Action;

use BeastMakers\Catalog\RawProduct\Domain\Validator\CreateRawProductValidator;
use BeastMakers\Catalog\RawProduct\Infra\Repository\WriteRepository;
use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\Result\CreateRawProductResult;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Result\ResultException;

class CreateRawProductCommand
{
  private CreateRawProductValidator $createRawProductValidator;

  private WriteRepository $rawProductWriteRepository;

  /**
   * @param CreateRawProductValidator $createRawProductValidator
   * @param WriteRepository $rawProductWriteRepository
   */
  public function __construct(
    CreateRawProductValidator $createRawProductValidator,
    WriteRepository $rawProductWriteRepository
  ) {
    $this->createRawProductValidator = $createRawProductValidator;
    $this->rawProductWriteRepository = $rawProductWriteRepository;
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return CreateRawProductResult
   * @throws DatabaseException
   */
  public function createRawProduct(RawProduct $rawProduct): CreateRawProductResult
  {
    $result = new CreateRawProductResult();

    try {
      $this->createRawProductValidator->validateRawProduct($rawProduct);
    } catch (ResultException $e) {
      $result->mergeWith($e->getResult());

      return $result;
    }

    $this->rawProductWriteRepository->createRawProduct($rawProduct);
    $result->setIsSuccess(true);

    return $result;
  }
}
