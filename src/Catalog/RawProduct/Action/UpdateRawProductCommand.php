<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Action;

use BeastMakers\Catalog\RawProduct\Domain\Validator\UpdateRawProductValidator;
use BeastMakers\Catalog\RawProduct\Infra\Repository\ReadRepository;
use BeastMakers\Catalog\RawProduct\Infra\Repository\WriteRepository;
use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\Catalog\Shared\Result\UpdateRawProductResult;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;

class UpdateRawProductCommand
{
  private UpdateRawProductValidator $updateRawProductValidator;

  private WriteRepository $rawProductWriteRepository;

  private ReadRepository $rawProductReadRepository;

  /**
   * @param UpdateRawProductValidator $updateRawProductValidator
   * @param ReadRepository $rawProductReadRepository
   * @param WriteRepository $rawProductWriteRepository
   */
  public function __construct(
    UpdateRawProductValidator $updateRawProductValidator,
    ReadRepository $rawProductReadRepository,
    WriteRepository $rawProductWriteRepository
  ) {
    $this->updateRawProductValidator = $updateRawProductValidator;
    $this->rawProductReadRepository = $rawProductReadRepository;
    $this->rawProductWriteRepository = $rawProductWriteRepository;
  }

  /**
   * @param RawProduct $rawProduct
   *
   * @return UpdateRawProductResult
   * @throws DatabaseException
   */
  public function updateRawProduct(RawProduct $rawProduct): UpdateRawProductResult
  {
    $result = new UpdateRawProductResult();

    try {
      $existingRawProduct = $this->readExistingRawProduct($rawProduct->getProductId());
      $mergedProduct = $existingRawProduct->merge($rawProduct);
      $this->updateRawProductValidator->validateRawProduct($mergedProduct);
    } catch (ResultException $e) {
      $result->mergeWith($e->getResult());

      return $result;
    }

    $this->rawProductWriteRepository->updateRawProduct($mergedProduct);
    $result->setIsSuccess(true);

    return $result;
  }

  /**
   * @param string $rawProductId
   *
   * @return RawProduct
   * @throws ResultException
   */
  private function readExistingRawProduct(string $rawProductId): RawProduct
  {
    $result = new Result();
    $existingRawProduct = $this->rawProductReadRepository->findRawProductById($rawProductId);
    if (empty($existingRawProduct->getProductId())) {
      $result->addError(new Error(ErrorCode::RAW_PRODUCT_NOT_FOUND));
      throw new ResultException($result);
    }

    return $existingRawProduct;
  }
}
