<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Action\Http;

use BeastMakers\Catalog\RawProduct\Factory;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class CreateRawProduct extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_ADMIN];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws DatabaseException
   * @throws ArrayValidationException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $rawProduct = $this->factory->createRawProductFromCreateRawProductRequest($request);
    $cmd = $this->factory->newCreateRawProductCommand();
    $result = $cmd->createRawProduct($rawProduct);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];
    $body = Stream::create('');

    return new Response(201, $headers, $body);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::INVALID_JSON_FORMAT => 400,
      ErrorCode::RAW_PRODUCT_WITH_THIS_ID_ALREADY_EXISTS => 422,
      ErrorCode::RAW_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS => 422,
      ErrorCode::RAW_PRODUCT_VALIDATION => 400,
    ];
  }
}
