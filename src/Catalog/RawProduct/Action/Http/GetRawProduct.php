<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Action\Http;

use BeastMakers\Catalog\RawProduct\Factory;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\SecurityApi\ApiAuth\Infra\Middleware\ApiAccessGuardMiddleware;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class GetRawProduct extends BaseHttpAction
{
  public const ALLOWED_API_ROLES = [ApiAccessGuardMiddleware::ROLE_READ_ONLY];

  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $cmd = $this->factory->newGetRawProductCommand();
    $productId = $request->getAttribute('productId', '');
    $result = $cmd->fetchRawProduct($productId);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];
    /** @psalm-suppress PossiblyNullReference */
    $body = Stream::create(json_encode($result->getRawProduct()->toArray()));

    return new Response(200, $headers, $body);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::RAW_PRODUCT_NOT_FOUND => 404,
    ];
  }
}
