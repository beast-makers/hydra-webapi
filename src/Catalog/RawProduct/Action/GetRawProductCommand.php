<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct\Action;

use BeastMakers\Catalog\RawProduct\Infra\Repository\ReadRepository;
use BeastMakers\Catalog\Shared\ErrorCode;
use BeastMakers\Catalog\Shared\Result\GetRawProductResult;
use BeastMakers\Shared\Result\Error;

class GetRawProductCommand
{
  private ReadRepository $rawProductReadRepository;

  /**
   * @param ReadRepository $rawProductReadRepository
   */
  public function __construct(
    ReadRepository $rawProductReadRepository
  ) {
    $this->rawProductReadRepository = $rawProductReadRepository;
  }

  /**
   * @param string $productId
   *
   * @return GetRawProductResult
   */
  public function fetchRawProduct(string $productId): GetRawProductResult
  {
    $result = new GetRawProductResult();

    $rawProduct = $this->rawProductReadRepository->findRawProductById($productId);
    if (empty($rawProduct->getProductId())) {
      $result->addError(new Error(ErrorCode::RAW_PRODUCT_NOT_FOUND));
      return $result;
    }

    $result->setRawProduct($rawProduct);
    $result->setIsSuccess(true);

    return $result;
  }
}
