<?php
declare(strict_types=1);

namespace BeastMakers\Catalog\RawProduct;

use BeastMakers\Catalog\RawProduct\Action\CreateRawProductCommand;
use BeastMakers\Catalog\RawProduct\Action\GetRawProductCommand;
use BeastMakers\Catalog\RawProduct\Action\UpdateRawProductCommand;
use BeastMakers\Catalog\RawProduct\Domain\Validator\CreateRawProductValidator;
use BeastMakers\Catalog\RawProduct\Domain\Validator\RawProductValidator;
use BeastMakers\Catalog\RawProduct\Domain\Validator\UpdateRawProductValidator;
use BeastMakers\Catalog\RawProduct\Infra\Repository\RawProductReadRepository;
use BeastMakers\Catalog\RawProduct\Infra\Repository\RawProductWriteRepository;
use BeastMakers\Catalog\RawProduct\Infra\Repository\ReadRepository;
use BeastMakers\Catalog\RawProduct\Infra\Repository\WriteRepository;
use BeastMakers\Catalog\RawProduct\Infra\Request\CreateRawProductRequest;
use BeastMakers\Catalog\RawProduct\Infra\Request\UpdateRawProductRequest;
use BeastMakers\Catalog\Shared\Domain\RawProduct;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return CreateRawProductCommand
   */
  public function newCreateRawProductCommand(): CreateRawProductCommand
  {
    return new CreateRawProductCommand(
      $this->shareCreateRawProductValidator(),
      $this->shareProductWriteRepository()
    );
  }

  /**
   * @return UpdateRawProductCommand
   */
  public function newUpdateRawProductCommand(): UpdateRawProductCommand
  {
    return new UpdateRawProductCommand(
      $this->newUpdateRawProductValidator(),
      $this->shareRawProductReadRepository(),
      $this->shareProductWriteRepository()
    );
  }

  /**
   * @return GetRawProductCommand
   */
  public function newGetRawProductCommand(): GetRawProductCommand
  {
    return new GetRawProductCommand($this->shareRawProductReadRepository());
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return RawProduct
   * @throws ArrayValidationException
   */
  public function createRawProductFromCreateRawProductRequest(ServerRequestInterface $request): RawProduct
  {
    $createRawProductRequest = new CreateRawProductRequest($this->dependencyProvider->shareArrayValidator());

    return $createRawProductRequest->createRawProductFromRequest($request);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return RawProduct
   * @throws ArrayValidationException
   */
  public function createRawProductFromUpdateRawProductRequest(ServerRequestInterface $request): RawProduct
  {
    $createRawProductRequest = new UpdateRawProductRequest($this->dependencyProvider->shareArrayValidator());

    return $createRawProductRequest->createRawProductFromRequest($request);
  }

  /**
   * @return WriteRepository
   */
  protected function newRawProductWriteRepository(): WriteRepository
  {
    return new RawProductWriteRepository($this->dependencyProvider->loadWebApiQueryRunner());
  }

  /**
   * @return ReadRepository
   */
  protected function newRawProductReadRepository(): ReadRepository
  {
    return new RawProductReadRepository($this->dependencyProvider->loadWebApiQueryRunner());
  }

  /**
   * @return CreateRawProductValidator
   */
  protected function newCreateRawProductValidator(): CreateRawProductValidator
  {
    return new CreateRawProductValidator($this->shareRawProductValidator(), $this->shareRawProductReadRepository());
  }

  /**
   * @return RawProductValidator
   */
  protected function newRawProductValidator(): RawProductValidator
  {
    return new RawProductValidator($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return UpdateRawProductValidator
   */
  private function newUpdateRawProductValidator(): UpdateRawProductValidator
  {
    return new UpdateRawProductValidator(
      $this->shareRawProductValidator(),
      $this->shareRawProductReadRepository()
    );
  }

  /**
   * @return ReadRepository
   */
  protected function shareRawProductReadRepository(): ReadRepository
  {
    return $this->shareInstance(RawProductReadRepository::class, static function (Factory $factory) {
      return $factory->newRawProductReadRepository();
    });
  }

  /**
   * @return WriteRepository
   */
  protected function shareProductWriteRepository(): WriteRepository
  {
    return $this->shareInstance(RawProductWriteRepository::class, static function (Factory $factory) {
      return $factory->newRawProductWriteRepository();
    });
  }

  /**
   * @return CreateRawProductValidator
   */
  protected function shareCreateRawProductValidator(): CreateRawProductValidator
  {
    return $this->shareInstance(CreateRawProductValidator::class, static function (Factory $factory) {
      return $factory->newCreateRawProductValidator();
    });
  }

  /**
   * @return RawProductValidator
   */
  protected function shareRawProductValidator(): RawProductValidator
  {
    return $this->shareInstance(RawProductValidator::class, static function (Factory $factory) {
      return $factory->newRawProductValidator();
    });
  }
}
