<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Storage;

use BeastMakers\Shared\DatabaseConnector\PostgreSQL\DbTable;

class AclTable extends DbTable
{
  public const COLUMN_USERNAME = 'username';
  public const COLUMN_IS_ACTIVE = 'is_active';
  public const COLUMN_SECRET = 'secret';
  public const COLUMN_SEED = 'seed';
  public const COLUMN_ROLES = 'roles';

  protected const TYPES = [
    self::COLUMN_USERNAME => self::TYPE_TEXT,
    self::COLUMN_IS_ACTIVE => self::TYPE_BOOL,
    self::COLUMN_SECRET => self::TYPE_TEXT,
    self::COLUMN_SEED => self::TYPE_TEXT,
    self::COLUMN_ROLES => self::TYPE_TEXT_ARRAY,
  ];
}
