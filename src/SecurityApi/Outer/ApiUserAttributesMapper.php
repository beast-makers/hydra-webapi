<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Outer;

use BeastMakers\SecurityApi\Shared\Domain\ApiUserAttributes;

class ApiUserAttributesMapper
{
  public static function toOutside(ApiUserAttributes $apiUserAttributes): array
  {
    return $apiUserAttributes->mapDtoToArray([
      ApiUserAttributes::ATTR_USER_ROLES => 'user_roles',
    ]);
  }
}
