<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\Result\Result;

class AuthenticationResult extends Result
{
  private ApiUser $apiUser;

  /**
   * @return ApiUser
   */
  public function getApiUser(): ApiUser
  {
    return $this->apiUser;
  }

  /**
   * @param ApiUser $apiUser
   */
  public function setApiUser(ApiUser $apiUser): void
  {
    $this->apiUser = $apiUser;
  }
}
