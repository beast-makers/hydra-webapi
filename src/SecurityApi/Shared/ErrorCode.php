<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared;

use BeastMakers\Shared\Result\CommonErrorCodes;

interface ErrorCode extends CommonErrorCodes
{
  public const API_USER_ALREADY_EXISTS = 'API_USER_ALREADY_EXISTS';
  public const API_USER_VALIDATION = 'API_USER_VALIDATION';
  public const INVALID_PASSWORD = 'INVALID_PASSWORD';
  public const USER_NOTFOUND = 'USER_NOTFOUND';
  public const INVALID_CREDENTIALS = 'INVALID_CREDENTIALS';
}
