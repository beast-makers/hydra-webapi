<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ConnectionData extends ArrayDto
{
  public const ATTR_USERNAME = 'username';
  public const ATTR_EXPIRY = 'expiry';

  public function getUsername(): string
  {
    return $this->get(self::ATTR_USERNAME, '');
  }

  public function getExpiry(): int
  {
    return $this->get(self::ATTR_EXPIRY, '');
  }

  public function definition(): array
  {
    return [
      self::ATTR_USERNAME => self::DEF_SCALAR,
      self::ATTR_EXPIRY => self::DEF_INT,
    ];
  }
}
