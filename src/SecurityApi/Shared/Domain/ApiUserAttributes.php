<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ApiUserAttributes extends ArrayDto
{
  public const ATTR_USER_ROLES = 'user_roles';

  /**
   * @return array
   */
  public function getUserRoles(): array
  {
    return $this->get(self::ATTR_USER_ROLES, '');
  }

  /**
   * @param array $userRoles
   *
   * @return ApiUserAttributes
   */
  public function setUserRoles(array $userRoles): self
  {
    $this->set(self::ATTR_USER_ROLES, $userRoles);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_USER_ROLES => self::DEF_ARRAY_UNIQUE,
    ];
  }
}
