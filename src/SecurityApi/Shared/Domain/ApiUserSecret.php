<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared\Domain;

class ApiUserSecret
{
  /**
   * @param string $password
   * @param string $seed
   *
   * @return string
   */
  public static function build(string $password, string $seed): string
  {
    return hash_hmac('sha256', $password, $seed);
  }

  /**
   * @param string $secret
   * @param string $seed
   * @param string $password
   *
   * @return bool
   */
  public static function verify(string $secret, string $seed, string $password): bool
  {
    return self::build($password, $seed) === $secret;
  }
}
