<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ApiUser extends ArrayDto
{
  public const ATTR_USERNAME = 'username';
  public const ATTR_IS_ACTIVE = 'is_active';
  public const ATTR_ROLES = 'roles';
  public const ATTR_SECRET = 'secret';
  public const ATTR_SEED = 'seed';

  public const ROLE_ADMIN = 'admin';
  public const ROLE_READ_ONLY = 'read-only';
  public const ROLE_EDITOR = 'editor';
  public const ROLE_CREATOR = 'creator';
  /**
   * @return string
   */
  public function getUsername(): string
  {
    return $this->get(self::ATTR_USERNAME, '');
  }

  /**
   * @param string $username
   *
   * @return ApiUser
   */
  public function setUsername(string $username): self
  {
    $this->set(self::ATTR_USERNAME, $username);

    return $this;
  }

  /**
   * @return bool
   */
  public function getIsActive(): bool
  {
    return $this->get(self::ATTR_IS_ACTIVE, false);
  }

  /**
   * @param bool $isActive
   *
   * @return ApiUser
   */
  public function setIsActive(bool $isActive): self
  {
    $this->set(self::ATTR_IS_ACTIVE, $isActive);

    return $this;
  }

  /**
   * @return array
   */
  public function getRoles(): array
  {
    return $this->get(self::ATTR_ROLES, []);
  }

  /**
   * @param array $userRoles
   *
   * @return ApiUser
   */
  public function setRoles(array $userRoles): self
  {
    $this->set(self::ATTR_ROLES, $userRoles);

    return $this;
  }

  /**
   * @return string
   */
  public function getSecret(): string
  {
    return $this->get(self::ATTR_SECRET, '');
  }

  /**
   * @param string $secret
   *
   * @return ApiUser
   */
  public function setSecret(string $secret): self
  {
    $this->set(self::ATTR_SECRET, $secret);

    return $this;
  }

  /**
   * @return string
   */
  public function getSeed(): string
  {
    return $this->get(self::ATTR_SEED, '');
  }

  /**
   * @param string $seed
   *
   * @return ApiUser
   */
  public function setSeed(string $seed): self
  {
    $this->set(self::ATTR_SEED, $seed);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_USERNAME => self::DEF_SCALAR,
      self::ATTR_IS_ACTIVE => self::DEF_BOOL,
      self::ATTR_ROLES => self::DEF_ARRAY_UNIQUE,
      self::ATTR_SECRET => self::DEF_SCALAR,
      self::ATTR_SEED => self::DEF_SCALAR,
    ];
  }
}
