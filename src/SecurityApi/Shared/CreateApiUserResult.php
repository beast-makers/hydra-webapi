<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Shared;

use BeastMakers\Shared\Result\Result;

class CreateApiUserResult extends Result
{
}
