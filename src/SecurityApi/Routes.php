<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi;

use BeastMakers\SecurityApi\Acl\Action\Http\CreateApiUser;
use BeastMakers\SecurityApi\ApiAuth\Action\Http\ApiAuth;
use BeastMakers\Shared\Kernel\Middleware\IsValidJsonMiddleware;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_API_AUTH_LOGIN = 'api-auth.login';
  public const ROUTE_CREATE_API_USER = 'acl.create-api-user';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $isValidJsonMiddleware = new IsValidJsonMiddleware();

    $app->post('/api-auth/login', ApiAuth::class)
      ->setName(self::ROUTE_API_AUTH_LOGIN);

    $app->post('/acl/create-api-user/{username}', CreateApiUser::class)
      ->setName(self::ROUTE_CREATE_API_USER)
      ->add($isValidJsonMiddleware);
  }
}
