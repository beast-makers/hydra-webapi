--liquibase formatted sql

--changeset konradg:001
CREATE SEQUENCE IF NOT EXISTS security_api.seq_api_user AS BIGINT
    INCREMENT 3 START 1;
SELECT setval('security_api.seq_api_user', 1);

--changeset konradg:002
CREATE TABLE IF NOT EXISTS security_api.api_user (
  dbid BIGINT PRIMARY KEY DEFAULT nextval('security_api.seq_api_user'),
  username VARCHAR UNIQUE NOT NULL,
  is_active BOOLEAN NOT NULL,
  secret VARCHAR NOT NULL,
  seed VARCHAR NOT NULL,
  roles text[],
  created_at TIMESTAMPTZ DEFAULT now(),
  updated_at TIMESTAMPTZ DEFAULT now()
);

--changeset konradg:003
ALTER SEQUENCE security_api.seq_api_user OWNED BY security_api.api_user.dbid;

--changeset konradg:004
CREATE INDEX IF NOT EXISTS security_api__api_user__username ON security_api.api_user USING BTREE (username);

--changset konradg:005
CREATE TRIGGER update_api_user_updated_at
    BEFORE UPDATE ON security_api.api_user
    FOR EACH ROW
EXECUTE PROCEDURE public.updated_at_timestamp_column();
