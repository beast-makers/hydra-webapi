<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl;

use BeastMakers\Shared\DatabaseConnector\QueryRunner;
use BeastMakers\Shared\DependencyLoader\OwnArrayValidator;
use BeastMakers\Shared\DependencyLoader\WebapiQueryRunner;
use BeastMakers\Shared\Kernel\BaseDependencyProvider;

class DependencyProvider extends BaseDependencyProvider
{
  use WebapiQueryRunner;
  use OwnArrayValidator;

  /**
   * @return QueryRunner
   */
  public function loadApiUserQueryRunner(): QueryRunner
  {
    return $this->shareWebapiQueryRunner();
  }
}
