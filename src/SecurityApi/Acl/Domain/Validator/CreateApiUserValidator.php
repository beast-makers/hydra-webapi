<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Domain\Validator;

use BeastMakers\SecurityApi\Acl\Infra\Repository\ReadRepository;
use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\SecurityApi\Shared\ErrorCode;
use BeastMakers\SecurityApi\Shared\ValidateApiUserResult;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Result\ResultException;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class CreateApiUserValidator
{
  private ApiUserValidator $apiUserValidator;

  private ReadRepository $aclReadRepository;

  /**
   * @param ApiUserValidator $apiUserValidator
   * @param ReadRepository $apiUserReadRepository
   */
  public function __construct(
    ApiUserValidator $apiUserValidator,
    ReadRepository $apiUserReadRepository
  ) {
    $this->apiUserValidator = $apiUserValidator;
    $this->aclReadRepository = $apiUserReadRepository;
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return ValidateApiUserResult
   * @throws RuleNotFoundException
   */
  public function validateApiUser(ApiUser $apiUser): ValidateApiUserResult
  {
    $apiUserValidationResult = new ValidateApiUserResult();

    try {
      if (!empty($apiUser->getUsername())) {
        $this->verifyApiUserIsUnique($apiUser);
      }
      $this->validateApiUserEntity($apiUser);
    } catch (ResultException $e) {
      $apiUserValidationResult->mergeWith($e->getResult());
      $apiUserValidationResult->setIsSuccess(false);

      return $apiUserValidationResult;
    }

    $apiUserValidationResult->setIsSuccess(true);

    return $apiUserValidationResult;
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return void
   * @throws ResultException
   */
  private function verifyApiUserIsUnique(ApiUser $apiUser): void
  {
    $existingApiUser = $this->aclReadRepository->readApiUser($apiUser->getUsername());
    if (!$existingApiUser->isEmpty()) {
      $result = new Result();
      $result->addError(new Error(ErrorCode::API_USER_ALREADY_EXISTS));

      throw new ResultException($result);
    }
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return void
   * @throws ResultException
   * @throws RuleNotFoundException
   */
  private function validateApiUserEntity(ApiUser $apiUser): void
  {
    try {
      $this->apiUserValidator->validate($apiUser);
    } catch (ArrayValidationException $e) {
      $result = new Result();
      $result->addError(new Error(ErrorCode::API_USER_VALIDATION, $e->getErrors()));

      throw new ResultException($result);
    }
  }
}
