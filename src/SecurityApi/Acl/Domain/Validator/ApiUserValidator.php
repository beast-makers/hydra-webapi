<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Domain\Validator;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

class ApiUserValidator
{
  private ArrayValidator $validator;

  private array $rules = [];

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return void
   * @throws ArrayValidationException
   * @throws RuleNotFoundException
   */
  public function validate(ApiUser $apiUser): void
  {
    $apiUserAsArray = $apiUser->asArray();

    $this->validator->validate($apiUserAsArray, $this->loadValidationRules());
  }

  /**
   * @return array
   * @throws RuleNotFoundException
   */
  private function loadValidationRules(): array
  {
    if (empty($this->rules)) {
      $v = $this->validator->rakit();
      $notAllowedCharacters = '/!¡?÷?¿\/\=#$%ˆ&*(){}|~<>;:[\]]/';
      $allowedRoles = self::allowedRoles();

      $this->rules = [
        ApiUser::ATTR_USERNAME => ['required', $v('not_regex', $notAllowedCharacters)],
        ApiUser::ATTR_IS_ACTIVE => 'required|boolean',
        ApiUser::ATTR_ROLES => "required|array|all_in:{$allowedRoles}",
        ApiUser::ATTR_SECRET => 'required',
        ApiUser::ATTR_SEED => 'required',
      ];
    }

    return $this->rules;
  }

  /**
   * @return string
   */
  private static function allowedRoles(): string
  {
    return implode(',', [
      ApiUser::ROLE_ADMIN,
      ApiUser::ROLE_READ_ONLY,
      ApiUser::ROLE_CREATOR,
      ApiUser::ROLE_EDITOR,
    ]);
  }
}
