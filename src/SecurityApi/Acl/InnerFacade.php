<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl;

use BeastMakers\SecurityApi\Shared\AuthenticationResult;
use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\Kernel\BaseInnerFacade;

/**
 * @property Factory $factory
 */
class InnerFacade extends BaseInnerFacade
{
  /**
   * @param string $username
   * @param string $password
   * @return AuthenticationResult
   */
  public function authenticateApiUser(string $username, string $password): AuthenticationResult
  {
    return $this->factory->newAuthenticationService()->authenticateApiUser($username, $password);
  }

  /**
   * @param string $username
   * @return ApiUser
   */
  public function readApiUser(string $username): ApiUser
  {
    return $this->factory->shareAclReadRepository()->readApiUser($username);
  }
}
