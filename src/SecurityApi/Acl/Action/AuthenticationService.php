<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Action;

use BeastMakers\SecurityApi\Acl\Infra\Repository\ReadRepository;
use BeastMakers\SecurityApi\Shared\AuthenticationResult;
use BeastMakers\SecurityApi\Shared\Domain\ApiUserSecret;
use BeastMakers\SecurityApi\Shared\ErrorCode;
use BeastMakers\Shared\Result\Error;

class AuthenticationService
{
  private ReadRepository $aclReadRepository;

  /**
   * @param ReadRepository $aclReadRepository
   */
  public function __construct(ReadRepository $aclReadRepository)
  {
    $this->aclReadRepository = $aclReadRepository;
  }

  /**
   * @param string $username
   * @param string $password
   * @return AuthenticationResult
   */
  public function authenticateApiUser(string $username, string $password): AuthenticationResult
  {
    $result = new AuthenticationResult();
    $apiUser = $this->aclReadRepository->readApiUser($username);

    if (empty($apiUser->getUsername())) {
      $result->addError((new Error(ErrorCode::USER_NOTFOUND)));

      return $result;
    }

    if (!ApiUserSecret::verify($apiUser->getSecret(), $apiUser->getSeed(), $password)) {
      $result->addError((new Error(ErrorCode::INVALID_PASSWORD)));

      return $result;
    }

    $result->setApiUser($apiUser);
    $result->setIsSuccess(true);

    return $result;
  }
}
