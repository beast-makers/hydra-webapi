<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Action;

use BeastMakers\SecurityApi\Acl\Domain\Validator\CreateApiUserValidator;
use BeastMakers\SecurityApi\Acl\Infra\Repository\WriteRepository;
use BeastMakers\SecurityApi\Shared\CreateApiUserResult;
use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\SecurityApi\Shared\Domain\ApiUserSecret;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use Rakit\Validation\RuleNotFoundException;

class CreateApiUserService
{
  private CreateApiUserValidator $apiUserValidator;

  private WriteRepository $aclWriteRepository;

  /**
   * @param CreateApiUserValidator $apiUserValidator
   * @param WriteRepository $aclWriteRepository
   */
  public function __construct(
    CreateApiUserValidator $apiUserValidator,
    WriteRepository $aclWriteRepository
  ) {
    $this->apiUserValidator = $apiUserValidator;
    $this->aclWriteRepository = $aclWriteRepository;
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return CreateApiUserResult
   * @throws DatabaseException
   * @throws RuleNotFoundException
   */
  public function createApiUser(ApiUser $apiUser): CreateApiUserResult
  {
    $result = new CreateApiUserResult();

    $this->fillApiUser($apiUser);
    $validationResult = $this->apiUserValidator->validateApiUser($apiUser);
    if (!$validationResult->getIsSuccess()) {
      $result->mergeWith($validationResult);

      return $result;
    }

    $this->aclWriteRepository->createApiUser($apiUser);
    $result->setIsSuccess(true);

    return $result;
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return void
   */
  private function fillApiUser(ApiUser $apiUser): void
  {
    $secretSeed = uniqid();

    $apiUser->applyArray([
      ApiUser::ATTR_SEED => $secretSeed,
      ApiUser::ATTR_SECRET => ApiUserSecret::build($apiUser->getSecret(), $secretSeed),
    ]);
  }
}
