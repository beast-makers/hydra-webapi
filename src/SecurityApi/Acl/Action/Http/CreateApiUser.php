<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Action\Http;

use BeastMakers\SecurityApi\Acl\Factory;
use BeastMakers\SecurityApi\Shared\ErrorCode;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\RuleNotFoundException;

/**
 * @property Factory $factory
 */
class CreateApiUser extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws DatabaseException
   * @throws RuleNotFoundException
   * @throws ArrayValidationException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $apiUser = $this->factory->createApiUserFromCreateApiUserRequest($request);
    $model = $this->factory->newCreateApiUserService();
    $result = $model->createApiUser($apiUser);

    if (!$result->getIsSuccess()) {
      $httpCodeErrorMap = $this->getHttpCodeErrorMap();

      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($result, $httpCodeErrorMap);
    }

    $headers = [
      'Content-type' => 'application/json',
    ];

    return new Response(201, $headers, '');
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::API_USER_VALIDATION => 400,
      ErrorCode::API_USER_ALREADY_EXISTS => 422,
    ];
  }
}
