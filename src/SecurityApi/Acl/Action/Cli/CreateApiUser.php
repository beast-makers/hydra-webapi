<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Action\Cli;

use BeastMakers\SecurityApi\Acl\Factory;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\Kernel\Action\BaseCliAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\RuleNotFoundException;

/**
 * @property Factory $factory
 */
class CreateApiUser extends BaseCliAction
{
  /**
   * @return string
   * @throws DatabaseException
   * @throws RuleNotFoundException
   * @throws ArrayValidationException
   */
  public function run(): string
  {
    $actionInput = $this->readStdIn();
    $model = $this->factory->newCreateApiUserService();
    $apiUser = $this->factory->createApiUserFromJson($actionInput);
    $result = $model->createApiUser($apiUser);

    if (!$result->getIsSuccess()) {
      echo "\nFailed\n";
      //TODO: print errors
      /** @psalm-suppress ForbiddenCode */
      var_dump($result->getErrorCollection()->getErrors());
      exit(1);
    }

    echo "\nOK User [{$apiUser->getUsername()}] created\n";
    exit(0);
  }
}
