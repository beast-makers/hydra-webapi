<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Infra\Request;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class CreateApiUserRequest
{
  public const PARAM_USERNAME = 'username';
  public const PARAM_PASSWORD = 'password';
  public const PARAM_IS_ACTIVE = 'is_active';
  public const PARAM_ROLES = 'roles';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return ApiUser
   * @throws ArrayValidationException
   */
  public function createApiUserFromRequest(ServerRequestInterface $request): ApiUser
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $params[self::PARAM_USERNAME] = $request->getAttribute(self::PARAM_USERNAME);

    $request->getBody()->rewind();

    return $this->createApiUserFromArray($params);
  }

  /**
   * @param string $jsonPayload
   *
   * @return ApiUser
   * @throws ArrayValidationException
   */
  public function createApiUserFromJson(string $jsonPayload): ApiUser
  {
    $input = json_decode($jsonPayload, true);

    return $this->createApiUserFromArray($input);
  }

  /**
   * @param array $input
   *
   * @return ApiUser
   * @throws ArrayValidationException
   */
  private function createApiUserFromArray(array $input): ApiUser
  {
    $this->validateParams($input);
    $map = self::defineRequestParamsToApiUserMap();

    return ApiUser::mapArrayToNewDto($input, $map);
  }

  /**
   * @param array $params
   *
   * @return void
   * @throws ArrayValidationException
   */
  private function validateParams(array $params): void
  {
    $this->validator->validate($params, static::defineValidationRules());
  }

  /**
   * @return array
   */
  private static function defineValidationRules(): array
  {
    return [
      self::PARAM_USERNAME => 'required',
      self::PARAM_PASSWORD => 'required',
      self::PARAM_IS_ACTIVE => 'required',
      self::PARAM_ROLES => 'required',
    ];
  }

  /**
   * @return array
   */
  private static function defineRequestParamsToApiUserMap(): array
  {
    return [
      self::PARAM_USERNAME => ApiUser::ATTR_USERNAME,
      self::PARAM_IS_ACTIVE => ApiUser::ATTR_IS_ACTIVE,
      self::PARAM_PASSWORD => ApiUser::ATTR_SECRET,
      self::PARAM_ROLES => ApiUser::ATTR_ROLES,
    ];
  }
}
