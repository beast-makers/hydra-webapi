<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Infra\Repository;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;

interface ReadRepository
{
  /**
   * @param string $username
   *
   * @return ApiUser
   */
  public function readApiUser(string $username): ApiUser;
}
