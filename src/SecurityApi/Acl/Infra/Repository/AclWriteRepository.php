<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Infra\Repository;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\SecurityApi\Storage\AclTable as at;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class AclWriteRepository implements WriteRepository
{
  private QueryRunner $queryRunner;

  /**
   * @param QueryRunner $queryRunner
   */
  public function __construct(QueryRunner $queryRunner)
  {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->useWriteConnection();
  }

  /**
   * @inheritDoc
   *
   * @throws \Throwable
   */
  public function createApiUser(ApiUser $apiUser): void
  {
    $this->queryRunner->connection()->runInTransaction(function () use ($apiUser): void {
      $this->createApiUserRecord($apiUser);
    });
  }

  /**
   * @param ApiUser $apiUser
   *
   * @return string
   * @throws DatabaseException
   */
  private function createApiUserRecord(ApiUser $apiUser): string
  {
    $columnValueMap = $apiUser->mapDtoToArray($this->apiUserToColumnsMap());
    $listPair = at::prepareInsertLists($columnValueMap);

    $sql = <<<SQL
INSERT INTO security_api.api_user ({$listPair->getIdentifiers()})
    VALUES ({$listPair->getValues()})
RETURNING dbid;
SQL;

    $result = $this->queryRunner->runQuery($sql, 'create api user');

    return pg_fetch_assoc($result)['dbid'];
  }

  /**
   * @return array
   */
  private function apiUserToColumnsMap(): array
  {
    return [
      ApiUser::ATTR_USERNAME => at::COLUMN_USERNAME,
      ApiUser::ATTR_IS_ACTIVE => at::COLUMN_IS_ACTIVE,
      ApiUser::ATTR_SECRET => at::COLUMN_SECRET,
      ApiUser::ATTR_SEED => at::COLUMN_SEED,
      ApiUser::ATTR_ROLES => at::COLUMN_ROLES,
    ];
  }
}
