<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Infra\Repository;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;

interface WriteRepository
{
  /**
   * @param ApiUser $apiUser
   *
   * @return void
   * @throws DatabaseException
   */
  public function createApiUser(ApiUser $apiUser): void;
}
