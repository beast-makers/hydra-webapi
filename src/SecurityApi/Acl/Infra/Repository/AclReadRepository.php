<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl\Infra\Repository;

use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\SecurityApi\Storage\AclTable as at;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner;

class AclReadRepository implements ReadRepository
{
  private QueryRunner $queryRunner;

  public function __construct(
    QueryRunner $queryRunner
  ) {
    $this->queryRunner = $queryRunner;

    $this->queryRunner->connection()->connectOnce();
  }

  /**
   * @param string $username
   * @return ApiUser
   * @throws DatabaseException
   */
  public function readApiUser(string $username): ApiUser
  {
    $sql = <<<'SQL'
SELECT usr.*
    FROM security_api.api_user usr
    WHERE usr.username = :username
SQL;

    /** @var resource $result */
    $result = $this->queryRunner->runQuery($sql, 'get api user', [
      'username' => at::escapeLiteral($username, at::COLUMN_USERNAME),
    ]);

    return $this->mapFindApiUserResult($result);
  }

  /**
   * @param $queryResult
   *
   * @return ApiUser
   */
  private function mapFindApiUserResult($queryResult): ApiUser
  {
    $apiUser = new ApiUser();
    if ($dbRecord = pg_fetch_assoc($queryResult)) {
      $apiUser->mapArrayToDto($dbRecord, $this->columnsToApiUserMap());
      $apiUser->setRoles(at::dbArrayToPhpArray($dbRecord['roles']));
    }

    return $apiUser;
  }

  /**
   * @return array
   */
  private function columnsToApiUserMap(): array
  {
    return [
      at::COLUMN_USERNAME => ApiUser::ATTR_USERNAME,
      at::COLUMN_IS_ACTIVE => ApiUser::ATTR_IS_ACTIVE,
      at::COLUMN_SECRET => ApiUser::ATTR_SECRET,
      at::COLUMN_SEED => ApiUser::ATTR_SEED,
    ];
  }
}
