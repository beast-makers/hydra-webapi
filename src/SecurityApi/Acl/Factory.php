<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\Acl;

use BeastMakers\SecurityApi\Acl\Action\AuthenticationService;
use BeastMakers\SecurityApi\Acl\Action\CreateApiUserService;
use BeastMakers\SecurityApi\Acl\Domain\Validator\ApiUserValidator;
use BeastMakers\SecurityApi\Acl\Domain\Validator\CreateApiUserValidator;
use BeastMakers\SecurityApi\Acl\Infra\Repository\AclReadRepository;
use BeastMakers\SecurityApi\Acl\Infra\Repository\AclWriteRepository;
use BeastMakers\SecurityApi\Acl\Infra\Repository\ReadRepository;
use BeastMakers\SecurityApi\Acl\Infra\Repository\WriteRepository;
use BeastMakers\SecurityApi\Acl\Infra\Request\CreateApiUserRequest;
use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return AuthenticationService
   */
  public function newAuthenticationService(): AuthenticationService
  {
    return new AuthenticationService(
      $this->shareAclReadRepository()
    );
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return ApiUser
   * @throws ArrayValidationException
   */
  public function createApiUserFromCreateApiUserRequest(ServerRequestInterface $request): ApiUser
  {
    $createApiUserRequest = new CreateApiUserRequest($this->dependencyProvider->shareArrayValidator());

    return $createApiUserRequest->createApiUserFromRequest($request);
  }

  /**
   * @param string $jsonPayload
   *
   * @return ApiUser
   * @throws ArrayValidationException
   */
  public function createApiUserFromJson(string $jsonPayload): ApiUser
  {
    $createApiUserRequest = new CreateApiUserRequest($this->dependencyProvider->shareArrayValidator());

    return $createApiUserRequest->createApiUserFromJson($jsonPayload);
  }

  /**
   * @return CreateApiUserService
   */
  public function newCreateApiUserService(): CreateApiUserService
  {
    return new CreateApiUserService(
      $this->newCreateApiUserValidator(),
      $this->shareAclWriteRepository()
    );
  }

  /**
   * @return ReadRepository
   */
  protected function newAclReadRepository(): ReadRepository
  {
    return new AclReadRepository($this->dependencyProvider->loadApiUserQueryRunner());
  }

  /**
   * @return WriteRepository
   */
  protected function newAclWriteRepository(): WriteRepository
  {
    return new AclWriteRepository($this->dependencyProvider->loadApiUserQueryRunner());
  }

  /**
   * @return CreateApiUserValidator
   */
  protected function newCreateApiUserValidator(): CreateApiUserValidator
  {
    return new CreateApiUserValidator(
      $this->newApiUserValidator(),
      $this->shareAclReadRepository()
    );
  }

  /**
   * @return ApiUserValidator
   */
  protected function newApiUserValidator(): ApiUserValidator
  {
    return new ApiUserValidator($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return ReadRepository
   */
  public function shareAclReadRepository(): ReadRepository
  {
    return $this->shareInstance(AclReadRepository::class, static function (Factory $factory) {
      return $factory->newAclReadRepository();
    });
  }

  /**
   * @return WriteRepository
   */
  protected function shareAclWriteRepository(): WriteRepository
  {
    return $this->shareInstance(AclWriteRepository::class, static function (Factory $factory) {
      return $factory->newAclWriteRepository();
    });
  }
}
