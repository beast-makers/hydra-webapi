<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth;

use BeastMakers\SecurityApi\ApiAuth\Action\AuthenticateApiUserService;
use BeastMakers\SecurityApi\ApiAuth\Infra\ApiAuth;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\ApiAuthReadRepository;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\ApiAuthWriteRepository;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\ReadRepository;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\WriteRepository;
use BeastMakers\SecurityApi\ApiAuth\Infra\Request\ApiAuthRequest;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property DependencyProvider $dependencyProvider
 * @property Config $config
 */
class Factory extends BaseFactory
{
  /**
   * @return AuthenticateApiUserService
   */
  public function newAuthenticateApiUserService(): AuthenticateApiUserService
  {
    return new AuthenticateApiUserService(
      $this->dependencyProvider->loadAclFacade(),
      $this->shareApiAuthReadRepository(),
      $this->shareApiAuthWriteRepository()
    );
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return ApiAuth
   * @throws ArrayValidationException
   */
  public function createApiAuthFromApiAuthRequest(ServerRequestInterface $request): ApiAuth
  {
    $apiAuthRequest = new ApiAuthRequest($this->dependencyProvider->shareArrayValidator());

    return $apiAuthRequest->createApiAuthFromRequest($request);
  }

  /**
   * @return ReadRepository
   */
  protected function newApiAuthReadRepository(): ReadRepository
  {
    return new ApiAuthReadRepository($this->dependencyProvider->loadRedisWebApiClient());
  }

  /**
   * @return WriteRepository
   */
  protected function newApiAuthWriteRepository(): WriteRepository
  {
    return new ApiAuthWriteRepository($this->dependencyProvider->loadRedisWebApiClient());
  }

  /**
   * @return ReadRepository
   */
  protected function shareApiAuthReadRepository(): ReadRepository
  {
    return $this->shareInstance(ApiAuthReadRepository::class, static function (Factory $factory) {
      return $factory->newApiAuthReadRepository();
    });
  }

  /**
   * @return WriteRepository
   */
  protected function shareApiAuthWriteRepository(): WriteRepository
  {
    return $this->shareInstance(ApiAuthWriteRepository::class, static function (Factory $factory) {
      return $factory->newApiAuthWriteRepository();
    });
  }
}
