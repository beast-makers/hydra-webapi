<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Repository;

use BeastMakers\Shared\RedisConnector\RedisClient;

class ApiAuthReadRepository implements ReadRepository
{
  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   */
  public function fetchConnectionData(string $connectionId): array
  {
    $data = $this->redisClient->redis()->get(Key::API_USER_CONNECTION_PREFIX . $connectionId);

    if (empty($data)) {
      return [];
    }

    return json_decode($data, true);
  }
}
