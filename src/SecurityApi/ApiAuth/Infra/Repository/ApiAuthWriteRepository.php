<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Repository;

use BeastMakers\Shared\RedisConnector\RedisClient;

class ApiAuthWriteRepository implements WriteRepository
{
  private const EXPIRY_1_DAY = 3600 * 24;

  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   */
  public function pushConnectionData(string $connectionId, string $data): void
  {
    $this->redisClient->useWriteConnection();

    $this->redisClient->redis()->setex(
      Key::API_USER_CONNECTION_PREFIX . $connectionId,
      self::EXPIRY_1_DAY,
      $data
    );
  }
}
