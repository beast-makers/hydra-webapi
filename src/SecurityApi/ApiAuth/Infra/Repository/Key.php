<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Repository;

interface Key
{
  public const API_USER_CONNECTION_PREFIX = 'apiuser_connection_';

  public const CONNECTION_KEY_API_USERNAME = 'api_username';
  public const CONNECTION_KEY_EXPIRY_TIMESTAMP = 'exp';
}
