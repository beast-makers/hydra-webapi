<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Repository;

use BeastMakers\Shared\RedisConnector\RedisException;

interface ReadRepository
{
  /**
   * @param string $connectionId
   *
   * @return array
   * @throws RedisException
   */
  public function fetchConnectionData(string $connectionId): array;
}
