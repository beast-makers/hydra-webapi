<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Repository;

use BeastMakers\Shared\RedisConnector\RedisException;

interface WriteRepository
{
  /**
   * @param string $connectionId
   * @param string $data
   * @return void
   * @throws RedisException
   */
  public function pushConnectionData(string $connectionId, string $data): void;
}
