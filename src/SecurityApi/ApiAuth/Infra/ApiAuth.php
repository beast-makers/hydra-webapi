<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ApiAuth extends ArrayDto
{
  public const ATTR_USERNAME = 'username';
  public const ATTR_PASSWORD = 'password';

  /**
   * @return string
   */
  public function getUsername(): string
  {
    return $this->get(self::ATTR_USERNAME, '');
  }

  /**
   * @param string $username
   *
   * @return ApiAuth
   */
  public function setUsername(string $username): self
  {
    $this->set(self::ATTR_USERNAME, $username);

    return $this;
  }

  /**
   * @return string
   */
  public function getPassword(): string
  {
    return $this->get(self::ATTR_PASSWORD, '');
  }

  /**
   * @param string $password
   *
   * @return ApiAuth
   */
  public function setPassword(string $password): self
  {
    $this->set(self::ATTR_PASSWORD, $password);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_USERNAME => self::DEF_SCALAR,
      self::ATTR_PASSWORD => self::DEF_SCALAR,
    ];
  }
}
