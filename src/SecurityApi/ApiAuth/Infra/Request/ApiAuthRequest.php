<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Request;

use BeastMakers\SecurityApi\ApiAuth\Infra\ApiAuth;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class ApiAuthRequest
{
  public const PARAM_USERNAME = 'username';
  public const PARAM_PASSWORD = 'password';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return ApiAuth
   * @throws ArrayValidationException
   */
  public function createApiAuthFromRequest(ServerRequestInterface $request): ApiAuth
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $request->getBody()->rewind();

    return $this->createApiAuthFromArray($params);
  }

  /**
   * @param array $input
   *
   * @return ApiAuth
   * @throws ArrayValidationException
   */
  private function createApiAuthFromArray(array $input): ApiAuth
  {
    $this->validator->validate($input, static::defineValidationRules());
    $map = self::defineRequestParamsToApiUserMap();

    return ApiAuth::mapArrayToNewDto($input, $map);
  }

  /**
   * @return array
   */
  private static function defineValidationRules(): array
  {
    return [
      self::PARAM_USERNAME => 'required',
      self::PARAM_PASSWORD => 'required',
    ];
  }

  /**
   * @return array
   */
  private static function defineRequestParamsToApiUserMap(): array
  {
    return [
      self::PARAM_USERNAME => ApiAuth::ATTR_USERNAME,
      self::PARAM_PASSWORD => ApiAuth::ATTR_PASSWORD,
    ];
  }
}
