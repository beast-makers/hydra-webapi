<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Infra\Middleware;

use BeastMakers\SecurityApi\Acl\InnerFacade as AclFacade;
use BeastMakers\SecurityApi\ApiAuth\InnerFacade as ApiAuthFacade;
use BeastMakers\Shared\DependencyLoader\InnerFacadeLoader;
use BeastMakers\Shared\RedisConnector\RedisException;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteInterface;
use Slim\Routing\RouteContext;

class ApiAccessGuardMiddleware
{
  use InnerFacadeLoader;

  public const ROLE_ADMIN = 'admin';
  public const ROLE_READ_ONLY = 'read-only';

  private ApiAuthFacade $apiAuthFacade;
  private AclFacade $aclFacade;

  public function __construct()
  {
    $this->apiAuthFacade = $this->shareFacade(ApiAuthFacade::class);
    $this->aclFacade = $this->shareFacade(AclFacade::class);
  }

  /**
   * @param ServerRequestInterface $request
   * @param RequestHandlerInterface $handler
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
  {
    if (!$this->isAuthenticationRequired($request)) {
      return $handler->handle($request);
    }

    $connectionId = $this->readConnectionId($request);

    try {
      $connectionData = $this->apiAuthFacade->authConnection($connectionId);
      $apiUser = $this->aclFacade->readApiUser($connectionData->getUsername());
    } catch (RedisException $e) {
      return new Response(500, [], 'Data storage error. Try again later or re-login.');
    }

    if (!$this->isAccessAllowed($request, $apiUser->getUsername())) {
      return new Response(403);
    }

    return $handler->handle($request);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return bool
   */
  private function isAuthenticationRequired(ServerRequestInterface $request): bool
  {
    /** @var RouteInterface $route */
    $route = RouteContext::fromRequest($request)->getRoute();
    $controllerClass = $route->getCallable();

    /** @psalm-suppress PossiblyInvalidOperand */
    if (!defined($controllerClass . '::IS_API_AUTHENTICATION_REQUIRED')) {
      return true;
    }

    return (bool) $controllerClass::IS_API_AUTHENTICATION_REQUIRED;
  }

  /**
   * @param ServerRequestInterface $request
   * @param string $username
   * @return bool
   */
  private function isAccessAllowed(ServerRequestInterface $request, string $username): bool
  {
    $apiUser = $this->aclFacade->readApiUser($username);
    $userRoles = $apiUser->getRoles();

    $allowedRoles = $this->readAllowedApiRoles($request);
    $matchedRoles = array_intersect($userRoles, $allowedRoles);

    return !empty($matchedRoles);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return array
   */
  private function readAllowedApiRoles(ServerRequestInterface $request): array
  {
    /** @var RouteInterface $route */
    $route = RouteContext::fromRequest($request)->getRoute();
    $actionClass = $route->getCallable();

    /** @psalm-suppress PossiblyInvalidOperand */
    if (!defined($actionClass . '::ALLOWED_API_ROLES')) {
      return [self::ROLE_ADMIN];
    }

    $allowedRoles = (array) $actionClass::ALLOWED_API_ROLES;
    return array_merge($allowedRoles, [self::ROLE_ADMIN]);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return string
   */
  private function readConnectionId(ServerRequestInterface $request): string
  {
    return $this->readTokenStringFromAuthorizationHeader($request);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return string
   */
  private function readTokenStringFromAuthorizationHeader(ServerRequestInterface $request): string
  {
    $authorizationHeader = current($request->getHeader('Authorization'));
    if ($authorizationHeader) {
      $authorizationParts = explode(' ', $authorizationHeader);
      return end($authorizationParts);
    }

    return '';
  }
}
