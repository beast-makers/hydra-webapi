<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Action\Http;

use BeastMakers\SecurityApi\ApiAuth\Config;
use BeastMakers\SecurityApi\ApiAuth\Factory;
use BeastMakers\SecurityApi\Shared\ErrorCode;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\RedisConnector\RedisException;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 * @property Config $config
 */
class ApiAuth extends BaseHttpAction
{
  public const IS_API_AUTHENTICATION_REQUIRED = false;

  /**
   * @param ServerRequestInterface $request
   * @return ResponseInterface
   * @throws RedisException
   * @throws MissingConfigOptionException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    try {
      $apiAuthDto = $this->factory->createApiAuthFromApiAuthRequest($request);
      $authenticateApiUserService = $this->factory->newAuthenticateApiUserService();
      $authenticationResult = $authenticateApiUserService->authenticateApiUser($apiAuthDto);
    } catch (ArrayValidationException $e) {
      return $this->buildInvalidInputResponse();
    }

    if (!$authenticationResult->getIsSuccess()) {
      return $this->buildInvalidCredentialsResponse();
    }

    $apiUser = $authenticationResult->getApiUser();
    $payload = $authenticateApiUserService->handleSuccessfulAuthentication($apiUser, $this->config->readExpiryTimestamp());

    return $this->buildResponse($payload);
  }

  /**
   * @return ResponseInterface
   */
  private function buildInvalidInputResponse(): ResponseInterface
  {
    $result = new Result();
    $result->addError((new Error(ErrorCode::INVALID_INPUT_DATA)));

    return $this->actionResponseBuilder->buildJsonHttpErrorResponse(
      $result,
      $this->getHttpCodeErrorMap()
    );
  }

  /**
   * @return ResponseInterface
   */
  private function buildInvalidCredentialsResponse(): ResponseInterface
  {
    $result = new Result();
    $result->addError((new Error(ErrorCode::INVALID_CREDENTIALS)));

    return $this->actionResponseBuilder->buildJsonHttpErrorResponse(
      $result,
      $this->getHttpCodeErrorMap()
    );
  }

  /**
   * @param array $payload
   * @return ResponseInterface
   */
  private function buildResponse(array $payload): ResponseInterface
  {
    $headers = [
      'Content-type' => 'application/json',
    ];

    return new Response(200, $headers, json_encode($payload));
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::INVALID_JSON_FORMAT => 400,
      ErrorCode::INVALID_INPUT_DATA => 400,
      ErrorCode::INVALID_CREDENTIALS => 401,
    ];
  }
}
