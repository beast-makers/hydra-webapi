<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth\Action;

use BeastMakers\SecurityApi\Acl\InnerFacade as AclFacade;
use BeastMakers\SecurityApi\ApiAuth\Infra\ApiAuth;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\Key;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\ReadRepository;
use BeastMakers\SecurityApi\ApiAuth\Infra\Repository\WriteRepository;
use BeastMakers\SecurityApi\Shared\AuthenticationResult;
use BeastMakers\SecurityApi\Shared\ConnectionData;
use BeastMakers\SecurityApi\Shared\Domain\ApiUser;
use BeastMakers\Shared\RedisConnector\RedisException;

class AuthenticateApiUserService
{
  private const KEY_CONNECTION_ID = 'con_id';
  private const KEY_EXPIRY_TIMESTAMP = 'exp';

  private AclFacade $aclFacade;

  private ReadRepository $readRepository;

  private WriteRepository $writeRepository;

  /**
   * @param AclFacade $apiUserFacade
   * @param ReadRepository $readRepository
   * @param WriteRepository $writeRepository
   */
  public function __construct(
    AclFacade $apiUserFacade,
    ReadRepository $readRepository,
    WriteRepository $writeRepository
  ) {
    $this->aclFacade = $apiUserFacade;
    $this->readRepository = $readRepository;
    $this->writeRepository = $writeRepository;
  }

  /**
   * @param ApiAuth $apiAuthDto
   * @return AuthenticationResult
   */
  public function authenticateApiUser(ApiAuth $apiAuthDto): AuthenticationResult
  {
    return $this->aclFacade->authenticateApiUser($apiAuthDto->getUsername(), $apiAuthDto->getPassword());
  }

  /**
   * @param ApiUser $apiUser
   * @param int $expiryTimestamp
   * @return array
   * @throws RedisException
   */
  public function handleSuccessfulAuthentication(ApiUser $apiUser, int $expiryTimestamp): array
  {
    $connectionId = $this->generateConnectionId();
    $this->storeConnectionData($connectionId, $expiryTimestamp, $apiUser->getUsername());

    return [
      self::KEY_EXPIRY_TIMESTAMP => $expiryTimestamp,
      self::KEY_CONNECTION_ID => $connectionId,
    ];
  }

  /**
   * @param string $connectionId
   * @param int $expiryTimestamp
   * @param string $apiUsername
   * @return void
   * @throws RedisException
   */
  private function storeConnectionData(string $connectionId, int $expiryTimestamp, string $apiUsername): void
  {
    $connectionData = json_encode([
      Key::CONNECTION_KEY_API_USERNAME => $apiUsername,
      Key::CONNECTION_KEY_EXPIRY_TIMESTAMP => $expiryTimestamp,
    ]);

    $this->writeRepository->pushConnectionData($connectionId, $connectionData);
  }

  /**
   * @param string $connectionId
   * @return ConnectionData
   * @throws RedisException
   */
  public function authConnection(string $connectionId): ConnectionData
  {
    $connectionData = $this->readRepository->fetchConnectionData($connectionId);
    return ConnectionData::mapArrayToNewDto($connectionData, [
      Key::CONNECTION_KEY_API_USERNAME => ConnectionData::ATTR_USERNAME,
      Key::CONNECTION_KEY_EXPIRY_TIMESTAMP => ConnectionData::ATTR_EXPIRY,
    ]);
  }

  /**
   * @return string
   */
  private function generateConnectionId(): string
  {
    return uniqid('', true);
  }
}
