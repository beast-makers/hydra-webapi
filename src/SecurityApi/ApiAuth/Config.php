<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth;

use BeastMakers\Shared\Config\Config as AuthConfig;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\BaseConfig;

class Config extends BaseConfig
{
  /**
   * @return int
   * @throws MissingConfigOptionException
   */
  public function readExpiryTimestamp(): int
  {
    $expiry = $this->readAuthConfig()->getString('expiry');

    return strtotime($expiry);
  }

  /**
   * @return AuthConfig
   * @throws MissingConfigOptionException
   */
  private function readAuthConfig(): AuthConfig
  {
    return $this->getConfig('api_auth');
  }
}
