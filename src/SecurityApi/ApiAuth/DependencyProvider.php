<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth;

use BeastMakers\SecurityApi\Acl\InnerFacade as AclFacade;
use BeastMakers\Shared\DependencyLoader;
use BeastMakers\Shared\Kernel\BaseDependencyProvider;
use BeastMakers\Shared\RedisConnector\RedisClient;

class DependencyProvider extends BaseDependencyProvider
{
  use DependencyLoader\InnerFacadeLoader;
  use DependencyLoader\RedisWebapiClient;
  use DependencyLoader\OwnArrayValidator;

  /**
   * @return AclFacade
   */
  public function loadAclFacade(): AclFacade
  {
    return $this->shareFacade(AclFacade::class);
  }

  /**
   * @return RedisClient
   */
  public function loadRedisWebApiClient(): RedisClient
  {
    return $this->shareRedisWebapiClient();
  }
}
