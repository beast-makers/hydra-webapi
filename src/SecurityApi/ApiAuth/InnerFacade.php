<?php
declare(strict_types=1);

namespace BeastMakers\SecurityApi\ApiAuth;

use BeastMakers\SecurityApi\Shared\ConnectionData;
use BeastMakers\Shared\Kernel\BaseInnerFacade;
use BeastMakers\Shared\RedisConnector\RedisException;

/**
 * @property Factory $factory
 */
class InnerFacade extends BaseInnerFacade
{
  /**
   * @param string $connectionId
   *
   * @return ConnectionData
   * @throws RedisException
   */
  public function authConnection(string $connectionId): ConnectionData
  {
    return $this->factory->newAuthenticateApiUserService()->authConnection($connectionId);
  }
}
