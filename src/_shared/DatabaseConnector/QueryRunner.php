<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

interface QueryRunner
{
  /**
   * @param string $query
   * @param string $queryName
   * @param array $params
   *
   * @return resource|false
   * @throws DatabaseException
   * @psalm-ignore-falsable-return
   */
  public function runQuery(string $query, string $queryName, array $params = []);

  /**
   * @param string $query
   * @param array $params
   *
   * @return string
   */
  public function buildQuery(string $query, array $params = []): string;

  /**
   * @return ConnectionInterface
   */
  public function connection(): ConnectionInterface;

  /**
   * @return void
   */
  public function useWriteConnection(): void;
}
