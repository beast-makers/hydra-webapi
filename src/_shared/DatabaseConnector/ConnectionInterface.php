<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

interface ConnectionInterface
{
  /**
   * @return resource
   */
  public function connectOnce();

  /**
   * @return resource
   */
  public function dbConnection();

  /**
   * @return void
   */
  public function startTransaction(): void;

  /**
   * @return void
   */
  public function commitTransaction(): void;

  /**
   * @return void
   */
  public function rollbackTransaction(): void;

  /**
   * @param callable $action
   * @throws \Throwable
   */
  public function runInTransaction(callable $action): void;
}
