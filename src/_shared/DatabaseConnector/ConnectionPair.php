<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

class ConnectionPair
{
  /**
   * @var ConnectionInterface
   */
  private $readConnection;

  /**
   * @var ConnectionInterface
   */
  private $writeConnection;

  /**
   * @param ConnectionInterface $readConnection
   * @param ConnectionInterface $writeConnection
   */
  public function __construct(ConnectionInterface $readConnection, ConnectionInterface $writeConnection)
  {
    $this->readConnection = $readConnection;
    $this->writeConnection = $writeConnection;
  }

  /**
   * @return ConnectionInterface
   */
  public function getWriteConnection(): ConnectionInterface
  {
    return $this->writeConnection;
  }

  /**
   * @return ConnectionInterface
   */
  public function getReadConnection(): ConnectionInterface
  {
    return $this->readConnection;
  }
}
