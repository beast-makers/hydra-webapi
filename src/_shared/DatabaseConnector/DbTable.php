<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

abstract class DbTable
{
  protected const TYPES = [];

  /**
   * @param $value
   * @param string $columnName
   *
   * @return string
   */
  abstract public static function escapeLiteral($value, string $columnName): string;

  /**
   * @param string $identifier
   *
   * @return string
   */
  abstract public static function escapeIdentifier(string $identifier): string;


  /**
   * @param string $columnName
   *
   * @return string
   */
  public static function typeFor(string $columnName): string
  {
    return static::TYPES[$columnName];
  }

  /**
   * @param array $columnValueMap
   *
   * @return ListPair
   */
  public static function prepareInsertLists(array $columnValueMap): ListPair
  {
    $escapedColumnsCsv = '';
    $escapedValuesCsv = '';

    foreach ($columnValueMap as $columnName => $value) {
      $escapedColumnsCsv .= ',' . static::escapeIdentifier($columnName);
      $escapedValuesCsv .= ',' . static::escapeLiteral($value, $columnName);
    }

    $escapedColumnsCsv = ltrim($escapedColumnsCsv, ',');
    $escapedValuesCsv = ltrim($escapedValuesCsv, ',');

    return new ListPair($escapedColumnsCsv, $escapedValuesCsv);
  }

  /**
   * @param array $columnsValuesMap
   *
   * @return string
   */
  public static function prepareUpdateStatementList(array $columnsValuesMap): string
  {
    $escapedList = '';
    foreach ($columnsValuesMap as $columnName => $value) {
      $escapedList .= ',' . static::escapeIdentifier($columnName) . '=' . static::escapeLiteral($value, $columnName);
    }

    return ltrim($escapedList, ', ');
  }

  /**
   * @param int $strlen
   *
   * @return string
   * @noinspection PhpDocMissingThrowsInspection
   */
  public static function uniqid(int $strlen = 12): string
  {
    return substr(bin2hex(random_bytes($strlen)), 0, $strlen);
  }
}
