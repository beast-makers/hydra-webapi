<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

abstract class BaseConnection implements ConnectionInterface
{
  /**
   * @var resource|null
   */
  protected $dbConnection = null;

  /**
   * @var string
   */
  protected $host;

  /**
   * @var int
   */
  protected $port;

  /**
   * @var string
   */
  protected $dbname;

  /**
   * @var string
   */
  protected $user;

  /**
   * @var string
   */
  protected $password;

  /**
   * @var array
   */
  protected $extraOptions = [];

  public function __construct(
    string $host,
    int $port,
    string $dbname,
    string $user,
    string $password,
    array $extraOptions = []
  ) {
    $this->host = $host;
    $this->port = $port;
    $this->dbname = $dbname;
    $this->user = $user;
    $this->password = $password;
    $this->extraOptions = $extraOptions;
  }

  /**
   * @return resource|null
   * @psalm-suppress ImplementedReturnTypeMismatch
   */
  public function dbConnection()
  {
    return $this->dbConnection;
  }
}
