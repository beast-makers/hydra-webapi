<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector\PostgreSQL;

use BeastMakers\Shared\DatabaseConnector\DbTable as BaseDbTable;

class DbTable extends BaseDbTable
{
  public const TYPE_TEXT = 'text';
  public const TYPE_BOOL = 'bool';
  public const TYPE_NUMERIC = 'numeric';
  public const TYPE_JSONB = 'jsonb';
  public const TYPE_TEXT_ARRAY = 'text_array';

  /**
   * @param $value
   * @param string $columnName
   *
   * @return string
   */
  public static function escapeLiteral($value, string $columnName): string
  {
    $type = static::typeFor($columnName);

    switch ($type) {
      case self::TYPE_BOOL: return self::toSqlBoolean($value);
      case self::TYPE_NUMERIC: return self::toSqlNumeric($value);
      case self::TYPE_TEXT_ARRAY: return self::toSqlTextArray($value);
      // case self::TYPE_JSONB: is treated as string

      default: return pg_escape_literal($value);
    }
  }

  /**
   * @inheritDoc
   */
  public static function escapeIdentifier(string $identifier): string
  {
    return pg_escape_identifier($identifier);
  }

  /**
   * @param string $dbString
   * @return array
   */
  public static function dbArrayToPhpArray(string $dbString): array
  {
    $csvList = trim($dbString, '{}');

    return explode(',', $csvList);
  }

  /**
   * @param $value
   *
   * @return string
   */
  private static function toSqlBoolean($value): string
  {
    $result = filter_var($value, FILTER_VALIDATE_BOOLEAN);

    return $result ? 'true' : 'false';
  }

  /**
   * @param $value
   *
   * @return string
   */
  private static function toSqlNumeric($value): string
  {
    return is_numeric($value) ? (string) $value : pg_escape_literal($value);
  }

  /**
   * @param array $list
   *
   * @return string
   */
  private static function toSqlTextArray(array $list): string
  {
    $values = [];
    foreach ($list as $value) {
      $values[] = pg_escape_literal((string) $value);
    }

    return 'ARRAY[' . implode(', ', $values) . ']';
  }
}
