<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector\PostgreSQL;

use BeastMakers\Shared\DatabaseConnector\ConnectionInterface;
use BeastMakers\Shared\DatabaseConnector\ConnectionPair;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;
use BeastMakers\Shared\DatabaseConnector\QueryRunner as QueryRunnerInterface;

class QueryRunner implements QueryRunnerInterface
{
  private ConnectionPair $connectionPair;

  private ConnectionInterface $currentConnection;

  /**
   * @param ConnectionPair $connectionPair
   */
  public function __construct(ConnectionPair $connectionPair)
  {
    $this->connectionPair = $connectionPair;
    $this->currentConnection = $connectionPair->getReadConnection();
  }

  /**
   * @param string $query
   * @param string $queryName
   * @param array $params
   *
   * @return resource|false
   * @throws DatabaseException
   */
  public function runQuery(string $query, string $queryName, array $params = [])
  {
    $this->connection()->connectOnce();

    $_query = $this->buildQuery($query, $params);
    $queryWithComment = "-- QueryName: {$queryName}\n{$_query}";

    try {
      $queryResult = pg_query($this->currentConnection->dbConnection(), $queryWithComment);
      if ($queryResult === false) {
        throw new DatabaseException('Invalid database query result');
      }
    } catch (\Throwable $e) {
      $lastError = pg_last_error($this->currentConnection->dbConnection());
      throw new DatabaseException('Database exception: ' . $lastError, 0, $e);
    }

    return $queryResult;
  }

  /**
   * @param string $query
   * @param array $params
   *
   * @return string
   */
  public function buildQuery(string $query, array $params = []): string
  {
    $search = [];
    $replace = [];
    foreach ($params as $key => $value) {
      $search[] = ':' . $key;
      $replace[] = $value;
    }

    return str_replace($search, $replace, $query);
  }

  /**
   * @return ConnectionInterface
   */
  public function connection(): ConnectionInterface
  {
    return $this->currentConnection;
  }

  /**
   * @return void
   */
  public function useWriteConnection(): void
  {
    $this->currentConnection = $this->connectionPair->getWriteConnection();
    $this->connection()->connectOnce();
  }
}
