<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector\PostgreSQL;

use BeastMakers\Shared\DatabaseConnector\BaseConnection;
use BeastMakers\Shared\DatabaseConnector\DatabaseException;

class Connection extends BaseConnection
{
  protected int $transactionCounter = 0;

  protected bool $abortTransaction = false;

  /**
   * @return resource
   * @throws \Exception
   */
  public function connectOnce()
  {
    if ($this->dbConnection === null) {
      $this->connect();
    }

    return $this->dbConnection;
  }

  /**
   * @return void
   * @throws \Exception
   */
  public function startTransaction(): void
  {
    $this->connectOnce();
    if ($this->transactionCounter === 0) {
      /** @psalm-suppress PossiblyNullArgument */
      pg_query($this->dbConnection, 'START TRANSACTION');
    }

    ++$this->transactionCounter;
  }

  /**
   * @return void
   */
  public function commitTransaction(): void
  {
    if ($this->transactionCounter === 1 && $this->abortTransaction) {
      $this->rollbackTransaction();
    }

    if ($this->transactionCounter === 1 && !$this->abortTransaction) {
      /** @psalm-suppress PossiblyNullArgument */
      pg_query($this->dbConnection, 'COMMIT');
      --$this->transactionCounter;
    }

    if ($this->transactionCounter > 1) {
      --$this->transactionCounter;
    }
  }

  /**
   * @return void
   */
  public function rollbackTransaction(): void
  {
    $this->abortTransaction = true;

    if ($this->transactionCounter === 1) {
      /** @psalm-suppress PossiblyNullArgument */
      pg_query($this->dbConnection, 'ROLLBACK');
      $this->abortTransaction = false;
    }

    --$this->transactionCounter;
  }

  /**
   * @param callable $action
   * @throws \Exception
   * @throws DatabaseException
   */
  public function runInTransaction(callable $action): void
  {
    $this->startTransaction();
    try {
      $action();
      $this->commitTransaction();
    } catch (\Throwable $e) {
      $this->rollbackTransaction();
      throw new DatabaseException($e->getMessage(), 0, $e);
    }
  }

  /**
   * @throws \Exception
   */
  private function connect(): void
  {
    $this->dbConnection = pg_connect("
        host={$this->host}
        port={$this->port}
        dbname={$this->dbname}
        user={$this->user}
        password={$this->password}
        connect_timeout=1
      ");

    if (!$this->dbConnection) {
      throw new DatabaseException('Could not connect: ' . pg_last_error());
    }
  }
}
