<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

class ListPair
{
  private string $identifiers = '';
  private string $values = '';

  /**
   * @param string $identifiers
   * @param string $values
   */
  public function __construct(string $identifiers, string $values)
  {
    $this->identifiers = $identifiers;
    $this->values = $values;
  }

  /**
   * @return string
   */
  public function getIdentifiers(): string
  {
    return $this->identifiers;
  }

  /**
   * @return string
   */
  public function getValues(): string
  {
    return $this->values;
  }

  /**
   * @return bool
   */
  public function isEmpty(): bool
  {
    return empty($this->identifiers);
  }
}
