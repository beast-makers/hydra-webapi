<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

class QueryRunnerPool
{
  /**
   * @var QueryRunner[]
   */
  protected $pool = [];

  /**
   * @param QueryRunner $queryRunner
   * @param string $key
   *
   * @return void
   */
  public function register(QueryRunner $queryRunner, string $key): void
  {
    $this->pool[$key] = $queryRunner;
  }

  /**
   * @param string $key
   *
   * @return QueryRunner
   */
  public function get(string $key): QueryRunner
  {
    return $this->pool[$key];
  }
}
