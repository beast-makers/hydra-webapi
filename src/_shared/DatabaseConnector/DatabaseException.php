<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DatabaseConnector;

class DatabaseException extends \Exception
{
}
