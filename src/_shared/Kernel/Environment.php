<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class Environment
{
  /**
   * @var string
   */
  private string $apiUsername = '';

  /**
   * @var string[]
   */
  private array $apiUserRoles = [];

  /**
   * @return string
   */
  public function getApiUsername(): string
  {
    return $this->apiUsername;
  }

  /**
   * @param string $apiUsername
   */
  public function setApiUsername(string $apiUsername): void
  {
    $this->apiUsername = $apiUsername;
  }

  /**
   * @return string[]
   */
  public function getApiUserRoles(): array
  {
    return $this->apiUserRoles;
  }

  /**
   * @param string[] $apiUserRoles
   */
  public function setApiUserRoles(array $apiUserRoles): void
  {
    $this->apiUserRoles = $apiUserRoles;
  }

  /**
   * @param string $role
   * @return bool
   */
  public function hasApiUserRole(string $role): bool
  {
    return !empty($this->apiUserRoles[$role]);
  }
}
