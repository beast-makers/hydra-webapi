<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class BaseFactory
{
  /**
   * @var object[]
   */
  protected static array $instanceCache = [];

  protected static array $constructorRegistry = [];

  protected BaseDependencyProvider $dependencyProvider;

  protected BaseConfig $config;

  /**
   * @param BaseDependencyProvider $dependencyProvider
   * @param BaseConfig $config
   */
  public function __construct(BaseDependencyProvider $dependencyProvider, BaseConfig $config)
  {
    $this->dependencyProvider = $dependencyProvider;
    $this->config = $config;
  }

  /**
   * @return BaseConfig
   */
  public function getConfig(): BaseConfig
  {
    return $this->config;
  }

  /**
   * @param string $key
   * @param $constructorClosure
   *
   * @return mixed
   */
  public function shareInstance(string $key, $constructorClosure)
  {
    if (!isset(self::$instanceCache[$key])) {
      self::$instanceCache[$key] = ($constructorClosure)($this);
    }

    return self::$instanceCache[$key];
  }
}
