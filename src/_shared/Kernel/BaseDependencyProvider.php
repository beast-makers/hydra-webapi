<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class BaseDependencyProvider
{
  protected DependencyContainer $di;

  public function __construct()
  {
    $this->di = DependencyContainer::getOwnInstance();
    $this->selfRegister();
  }

  /**
   * @return void
   */
  private function selfRegister(): void
  {
    $this->di->setInstance(static::class, $this);
  }
}
