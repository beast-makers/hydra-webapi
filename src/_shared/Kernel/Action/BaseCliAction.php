<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Action;

class BaseCliAction extends BaseAction
{
  /**
   * @return string
   */
  protected function readStdIn(): string
  {
    return file_get_contents('php://stdin');
  }
}
