<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Action;

use BeastMakers\Shared\Result\Result;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class ActionResponseBuilder
{
  /**
   * @param Result $actionResult
   * @param array $httpCodeErrorMap
   *
   * @return ResponseInterface
   */
  public function buildJsonHttpErrorResponse(Result $actionResult, array $httpCodeErrorMap): ResponseInterface
  {
    $errorCollection = $actionResult->getErrorCollection();
    $httpCode = $this->discoverHttpCode($actionResult, $httpCodeErrorMap);

    return $this->createJsonHttpResponse($httpCode, $errorCollection->getErrors());
  }

  /**
   * @param int $httpResponseCode
   * @param array $content
   *
   * @return ResponseInterface
   */
  public function createJsonHttpResponse(int $httpResponseCode, array $content): ResponseInterface
  {
    $headers = [
      'Content-type' => 'application/json',
    ];

    return new Response($httpResponseCode, $headers, json_encode($content));
  }

  /**
   * @param Result $actionResult
   * @param array $httpCodeErrorMap
   *
   * @return int
   */
  private function discoverHttpCode(Result $actionResult, array $httpCodeErrorMap): int
  {
    $errorCollection = $actionResult->getErrorCollection();
    $errorCode = $errorCollection->getFirstError()->errorCode;
    $httpCode = 500;

    if (array_key_exists($errorCode, $httpCodeErrorMap)) {
      $httpCode = $httpCodeErrorMap[$errorCode];
    }

    return $httpCode;
  }
}
