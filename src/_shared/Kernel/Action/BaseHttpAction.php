<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Action;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use Slim\Interfaces\RouteParserInterface;

class BaseHttpAction extends BaseAction
{
  protected ActionResponseBuilder $actionResponseBuilder;

  protected RouteParserInterface $routeParser;

  public function __construct()
  {
    parent::__construct();

    $diContainer = DependencyContainer::getOwnInstance();

    $this->actionResponseBuilder = $diContainer->shareInstance(DependencyContainerItem::ACTION_RESPONSE_BUILDER);
    $this->routeParser = $diContainer->shareInstance(DependencyContainerItem::ACTION_ROUTE_PARSER);
  }
}
