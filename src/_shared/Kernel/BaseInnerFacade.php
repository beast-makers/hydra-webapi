<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class BaseInnerFacade
{
  protected BaseFactory $factory;

  public function __construct()
  {
    $this->setFactory($this->getModuleFactory());
  }

  /**
   * @param BaseFactory $factory
   *
   * @return void
   */
  public function setFactory(BaseFactory $factory): void
  {
    $this->factory = $factory;
  }

  /**
   * @return BaseFactory
   */
  protected function getModuleFactory(): BaseFactory
  {
    return FactoryRegistry::getFactoryInstance(static::class);
  }
}
