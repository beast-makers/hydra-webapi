<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Middleware;

use BeastMakers\Shared\Result\CommonErrorCodes;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadEnvironmentMiddleware
{
  /**
   * @param ServerRequestInterface $request
   * @param RequestHandlerInterface $handler
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
  {
    $requestBody = $request->getBody();
    $contents = $requestBody->getContents();
    $requestBody->rewind();
    if (json_decode($contents) === null) {
      return $this->buildInvalidJsonFormatResponse();
    }

    return $handler->handle($request);
  }

  /**
   * @return ResponseInterface
   */
  private function buildInvalidJsonFormatResponse(): ResponseInterface
  {
    $result = new Result();
    $result->addError(new Error(CommonErrorCodes::INVALID_JSON_FORMAT));

    $headers = [
      'Content-type' => 'application/json',
    ];

    return new Response(400, $headers, json_encode($result->getErrorCollection()->getErrors()));
  }
}
