<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class DependencyContainer
{
  private static ?DependencyContainer $instance = null;

  /**
   * @var callable[]
   */
  private array $dependencies = [];

  /**
   * @var object[]
   */
  protected static array $instanceCache = [];

  protected static array $constructorRegistry = [];

  private function __construct()
  {
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @return DependencyContainer
   */
  public static function getOwnInstance(): self
  {
    if (static::$instance === null) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  /**
   * @param string $key
   * @param mixed $instance
   */
  public function setInstance(string $key, $instance): void
  {
    self::$instanceCache[$key] = $instance;
  }

  /**
   * @param string $key
   * @param \Closure $constructorClosure
   *
   * @return void
   */
  public function registerConstructor(string $key, \Closure $constructorClosure): void
  {
    self::$constructorRegistry[$key] = $constructorClosure;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function shareInstance(string $key)
  {
    if (!isset(self::$instanceCache[$key])) {
      self::$instanceCache[$key] = (self::$constructorRegistry[$key])();
    }

    return self::$instanceCache[$key];
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  public function contains(string $key): bool
  {
    return array_key_exists($key, $this->dependencies);
  }
}
