<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

use BeastMakers\Application\Bootstrap\ApplicationConfig;

class FactoryRegistry
{
  private static array $registry = [];

  public static function getFactoryInstance(string $className): BaseFactory
  {
    if (empty(self::$registry[$className])) {
      self::$registry[$className] = self::createBeastMakersModuleFactory($className);
    }

    return self::$registry[$className];
  }

  /**
   * @param string $className
   *
   * @return BaseFactory
   */
  public static function createBeastMakersModuleFactory(string $className): BaseFactory
  {
    $vendorNamespace = '\BeastMakers';
    $moduleNamespace = self::extractModuleNamespace($vendorNamespace, $className);

    return self::createModuleFactory($moduleNamespace);
  }

  /**
   * @param string $namespace
   * @param string $className
   *
   * @return string
   */
  private static function extractModuleNamespace(string $namespace, string $className): string
  {
    $cleanedClassName = trim($className, '\\');
    $cleanedNamespace = trim($namespace, '\\');
    $modulePath = str_replace($cleanedNamespace, '', $cleanedClassName);
    $tokens = explode('\\', trim($modulePath, '\\'));

    return "{$namespace}\\{$tokens[0]}\\{$tokens[1]}";
  }

  /**
   * @param string $moduleNamespace
   *
   * @return BaseFactory
   */
  private static function createModuleFactory(string $moduleNamespace): BaseFactory
  {
    $factoryClassName = self::discoverFullClassName('Factory', $moduleNamespace);
    $dependencyContainerClassName = self::discoverFullClassName('DependencyProvider', $moduleNamespace);
    $configClassName = self::discoverFullClassName('Config', $moduleNamespace);

    /** @var BaseDependencyProvider $dependencyProvider */
    $dependencyProvider = new $dependencyContainerClassName();

    /** @var BaseConfig $config */
    $config = new $configClassName();

    /** @var BaseFactory */
    return new $factoryClassName($dependencyProvider, $config);
  }

  /**
   * @param string $className
   * @param string $moduleNamespace
   *
   * @return string
   * @noinspection PhpDocMissingThrowsInspection
   */
  private static function discoverFullClassName(string $className, string $moduleNamespace): string
  {
    /** @noinspection PhpUnhandledExceptionInspection */
    $appStore = ApplicationConfig::readCurrentStore();
    $appStoreSpecificFullClassName = $moduleNamespace . strtoupper($appStore) . "\\{$className}";

    if (class_exists($appStoreSpecificFullClassName)) {
      return $appStoreSpecificFullClassName;
    }

    return "{$moduleNamespace}\\{$className}";
  }
}
