<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine;

class Condition
{
  /**
   * @param StateMachine $stateMachine
   * @param array $arguments
   *
   * @return bool
   */
  public static function execute(StateMachine $stateMachine, array $arguments = []): bool
  {
    return false;
  }
}
