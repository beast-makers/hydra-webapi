<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\Exception\MissingMetadataKeyException;

class StateMachine
{
  private StateMachineReader $smReader;

  private string $instanceId;

  private array $metadata;

  private string $activeStateName;

  private string $statusCode;

  private StateMachineListener $listener;

  public function __construct(
    StateMachineReader $smReader,
    StateMachineListener $listener,
    string $instanceId,
    array $metadata = []
  ) {
    $this->smReader = $smReader;
    $this->instanceId = $instanceId;
    $this->metadata = $metadata;
    $this->listener = $listener;
  }

  /**
   * @return string
   */
  public function getInstanceId(): string
  {
    return $this->instanceId;
  }

  /**
   * @return string
   */
  public function getActiveStateName(): string
  {
    return $this->activeStateName;
  }

  /**
   * @param string $activeStateName
   *
   * @return $this
   */
  public function setActiveStateName(string $activeStateName): self
  {
    $this->activeStateName = $activeStateName;

    return $this;
  }

  /**
   * @param string $statusCode
   *
   * @return $this
   */
  public function setStatusCode(string $statusCode): self
  {
    $this->statusCode = $statusCode;

    return $this;
  }

  /**
   * @return string
   */
  public function getStatusCode(): string
  {
    return $this->statusCode;
  }
  /**
   * @return void
   */
  public function resetStatusCode(): void
  {
    $this->setStatusCode('');
  }

  /**
   * @return string
   */
  public function moveForward(): string
  {
    $this->resetStatusCode();

    while (true) {
      $transition = $this->findNextOpenTransition();
      if (empty($transition)) {
        break;
      }
      if (!$this->tryExecutingAction($transition)) {
        break;
      }

      $activeStateName = $this->getActiveStateName();
      $nextStateName = $this->smReader->getStateNameFor($transition);
      $this->setActiveStateName($nextStateName);
      $this->listener->changedState($this, $activeStateName, $nextStateName);
    }

    return $this->getActiveStateName();
  }

  /**
   * @param string $eventName
   *
   * @return string
   */
  public function triggerEvent(string $eventName): string
  {
    $this->resetStatusCode();
    $this->listener->triggeredEvent($this, $eventName);

    $activeStateName = $this->getActiveStateName();

    $transitions = $this->findEventTriggeredTransitions($eventName);
    if (empty($transitions)) {
      return $activeStateName;
    }

    $stateNameCandidate = $this->processEventTriggeredTransitions($transitions);

    if ($activeStateName === $stateNameCandidate) {
      return $activeStateName;
    }

    $this->setActiveStateName($stateNameCandidate);
    $this->listener->changedState($this, $activeStateName, $stateNameCandidate);

    return $this->moveForward();
  }

  /**
   * @return array
   */
  public function findNextOpenTransition(): array
  {
    $activeStateName = $this->getActiveStateName();
    if ($this->smReader->isFinalState($activeStateName)) {
      return [];
    }

    $transitions = $this->smReader->allConditionalTransitionsByState($activeStateName);
    foreach ($transitions as $transition) {
      if ($this->executeTransitionCondition($transition)) {
        return $transition;
      }
    }

    $transitions = $this->smReader->allUnconditionalTransitionsByState($activeStateName);
    if (!empty($transitions[0])) {
      return $transitions[0];
    }

    return [];
  }

  /**
   * @param string $key
   *
   * @return bool
   * @throws MissingMetadataKeyException
   */
  public function readMetadataBoolValue(string $key): bool
  {
    return $this->smReader->readMetadataBoolValue($this->getActiveStateName(), $key);
  }

  /**
   * @param string $event
   *
   * @return array
   */
  private function findEventTriggeredTransitions(string $event): array
  {
    $activeStateName = $this->getActiveStateName();
    $transitions = $this->findTransitionsByStateByEvent($activeStateName, $event);
    if (!empty($transitions)) {
      return $transitions;
    }

    $transitions = $this->smReader->findGlobalEventTransitions($event);
    if (!empty($transitions)) {
      return $transitions;
    }

    return [];
  }

  /**
   * @param array $transitions
   *
   * @return string
   */
  private function processEventTriggeredTransitions(array $transitions): string
  {
    $transitionToFollow = [];
    foreach ($transitions as $transition) {
      if ($this->tryExecutingCondition($transition)) {
        $transitionToFollow = $transition;
        break;
      }
    }

    $activeStateName = $this->getActiveStateName();

    if (empty($transitionToFollow)) {
      return $activeStateName;
    }

    if (!$this->tryExecutingAction($transitionToFollow)) {
      return $activeStateName;
    }

    return $this->smReader->getStateNameFor($transitionToFollow);
  }

  /**
   * @param string $stateName
   * @param string $event
   *
   * @return array
   */
  private function findTransitionsByStateByEvent(string $stateName, string $event): array
  {
    $transitions = $this->smReader->allEventTriggeredTransitionsByState($stateName);
    $matchingTransitions = [];
    foreach ($transitions as $transition) {
      if ($this->smReader->getEventFor($transition) === $event) {
        $matchingTransitions[] = $transition;
      }
    }

    return $matchingTransitions;
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function executeTransitionCondition(array $transition): bool
  {
    [$conditionClassName, $arguments] = $this->smReader->readConditionFor($transition);

    $result = $conditionClassName::execute($this, $arguments);
    $this->listener->executedCondition($this, $conditionClassName, $arguments, $result);

    return $result;
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function tryExecutingCondition(array $transition): bool
  {
    if (!$this->smReader->conditionExistsFor($transition)) {
      return true;
    }

    return $this->executeTransitionCondition($transition);
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function tryExecutingAction(array $transition): bool
  {
    if (!$this->smReader->actionExistsFor($transition)) {
      return true;
    }

    [$actionClassName, $arguments] = $this->smReader->readActionFor($transition);
    $result = $actionClassName::execute($this, $arguments);
    $this->listener->executedAction($this, $actionClassName, $arguments, $result);

    return $result;
  }
}
