<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine;

use BeastMakers\Shared\StateMachine\Exception\MissingMetadataKeyException;

class StateMachineReader
{
  private const ROOT_KEY_CONDITIONS = 'conditions';
  private const ROOT_KEY_ACTIONS = 'actions';
  private const ROOT_KEY_FLOW = 'flow';
  private const ROOT_KEY_EXTENDS = 'extends';
  private const ROOT_KEY_GLOBAL_EVENTS = 'global_events';

  private const TRANSITION_KEY_CONDITION = 'if';
  private const TRANSITION_KEY_EVENT = 'event';
  private const TRANSITION_KEY_STATE = 'state';
  private const TRANSITION_KEY_ACTION = 'action';

  private const STATE_KEY_TRANSITION = 'to';
  private const STATE_KEY_METADATA = 'metadata';

  private StateMachineRegistry $smRegistry;
  private array $smDefinition;

  /**
   * @param StateMachineRegistry $smRegistry
   */
  public function __construct(StateMachineRegistry $smRegistry)
  {
    $this->smRegistry = $smRegistry;
  }

  /**
   * @param string $smId
   *
   * @return array
   * @throws \Exception
   */
  public function use(string $smId): array
  {
    $smDefinition = $this->smRegistry->read($smId);
    $this->smDefinition = $this->extendStateMachine([], $smDefinition);

    return $this->smDefinition;
  }

  /**
   * @param array $targetDefinition
   * @param array $sourceDefinition
   *
   * @return array
   * @throws \Exception
   */
  private function extendStateMachine(array $targetDefinition, array $sourceDefinition): array
  {
    $extendsList = array_reverse($this->readExtendsList($sourceDefinition));
    $resultDefinition = $sourceDefinition;
    foreach ($extendsList as $smId) {
      $smDefinition = $this->smRegistry->read($smId);
      $smDefinition = $this->extendStateMachine($resultDefinition, $smDefinition);
      $resultDefinition = $this->mergeStateMachines($smDefinition, $resultDefinition);
    }

    return $this->mergeStateMachines($targetDefinition, $resultDefinition);
  }

  /**
   * @param array $source
   * @param array $target
   *
   * @return array
   */
  private function mergeStateMachines(array $target, array $source): array
  {
    $result = $target;
    $result[self::ROOT_KEY_FLOW] = $this->mergeProperty($target, $source, self::ROOT_KEY_FLOW);
    $result[self::ROOT_KEY_CONDITIONS] = $this->mergeProperty($target, $source, self::ROOT_KEY_CONDITIONS);
    $result[self::ROOT_KEY_ACTIONS] = $this->mergeProperty($target, $source, self::ROOT_KEY_ACTIONS);
    $result[self::ROOT_KEY_GLOBAL_EVENTS]
      = $this->mergeProperty($target, $source, self::ROOT_KEY_GLOBAL_EVENTS);

    return $result;
  }

  /**
   * @param array $target
   * @param array $source
   * @param string $key
   *
   * @return array
   */
  private function mergeProperty(array $target, array $source, string $key): array
  {
    return array_merge($target[$key] ?? [], $source[$key] ?? []);
  }

  /**
   * @param $smDefinition
   *
   * @return array
   */
  private function readExtendsList($smDefinition): array
  {
    return $smDefinition[self::ROOT_KEY_EXTENDS] ?? [];
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function readState(string $stateName): array
  {
    $flow = $this->getFlow();

    return $flow[$stateName] ?? [];
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  public function actionExistsFor(array $transition): bool
  {
    return array_key_exists(self::TRANSITION_KEY_ACTION, $transition);
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  public function conditionExistsFor(array $transition): bool
  {
    return array_key_exists(self::TRANSITION_KEY_CONDITION, $transition);
  }

  /**
   * @param array $transition
   *
   * @return array
   */
  public function readActionFor(array $transition): array
  {
    $actionValue = $transition[self::TRANSITION_KEY_ACTION];
    [$functionName, $arguments] = $this->parseFunction($actionValue);
    $actionClass = $this->getActions()[$functionName];

    return [$actionClass, $arguments];
  }

  /**
   * @param array $transition
   *
   * @return array
   */
  public function readConditionFor(array $transition): array
  {
    $conditionValue = $transition[self::TRANSITION_KEY_CONDITION];
    [$functionName, $arguments] = $this->parseFunction($conditionValue);
    $conditionClass = $this->getConditions()[$functionName];

    return [$conditionClass, $arguments];
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function allMetadataFor(string $stateName): array
  {
    $state = $this->readState($stateName);

    if (empty($state[self::STATE_KEY_METADATA])) {
      return [];
    }

    return $state[self::STATE_KEY_METADATA];
  }

  /**
   * @param string $stateName
   * @param string $key
   *
   * @return string
   * @throws MissingMetadataKeyException
   */
  public function readMetadataByStateByKey(string $stateName, string $key): string
  {
    $state = $this->readState($stateName);

    if (!isset($state[self::STATE_KEY_METADATA][$key])) {
      throw new MissingMetadataKeyException("State [{$stateName}], metadata key not found [{$key}]");
    }

    return (string) $state[self::STATE_KEY_METADATA][$key];
  }

  /**
   * @param string $stateName
   * @param string $key
   *
   * @return bool
   * @throws MissingMetadataKeyException
   */
  public function readMetadataBoolValue(string $stateName, string $key): bool
  {
    $value = $this->readMetadataByStateByKey($stateName, $key);

    return filter_var($value, FILTER_VALIDATE_BOOLEAN);
  }

  /**
   * @param array $transition
   *
   * @return string
   */
  public function getEventFor(array $transition): string
  {
    return $transition[self::TRANSITION_KEY_EVENT];
  }

  /**
   * @param array $transition
   *
   * @return string
   */
  public function getStateNameFor(array $transition): string
  {
    return $transition[self::TRANSITION_KEY_STATE];
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function allTransitionsByState(string $stateName): array
  {
    $states = $this->readState($stateName);
    $keyTransitions = self::STATE_KEY_TRANSITION;

    return array_key_exists($keyTransitions, $states) ? $states[$keyTransitions] : [];
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function allUnconditionalTransitionsByState(string $stateName): array
  {
    return $this->filterTransitions(
      $stateName,
      fn ($transition) => $this->isUnconditionalTransition($transition)
    );
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function allEventTriggeredTransitionsByState(string $stateName): array
  {
    return $this->filterTransitions(
      $stateName,
      fn ($transition) => $this->isEventTriggeredTransition($transition)
    );
  }

  /**
   * @param string $stateName
   *
   * @return array
   */
  public function allConditionalTransitionsByState(string $stateName): array
  {
    return $this->filterTransitions(
      $stateName,
      fn ($transition) => $this->isConditionalTransition($transition)
    );
  }

  /**
   * @return array
   */
  public function allGlobalEventTransitions(): array
  {
    $eventTransitionsExist = array_key_exists(self::ROOT_KEY_GLOBAL_EVENTS, $this->smDefinition);

    return $eventTransitionsExist ? $this->smDefinition[self::ROOT_KEY_GLOBAL_EVENTS] : [];
  }

  /**
   * @param string $event
   *
   * @return array
   */
  public function findGlobalEventTransitions(string $event): array
  {
    $transitions = $this->allGlobalEventTransitions();
    if (array_key_exists($event, $transitions)) {
      return $transitions[$event];
    }

    return [];
  }

  /**
   * @param string $stateName
   * @param \Closure $condition
   *
   * @return array
   */
  private function filterTransitions(string $stateName, \Closure $condition): array
  {
    $transitions = $this->allTransitionsByState($stateName);
    $result = [];

    foreach ($transitions as $transition) {
      if ($condition($transition)) {
        $result[] = $transition;
      }
    }

    return $result;
  }

  /**
   * @param string $stateName
   *
   * @return bool
   */
  public function isFinalState(string $stateName): bool
  {
    $transitions = $this->allTransitionsByState($stateName);

    return empty($transitions);
  }

  /**
   * @return array
   */
  private function getFlow(): array
  {
    return $this->smDefinition[self::ROOT_KEY_FLOW];
  }

  /**
   * @return array
   */
  private function getConditions(): array
  {
    return $this->smDefinition[self::ROOT_KEY_CONDITIONS];
  }

  /**
   * @return array
   */
  private function getActions(): array
  {
    return $this->smDefinition[self::ROOT_KEY_ACTIONS];
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function isUnconditionalTransition(array $transition): bool
  {
    if ($this->isEventTriggeredTransition($transition)) {
      return false;
    }
    if ($this->isConditionalTransition($transition)) {
      return false;
    }

    return true;
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function isConditionalTransition(array $transition): bool
  {
    return array_key_exists(self::TRANSITION_KEY_CONDITION, $transition);
  }

  /**
   * @param array $transition
   *
   * @return bool
   */
  private function isEventTriggeredTransition(array $transition): bool
  {
    return array_key_exists(self::TRANSITION_KEY_EVENT, $transition);
  }

  /**
   * @param string $input
   *
   * @return array
   */
  private function parseFunction(string $input): array
  {
    $functionName = strtok($input, '(');
    $arguments = [];

    $argumentsList = trim(str_replace($functionName, '', $input), '()');
    if (!empty($argumentsList)) {
      $tokens = explode(',', $argumentsList);
      $arguments = array_map(static fn ($token) => trim($token), $tokens);
    }

    return [$functionName, $arguments];
  }
}
