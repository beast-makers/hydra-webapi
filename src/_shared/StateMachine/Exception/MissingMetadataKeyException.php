<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine\Exception;

class MissingMetadataKeyException extends \Exception
{
}
