<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine;

class StateMachineRegistry
{
  private array $index = [];

  private array $definitions = [];

  /**
   * @param string $id
   * @param string $filePath
   *
   * @return void
   */
  public function register(string $id, string $filePath): void
  {
    $this->index[$id] = $filePath;
  }

  /**
   * @param string $smId
   *
   * @return array
   * @throws \Exception
   */
  public function read(string $smId): array
  {
    if (!isset($this->index[$smId])) {
      throw new \Exception("State Machine definition not registered [{$smId}]");
    }

    if (!isset($this->definitions[$smId])) {
      $this->definitions[$smId] = yaml_parse_file($this->index[$smId]);
    }

    return $this->definitions[$smId];
  }
}
