<?php
declare(strict_types=1);

namespace BeastMakers\Shared\StateMachine;

class StateMachineListener
{
  /**
   * @param StateMachine $stateMachine
   * @param string $eventName
   *
   * @return void
   */
  public function triggeredEvent(StateMachine $stateMachine, string $eventName): void
  {
  }

  /**
   * @param StateMachine $stateMachine
   * @param string $conditionClass
   * @param array $arguments
   * @param bool $status
   *
   * @return void
   */
  public function executedCondition(
    StateMachine $stateMachine,
    string $conditionClass,
    array $arguments,
    bool $status
  ): void {
  }

  /**
   * @param StateMachine $stateMachine
   * @param string $actionClass
   * @param array $arguments
   * @param bool $status
   *
   * @return void
   */
  public function executedAction(
    StateMachine $stateMachine,
    string $actionClass,
    array $arguments,
    bool $status
  ): void {
  }

  /**
   * @param StateMachine $stateMachine
   * @param string $previousState
   * @param string $currentState
   *
   * @return void
   */
  public function changedState(StateMachine $stateMachine, string $previousState, string $currentState): void
  {
  }
}
