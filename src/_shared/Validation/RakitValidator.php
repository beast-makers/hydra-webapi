<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation;

use BeastMakers\Shared\Validation\Rule\AllIn;
use BeastMakers\Shared\Validation\Rule\CountryCodeAlpha2;
use BeastMakers\Shared\Validation\Rule\DateFormat;
use BeastMakers\Shared\Validation\Rule\NotRegex;
use Rakit\Validation\Validator;

class RakitValidator extends Validator
{
  /**
   * @param array $messages
   *
   * @return void
   */
  public function __construct(array $messages = [])
  {
    parent::__construct($this->getCustomMessages());
  }

  /**
   * @return void
   */
  protected function registerBaseValidators(): void
  {
    parent::registerBaseValidators();

    $ownValidators = [
      'date_format' => new DateFormat(),
      'all_in' => new AllIn(),
      'country_code_alpha2' => new CountryCodeAlpha2(),
      'not_regex' => new NotRegex(),
    ];

    /** @psalm-suppress InvalidArgument **/
    foreach ($ownValidators as $key => $validator) {
      $this->setValidator($key, $validator);
    }
  }

  /**
   * @return array
   */
  private function getCustomMessages(): array
  {
    return [
      'required' => ':field_label is required',
      'regex' => ':field_label format/value is invalid',
      'not_regex' => ':field_label format/value is invalid',
      'email' => ':field_label is not a valid email address',
    ];
  }
}
