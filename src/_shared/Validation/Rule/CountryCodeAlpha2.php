<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation\Rule;

use Rakit\Validation\Rule;

class CountryCodeAlpha2 extends Rule
{
  private const COUNTRY_CODES = [
    'AD' => true, // Andorra
    'AE' => true, // United Arab Emirates
    'AF' => true, // Afghanistan
    'AG' => true, // Antigua and Barbuda
    'AI' => true, // French Afars and Issas, Anguilla
    'AL' => true, // Albania
    'AM' => true, // Armenia
    'AN' => true, // Netherlands Antilles
    'AO' => true, // Angola
    'AQ' => true, // Antarctica
    'AR' => true, // Argentina
    'AS' => true, // American Samoa
    'AT' => true, // Austria
    'AU' => true, // Australia
    'AW' => true, // Aruba
    'AX' => true, // Åland Islands
    'AZ' => true, // Azerbaijan
    'BA' => true, // Bosnia and Herzegovina
    'BB' => true, // Barbados
    'BD' => true, // Bangladesh
    'BE' => true, // Belgium
    'BF' => true, // Burkina Faso
    'BG' => true, // Bulgaria
    'BH' => true, // Bahrain
    'BI' => true, // Burundi
    'BJ' => true, // Benin
    'BL' => true, // Saint Barthélemy
    'BM' => true, // Bermuda
    'BN' => true, // Brunei Darussalam
    'BO' => true, // Bolivia, Plurinational State of
    'BQ' => true, // British Antarctic Territory, Bonaire, Sint Eustatius and Saba
    'BR' => true, // Brazil
    'BS' => true, // Bahamas
    'BT' => true, // Bhutan
    'BU' => true, // Burma, Socialist Republic of the Union of
    'BV' => true, // Bouvet Island
    'BW' => true, // Botswana
    'BY' => true, // Belarus, Byelorussian SSR Soviet Socialist Republic
    'BZ' => true, // Belize
    'CA' => true, // Canada
    'CC' => true, // Cocos (Keeling) Islands
    'CD' => true, // Congo, The Democratic Republic of the
    'CF' => true, // Central African Republic
    'CG' => true, // Congo
    'CH' => true, // Switzerland
    'CI' => true, // Côte d'Ivoire
    'CK' => true, // Cook Islands
    'CL' => true, // Chile
    'CM' => true, // Cameroon
    'CN' => true, // China
    'CO' => true, // Colombia
    'CR' => true, // Costa Rica
    'CS' => true, // Czechoslovakia, Czechoslovak Socialist Republic, Serbia and Montenegro
    'CT' => true, // Canton and Enderbury Islands
    'CU' => true, // Cuba
    'CV' => true, // Cabo Verde
    'CW' => true, // Curaçao
    'CX' => true, // Christmas Island
    'CY' => true, // Cyprus
    'CZ' => true, // Czechia
    'DD' => true, // German Democratic Republic
    'DE' => true, // Germany
    'DJ' => true, // Djibouti
    'DK' => true, // Denmark
    'DM' => true, // Dominica
    'DO' => true, // Dominican Republic
    'DY' => true, // Dahomey
    'DZ' => true, // Algeria
    'EC' => true, // Ecuador
    'EE' => true, // Estonia
    'EG' => true, // Egypt
    'EH' => true, // Western Sahara
    'ER' => true, // Eritrea
    'ES' => true, // Spain
    'ET' => true, // Ethiopia
    'FI' => true, // Finland
    'FJ' => true, // Fiji
    'FK' => true, // Falkland Islands (Malvinas)
    'FM' => true, // Micronesia, Federated States of
    'FO' => true, // Faroe Islands
    'FQ' => true, // French Southern and Antarctic Territories
    'FR' => true, // France
    'FX' => true, // France, Metropolitan
    'GA' => true, // Gabon
    'GB' => true, // United Kingdom
    'GD' => true, // Grenada
    'GE' => true, // Gilbert and Ellice Islands, Georgia
    'GF' => true, // French Guiana
    'GG' => true, // Guernsey
    'GH' => true, // Ghana
    'GI' => true, // Gibraltar
    'GL' => true, // Greenland
    'GM' => true, // Gambia
    'GN' => true, // Guinea
    'GP' => true, // Guadeloupe
    'GQ' => true, // Equatorial Guinea
    'GR' => true, // Greece
    'GS' => true, // South Georgia and the South Sandwich Islands
    'GT' => true, // Guatemala
    'GU' => true, // Guam
    'GW' => true, // Guinea-Bissau
    'GY' => true, // Guyana
    'HK' => true, // Hong Kong
    'HM' => true, // Heard Island and McDonald Islands
    'HN' => true, // Honduras
    'HR' => true, // Croatia
    'HT' => true, // Haiti
    'HU' => true, // Hungary
    'HV' => true, // Upper Volta, Republic of
    'ID' => true, // Indonesia
    'IE' => true, // Ireland
    'IL' => true, // Israel
    'IM' => true, // Isle of Man
    'IN' => true, // India
    'IO' => true, // British Indian Ocean Territory
    'IQ' => true, // Iraq
    'IR' => true, // Iran, Islamic Republic of
    'IS' => true, // Iceland
    'IT' => true, // Italy
    'JE' => true, // Jersey
    'JM' => true, // Jamaica
    'JO' => true, // Jordan
    'JP' => true, // Japan
    'JT' => true, // Johnston Island
    'KE' => true, // Kenya
    'KG' => true, // Kyrgyzstan
    'KH' => true, // Cambodia
    'KI' => true, // Kiribati
    'KM' => true, // Comoros
    'KN' => true, // Saint Kitts and Nevis
    'KP' => true, // Korea, Democratic People's Republic of
    'KR' => true, // Korea, Republic of
    'KW' => true, // Kuwait
    'KY' => true, // Cayman Islands
    'KZ' => true, // Kazakhstan
    'LA' => true, // Lao People's Democratic Republic
    'LB' => true, // Lebanon
    'LC' => true, // Saint Lucia
    'LI' => true, // Liechtenstein
    'LK' => true, // Sri Lanka
    'LR' => true, // Liberia
    'LS' => true, // Lesotho
    'LT' => true, // Lithuania
    'LU' => true, // Luxembourg
    'LV' => true, // Latvia
    'LY' => true, // Libya
    'MA' => true, // Morocco
    'MC' => true, // Monaco
    'MD' => true, // Moldova, Republic of
    'ME' => true, // Montenegro
    'MF' => true, // Saint Martin (French part)
    'MG' => true, // Madagascar
    'MH' => true, // Marshall Islands
    'MI' => true, // Midway Islands
    'MK' => true, // North Macedonia
    'ML' => true, // Mali
    'MM' => true, // Myanmar
    'MN' => true, // Mongolia
    'MO' => true, // Macao
    'MP' => true, // Northern Mariana Islands
    'MQ' => true, // Martinique
    'MR' => true, // Mauritania
    'MS' => true, // Montserrat
    'MT' => true, // Malta
    'MU' => true, // Mauritius
    'MV' => true, // Maldives
    'MW' => true, // Malawi
    'MX' => true, // Mexico
    'MY' => true, // Malaysia
    'MZ' => true, // Mozambique
    'NA' => true, // Namibia
    'NC' => true, // New Caledonia
    'NE' => true, // Niger
    'NF' => true, // Norfolk Island
    'NG' => true, // Nigeria
    'NH' => true, // New Hebrides
    'NI' => true, // Nicaragua
    'NL' => true, // Netherlands
    'NO' => true, // Norway
    'NP' => true, // Nepal
    'NQ' => true, // Dronning Maud Land
    'NR' => true, // Nauru
    'NT' => true, // Neutral Zone
    'NU' => true, // Niue
    'NZ' => true, // New Zealand
    'OM' => true, // Oman
    'PA' => true, // Panama
    'PC' => true, // Pacific Islands (trust territory)
    'PE' => true, // Peru
    'PF' => true, // French Polynesia
    'PG' => true, // Papua New Guinea
    'PH' => true, // Philippines
    'PK' => true, // Pakistan
    'PL' => true, // Poland
    'PM' => true, // Saint Pierre and Miquelon
    'PN' => true, // Pitcairn
    'PR' => true, // Puerto Rico
    'PS' => true, // Palestine, State of
    'PT' => true, // Portugal
    'PU' => true, // US Miscellaneous Pacific Islands
    'PW' => true, // Palau
    'PY' => true, // Paraguay
    'PZ' => true, // Panama Canal Zone
    'QA' => true, // Qatar
    'RE' => true, // Réunion
    'RH' => true, // Southern Rhodesia
    'RO' => true, // Romania
    'RS' => true, // Serbia
    'RU' => true, // Russian Federation
    'RW' => true, // Rwanda
    'SA' => true, // Saudi Arabia
    'SB' => true, // Solomon Islands
    'SC' => true, // Seychelles
    'SD' => true, // Sudan
    'SE' => true, // Sweden
    'SG' => true, // Singapore
    'SH' => true, // Saint Helena, Ascension and Tristan da Cunha
    'SI' => true, // Slovenia
    'SJ' => true, // Svalbard and Jan Mayen
    'SK' => true, // Sikkim, Slovakia
    'SL' => true, // Sierra Leone
    'SM' => true, // San Marino
    'SN' => true, // Senegal
    'SO' => true, // Somalia
    'SR' => true, // Suriname
    'SS' => true, // South Sudan
    'ST' => true, // Sao Tome and Principe
    'SU' => true, // USSR, Union of Soviet Socialist Republics
    'SV' => true, // El Salvador
    'SX' => true, // Sint Maarten (Dutch part)
    'SY' => true, // Syrian Arab Republic
    'SZ' => true, // Eswatini
    'TC' => true, // Turks and Caicos Islands
    'TD' => true, // Chad
    'TF' => true, // French Southern Territories
    'TG' => true, // Togo
    'TH' => true, // Thailand
    'TJ' => true, // Tajikistan
    'TK' => true, // Tokelau
    'TL' => true, // Timor-Leste
    'TM' => true, // Turkmenistan
    'TN' => true, // Tunisia
    'TO' => true, // Tonga
    'TP' => true, // East Timor
    'TR' => true, // Turkey
    'TT' => true, // Trinidad and Tobago
    'TV' => true, // Tuvalu
    'TW' => true, // Taiwan, Province of China
    'TZ' => true, // Tanzania, United Republic of
    'UA' => true, // Ukraine
    'UG' => true, // Uganda
    'UM' => true, // United States Minor Outlying Islands
    'US' => true, // United States
    'UY' => true, // Uruguay
    'UZ' => true, // Uzbekistan
    'VA' => true, // Holy See (Vatican City State)
    'VC' => true, // Saint Vincent and the Grenadines
    'VD' => true, // Viet-Nam, Democratic Republic of
    'VE' => true, // Venezuela, Bolivarian Republic of
    'VG' => true, // Virgin Islands, British
    'VI' => true, // Virgin Islands, U.S.
    'VN' => true, // Viet Nam
    'VU' => true, // Vanuatu
    'WF' => true, // Wallis and Futuna
    'WK' => true, // Wake Island
    'WS' => true, // Samoa
    'YD' => true, // Yemen, Democratic, People's Democratic Republic of
    'YE' => true, // Yemen
    'YT' => true, // Mayotte
    'YU' => true, // Yugoslavia, Socialist Federal Republic of
    'ZA' => true, // South Africa
    'ZM' => true, // Zambia
    'ZR' => true, // Zaire, Republic of
    'ZW' => true, // Zimbabwe
  ];

  /**
   * Check the $value is valid
   *
   * @param mixed $value
   * @return bool
   */
  public function check($value): bool
  {
    return isset(self::COUNTRY_CODES[$value]);
  }
}
