<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation\Rule;

use Rakit\Validation\MissingRequiredParameterException;
use Rakit\Validation\Rule;

class AllIn extends Rule
{
  /** @var string */
  protected $message = 'The :attribute only allows :allowed_values';

  /**
   * Given $params and assign the $this->params
   *
   * @param array $params
   * @return self
   */
  public function fillParameters(array $params): Rule
  {
    if (count($params) === 1 && is_array($params[0])) {
      $params = $params[0];
    }
    $this->params['allowed_values'] = $params;
    return $this;
  }

  /**
   * Check all $value exist
   *
   * @param mixed $value
   * @return bool
   * @throws MissingRequiredParameterException
   */
  public function check($value): bool
  {
    $this->requireParameters(['allowed_values']);

    $allowedValues = $this->parameter('allowed_values');

    $intersection = array_intersect($value, $allowedValues);

    return count($value) === count($intersection);
  }
}
