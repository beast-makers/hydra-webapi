<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation\Rule;

use Rakit\Validation\Rules\Regex;

class NotRegex extends Regex
{
  /**
   * Check the $value is valid
   *
   * @param mixed $value
   * @return bool
   */
  public function check($value): bool
  {
    return !parent::check($value);
  }
}
