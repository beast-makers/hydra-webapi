<?php
declare(strict_types=1);

namespace BeastMakers\Shared\KVCache;

class KVCacheMissingValueException extends \Exception
{
}
