<?php
declare(strict_types=1);

namespace BeastMakers\Shared\KVCache;

class KVCache
{
  private array $storage = [];

  /**
   * @param string $key
   * @param string $value
   *
   * @return void
   */
  public function setString(string $key, string $value): void
  {
    $this->storage[$key] = $value;
  }

  /**
   * @param string $key
   *
   * @return string
   * @throws KVCacheMissingValueException
   */
  public function getString(string $key): string
  {
    if (!array_key_exists($key, $this->storage)) {
      throw new KVCacheMissingValueException("Missing cached string value for [${key}]");
    }

    return $this->storage[$key];
  }

  /**
   * @param string $key
   * @param object $value
   *
   * @return void
   */
  public function setObject(string $key, object $value): void
  {
    $this->storage[$key] = $value;
  }

  /**
   * @param string $key
   *
   * @return object
   * @throws KVCacheMissingValueException
   */
  public function getObject(string $key): object
  {
    if (!array_key_exists($key, $this->storage)) {
      throw new KVCacheMissingValueException("Missing cached object value for [${key}]");
    }

    return $this->storage[$key];
  }
}
