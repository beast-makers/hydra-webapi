<?php
declare(strict_types=1);

namespace BeastMakers\Shared;

interface DependencyContainerItem
{
  public const APPLICATION_ENVIRONMENT = 'application_environment';
  public const APPLICATION_CONFIG = 'application_config';
  public const ACTION_RESPONSE_BUILDER = 'action_response_builder';
  public const ACTION_ROUTE_PARSER = 'action_route_parser';
  public const DATABASE_QUERY_RUNNER_POOL = 'database_query_runner_pool';
  public const REDIS_CONNECTION_POOL = 'redis_client_pool';
}
