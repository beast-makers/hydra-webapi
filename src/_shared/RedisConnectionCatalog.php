<?php
declare(strict_types=1);

namespace BeastMakers\Shared;

interface RedisConnectionCatalog
{
  public const WEBAPI = 'WEBAPI';
}
