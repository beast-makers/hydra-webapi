<?php
declare(strict_types=1);

namespace BeastMakers\Shared;

interface QueryRunnerCatalog
{
  public const WEBAPI = 'WEBAPI';
}
