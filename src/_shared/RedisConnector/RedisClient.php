<?php
declare(strict_types=1);

namespace BeastMakers\Shared\RedisConnector;

class RedisClient
{
  /**
   * @var ConnectionPair
   */
  private $connectionPair;

  /**
   * @var Connection
   */
  private $currentConnection;

  /**
   * @param ConnectionPair $connectorPair
   */
  public function __construct(ConnectionPair $connectorPair)
  {
    $this->connectionPair = $connectorPair;
    $this->currentConnection = $connectorPair->getReadConnection();
  }

  /**
   * @return \Redis
   * @throws RedisException
   */
  public function redis(): \Redis
  {
    return $this->currentConnection->connectOnce();
  }

  /**
   * @return void
   * @throws RedisException
   */
  public function useWriteConnection(): void
  {
    $this->currentConnection = $this->connectionPair->getWriteConnection();
    $this->currentConnection->connectOnce();
  }
}
