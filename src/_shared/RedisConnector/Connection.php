<?php
declare(strict_types=1);

namespace BeastMakers\Shared\RedisConnector;

class Connection
{
  protected ?\Redis $redis = null;

  protected string $host;

  protected int $port;

  protected int $dbIndex;


  public function __construct(
    string $host,
    int $port,
    int $dbIndex
  ) {
    $this->host = $host;
    $this->port = $port;
    $this->dbIndex = $dbIndex;
  }

  /**
   * @return \Redis
   * @throws RedisException
   */
  public function connectOnce(): \Redis
  {
    if ($this->redis === null) {
      $this->connect();
    }

    return $this->redis;
  }

  /**
   * @return \Redis
   * @throws RedisException
   * @psalm-suppress ImplementedReturnTypeMismatch
   */
  private function connect(): \Redis
  {
    $redis = new \Redis();
    if (!$redis->connect($this->host, $this->port, 1)) {
      throw new RedisException('Redis could not connect');
    }
    $redis->select($this->dbIndex);
    $this->redis = $redis;

    return $redis;
  }
}
