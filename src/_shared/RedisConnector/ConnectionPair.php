<?php
declare(strict_types=1);

namespace BeastMakers\Shared\RedisConnector;

class ConnectionPair
{
  /**
   * @var Connection
   */
  private $readConnection;

  /**
   * @var Connection
   */
  private $writeConnection;

  /**
   * @param Connection $readConnection
   * @param Connection $writeConnection
   */
  public function __construct(Connection $readConnection, Connection $writeConnection)
  {
    $this->readConnection = $readConnection;
    $this->writeConnection = $writeConnection;
  }

  /**
   * @return Connection
   */
  public function getWriteConnection(): Connection
  {
    return $this->writeConnection;
  }

  /**
   * @return Connection
   */
  public function getReadConnection(): Connection
  {
    return $this->readConnection;
  }
}
