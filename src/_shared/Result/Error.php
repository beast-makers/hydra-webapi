<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Result;

class Error
{
  public string $errorCode = '';

  public array $message = [];

  /**
   * @param string $errorCode
   * @param array $errorMessages
   */
  public function __construct(string $errorCode, array $errorMessages = [])
  {
    $this->errorCode = $errorCode;
    $this->message = $errorMessages;
  }
}
