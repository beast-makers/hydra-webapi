<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Result;

class ResultException extends \Exception
{
  /**
   * @var Result
   */
  private $result;

  public function __construct(Result $result)
  {
    parent::__construct();

    $this->result = $result;
  }

  /**
   * @return Result
   */
  public function getResult(): Result
  {
    return $this->result;
  }
}
