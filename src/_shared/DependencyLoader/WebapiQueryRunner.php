<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\DatabaseConnector\QueryRunner;
use BeastMakers\Shared\DatabaseConnector\QueryRunnerPool;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\QueryRunnerCatalog;

trait WebapiQueryRunner
{
  /**
   * @var QueryRunner|null
   */
  protected $webapiQueryRunner;

  /**
   * @return QueryRunner
   */
  protected function shareWebapiQueryRunner(): QueryRunner
  {
    if ($this->webapiQueryRunner === null) {
      /** @var QueryRunnerPool $queryRunnerPool */
      $queryRunnerPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::DATABASE_QUERY_RUNNER_POOL);

      $this->webapiQueryRunner = $queryRunnerPool->get(QueryRunnerCatalog::WEBAPI);
    }

    return $this->webapiQueryRunner;
  }
}
