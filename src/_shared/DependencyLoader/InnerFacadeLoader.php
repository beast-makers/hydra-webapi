<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\Kernel\BaseInnerFacade;
use BeastMakers\Shared\Kernel\DependencyContainer;

trait InnerFacadeLoader
{
  /**
   * @param string $className
   *
   * @return mixed|BaseInnerFacade
   */
  protected function shareFacade(string $className)
  {
    $dc = DependencyContainer::getOwnInstance();

    if (!$dc->contains($className)) {
      $dc->registerConstructor($className, static function () use ($className) {
        return new $className();
      });
    }

    return $dc->shareInstance($className);
  }
}
