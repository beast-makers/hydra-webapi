<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\RedisConnectionCatalog;
use BeastMakers\Shared\RedisConnector\ClientPool;
use BeastMakers\Shared\RedisConnector\RedisClient;

trait RedisWebapiClient
{
  /**
   * @var RedisClient|null
   */
  protected $redisWebapiClient;

  /**
   * @return RedisClient
   */
  protected function shareRedisWebapiClient(): RedisClient
  {
    if ($this->redisWebapiClient === null) {
      /** @var ClientPool $connectionPool */
      $connectionPool = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::REDIS_CONNECTION_POOL);

      $this->redisWebapiClient = $connectionPool->get(RedisConnectionCatalog::WEBAPI);
    }

    return $this->redisWebapiClient;
  }
}
