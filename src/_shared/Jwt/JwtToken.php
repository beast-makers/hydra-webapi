<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

class JwtToken
{
  private array $payload = [];

  private array $header = [];

  private string $signature;

  /**
   * @param array $payload
   * @param array $header
   * @param string $signature
   */
  public function __construct(array $payload, array $header = [], string $signature = '')
  {
    $this->payload = $payload;
    $this->header = $header;
    $this->signature = $signature;
  }

  /**
   * @return array
   */
  public function getPayload(): array
  {
    return $this->payload;
  }

  /**
   * @return array
   */
  public function getHeader(): array
  {
    return $this->header;
  }

  /**
   * @return string
   */
  public function getSignature(): string
  {
    return $this->signature;
  }
}
