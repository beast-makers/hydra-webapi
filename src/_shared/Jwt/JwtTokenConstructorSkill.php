<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;

trait JwtTokenConstructorSkill
{
  /**
   * @return JwtTokenBuilder
   * @throws MissingConfigOptionException
   */
  private function constructTokenBuilder(): JwtTokenBuilder
  {
    /** @var Config $appConfig */
    $appConfig = DependencyContainer::getOwnInstance()->shareInstance(DependencyContainerItem::APPLICATION_CONFIG);
    $authConfig = $appConfig->getConfig('api_auth');

    $hashAlgo = $authConfig->getString('cypher_method');
    $expiry = $authConfig->getString('expiry');
    $tokenConfig = new TokenConfig($hashAlgo, strtotime($expiry));

    return new JwtTokenBuilder($tokenConfig);
  }
}
