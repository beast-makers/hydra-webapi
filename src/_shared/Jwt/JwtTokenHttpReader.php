<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

use Psr\Http\Message\ServerRequestInterface;

class JwtTokenHttpReader
{
  private JwtTokenBuilder $tokenBuilder;

  /**
   * @param JwtTokenBuilder $tokenBuilder
   */
  public function __construct(JwtTokenBuilder $tokenBuilder)
  {
    $this->tokenBuilder = $tokenBuilder;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return JwtToken
   * @throws JwtTokenException
   */
  public function readJwtToken(ServerRequestInterface $request): JwtToken
  {
    $tokenString = $this->readTokenString($request);
    $this->tokenBuilder->verifyTokenString($tokenString);

    return $this->tokenBuilder->decodeTokenString($tokenString);
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return string
   */
  private function readTokenString(ServerRequestInterface $request): string
  {
    $tokenString = $this->readTokenStringFromAuthorizationHeader($request);
    if (!empty($tokenString)) {
      return $tokenString;
    }

    return '';
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return string
   */
  private function readTokenStringFromAuthorizationHeader(ServerRequestInterface $request): string
  {
    $authorizationHeader = current($request->getHeader('Authorization'));
    if ($authorizationHeader) {
      $authorizationParts = explode(' ', $authorizationHeader);
      return end($authorizationParts);
    }

    return '';
  }
}
