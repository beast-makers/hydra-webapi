<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

class JwtTokenExpiredException extends \Exception
{
}
