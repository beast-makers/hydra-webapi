<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

class JwtTokenBuilder
{
  private TokenConfig $tokenConfig;

  /**
   * @param TokenConfig $tokenConfig
   */
  public function __construct(TokenConfig $tokenConfig)
  {
    $this->tokenConfig = $tokenConfig;
  }

  /**
   * @param array $payload
   * @param array $header
   * @param string $signature
   *
   * @return JwtToken
   */
  public function createJwtToken(array $payload, array $header = [], string $signature = ''): JwtToken
  {
    return new JwtToken($payload, $header, $signature);
  }

  /**
   * @param JwtToken $jwtToken
   * @param string $secret
   *
   * @return string
   * @throws JwtTokenException
   */
  public function buildTokenString(JwtToken $jwtToken, string $secret): string
  {
    $header = json_encode(array_merge($this->defaultHeader(), $jwtToken->getHeader()));
    $jwtHeader = $this->base64UrlEncode($header);
    $jwtPayload = $this->base64UrlEncode(json_encode($jwtToken->getPayload()));
    $jwtSignature = $this->createSignature(
      $jwtHeader,
      $jwtPayload,
      $secret,
      $this->tokenConfig->getHashAlgo()
    );

    return "{$jwtHeader}.{$jwtPayload}.{$jwtSignature}";
  }

  /**
   * @param JwtToken $jwtToken
   *
   * @return string
   */
  public function buildSplitTokenStringFirstPart(JwtToken $jwtToken): string
  {
    $header = json_encode(array_merge($this->defaultHeader(), $jwtToken->getHeader()));
    $jwtHeader = $this->base64UrlEncode($header);
    $jwtPayload = $this->base64UrlEncode(json_encode($jwtToken->getPayload()));

    return "{$jwtHeader}.{$jwtPayload}";
  }

  /**
   * @param string $tokenString
   *
   * @return JwtToken
   */
  public function decodeTokenString(string $tokenString): JwtToken
  {
    [$jwtHeader, $jwtPayload, $jwtSignature] = explode('.', $tokenString);

    $decodedPayload = json_decode($this->base64UrlDecode($jwtPayload), true) ?: [];
    $decodedHeader = json_decode($this->base64UrlDecode($jwtHeader), true) ?: [];

    return new JwtToken($decodedPayload, $decodedHeader, $jwtSignature);
  }

  /**
   * @param string $encodedHeader
   * @param string $encodedPayload
   * @param string $secret
   * @param string $hashAlgo
   *
   * @return string
   * @throws JwtTokenException
   */
  public function createSignature(
    string $encodedHeader,
    string $encodedPayload,
    string $secret,
    string $hashAlgo
  ): string {
    if (empty($secret)) {
      throw new JwtTokenException('JWT Token can not have empty secret');
    }

    $jwtSignature = hash_hmac($hashAlgo, "{$encodedHeader}.{$encodedPayload}", $secret, true);

    return $this->base64UrlEncode($jwtSignature);
  }

  /**
   * @return array
   */
  private function defaultHeader(): array
  {
    return [
      'typ' => 'JWT',
      'exp' => $this->tokenConfig->getExpTimestamp(),
      'alg' => $this->tokenConfig->getHashAlgo(),
    ];
  }

  /**
   * @param string $input
   *
   * @return string
   */
  private function base64UrlEncode(string $input): string
  {
    return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($input));
  }

  /**
   * @param string $input
   *
   * @return string
   */
  private function base64UrlDecode(string $input): string
  {
    return base64_decode(str_replace(['-', '_', ''], ['+', '/', '='], $input));
  }

  /**
   * @param string $tokenString
   *
   * @return void
   * @throws JwtTokenException
   */
  public function verifyTokenString(string $tokenString): void
  {
    $tokenParts = explode('.', $tokenString);

    if (count($tokenParts) !== 3) {
      throw new JwtTokenException('Invalid token string');
    }
  }

  /**
   * @param JwtToken $jwtToken
   * @param string $secret
   *
   * @return void
   * @throws JwtTokenException
   * @throws JwtTokenExpiredException
   * @throws JwtTokenInsecureException
   */
  public function verifyToken(JwtToken $jwtToken, string $secret): void
  {
    $header = $jwtToken->getHeader();
    $encodedJwtHeader = $this->base64UrlEncode(json_encode($jwtToken->getHeader()));
    $encodedJwtPayload = $this->base64UrlEncode(json_encode($jwtToken->getPayload()));

    $verificationSignature = $this->createSignature(
      $encodedJwtHeader,
      $encodedJwtPayload,
      $secret,
      $this->tokenConfig->getHashAlgo()
    );

    $isTokenSecure = $jwtToken->getSignature() === $verificationSignature;
    if (!$isTokenSecure) {
      throw new JwtTokenInsecureException();
    }

    $isExpired = time() > $header;
    if ($isExpired) {
      throw new JwtTokenExpiredException();
    }
  }
}
