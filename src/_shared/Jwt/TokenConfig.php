<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Jwt;

class TokenConfig
{
  private string $hashAlgo = '';

  private int $expTimestamp = 0;

  /**
   * @param string $hashAlgo
   * @param int $expTimestamp
   */
  public function __construct(string $hashAlgo, int $expTimestamp = 0)
  {
    $this->hashAlgo = $hashAlgo;
    $this->expTimestamp = $expTimestamp;
  }

  /**
   * @return string
   */
  public function getHashAlgo(): string
  {
    return $this->hashAlgo;
  }

  /**
   * @return int
   */
  public function getExpTimestamp(): int
  {
    return $this->expTimestamp;
  }
}
