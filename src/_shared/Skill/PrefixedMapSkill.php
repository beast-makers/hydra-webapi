<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Skill;

trait PrefixedMapSkill
{
  /**
   * @param array $input
   * @param string $prefix
   *
   * @return array
   */
  private static function prefixReplaceMapKey(array $input, string $prefix = ''): array
  {
    $prefixedMap = [];
    $transformFunction = $prefix ?
        function (string $value): string {
          return ucfirst($value);
        }
    :
        function (string $value): string {
          return $value;
        };

    array_walk($input, function (string $value, string $key) use (&$prefixedMap, $prefix, $transformFunction): void {
      $prefixedKey = ":{$prefix}" . $transformFunction($key);
      $prefixedMap[$prefixedKey] = $value;
    });

    return $prefixedMap;
  }
}
