--liquibase formatted sql

--changeset konradg:001
CREATE TYPE public.audit_log_entry AS (
    event_id BIGINT,
    schema_name VARCHAR,
    table_name VARCHAR,
    session_user_name VARCHAR,
    application_user_name VARCHAR,
    action VARCHAR,
    statement_timestamp TIMESTAMPTZ,
    transaction_id BIGINT,
    old_data JSONB,
    new_data JSONB
);

--changeset konradg:002
CREATE OR REPLACE FUNCTION public.create_audit_table(target_schema varchar)
    RETURNS void AS '
DECLARE
    table_name text;
    seq_table_name text;
BEGIN
    table_name = target_schema || ''.audit_log'';
    seq_table_name = table_name || ''_seq'';

    EXECUTE ''CREATE SEQUENCE IF NOT EXISTS '' || seq_table_name || '' AS BIGINT INCREMENT 3 START 1;'';

    EXECUTE ''SELECT setval('''' || seq_table_name || '''', 1);'';

    EXECUTE ''CREATE TABLE IF NOT EXISTS '' || table_name || '' ('' ||
            ''event_id BIGINT PRIMARY KEY DEFAULT nextval('''' || seq_table_name || ''''),'' ||
            ''schema_name VARCHAR NOT NULL,'' ||
            ''table_name VARCHAR NOT NULL,'' ||
            ''session_user_name VARCHAR NOT NULL,'' ||
            ''application_user_name VARCHAR,'' ||
            ''action VARCHAR NOT NULL CHECK (action IN (''''I'''', ''''D'''', ''''U'''', ''''T'''')),'' ||
            ''statement_timestamp TIMESTAMPTZ NOT NULL,'' ||
            ''transaction_id BIGINT NOT NULL,'' ||
            ''old_data JSONB,'' ||
            ''new_data JSONB'' ||
            '');'';

    EXECUTE ''ALTER SEQUENCE '' || seq_table_name || '' OWNED BY '' || table_name || ''.event_id'';
END;
' LANGUAGE plpgsql;

--changeset konradg:003
CREATE OR REPLACE FUNCTION public.audit_log()
    RETURNS TRIGGER AS '
DECLARE
    audit_row audit_log_entry;
    old_value JSONB;
    new_value JSONB;
BEGIN
    audit_row = ROW(
        nextval(TG_TABLE_SCHEMA::text || ''.audit_log_seq''),             -- event_id
        TG_TABLE_SCHEMA::text,                                            -- schema_name
        TG_TABLE_NAME::text,                                              -- table_name
                session_user::text,                                       -- session_user_name
        coalesce(current_setting(''brand.app_user'', true), ''unset''),   -- application_user_name
        substring(TG_OP,1,1),                                             -- action
                current_timestamp,                                        -- action_timestamp
        txid_current(),                                                   -- transaction ID
        NULL,                                                             -- row_data
        NULL                                                              -- changed_fields
        );

    old_value = to_jsonb(OLD.*);
    new_value = to_jsonb(NEW.*);

    IF (TG_OP = ''UPDATE'' AND TG_LEVEL = ''ROW'') THEN
        audit_row.old_data =  old_value;
        audit_row.new_data =  new_value - old_value;
    ELSIF (TG_OP = ''DELETE'' AND TG_LEVEL = ''ROW'') THEN
        audit_row.old_data = old_value;
    ELSIF (TG_OP = ''INSERT'' AND TG_LEVEL = ''ROW'') THEN
        audit_row.old_data = new_value;
    END IF;

    EXECUTE format(''INSERT INTO %I.%I VALUES ($1.*)'', TG_TABLE_SCHEMA::text, ''audit_log'')
        USING audit_row;
    RETURN NULL;
END;
' LANGUAGE plpgsql;

--changeset konradg:004
CREATE OR REPLACE FUNCTION public.register_audit_table(target_table regclass)
    RETURNS void AS '
DECLARE
    query text;
BEGIN
    EXECUTE ''DROP TRIGGER IF EXISTS audit_trigger_row ON '' || target_table;

    query = ''CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON '' || target_table ||
            '' FOR EACH ROW EXECUTE PROCEDURE public.audit_log();'';
    RAISE NOTICE ''%'',query;
    EXECUTE query;
END;
' LANGUAGE 'plpgsql';

/*
Usage:
SELECT public.create_audit_table('<target schema>'); -- create audit table in schema
SELECT public.register_audit_table('<target schema>.<target table>'); -- register table to be monitored for changes

SET brand.app_user = 'k_gawlinski';
*/
