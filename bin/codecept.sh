#!/usr/bin/env bash
set -euxo pipefail

export XDEBUG_MODE=off
export PHP_IDE_CONFIG="serverName=hydra-webapi.local"
exec vendor/bin/codecept run -c tests/codeception.yml

### command
# APP_STORE=DE HYDRA_WEBAPI_URL=http://bm-api-dev.de.local bin/codecept.sh
# export APP_STORE=DE
# export HYDRA_WEBAPI_URL=http://bm-api-dev.de.local
