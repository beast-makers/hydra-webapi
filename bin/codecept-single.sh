#!/usr/bin/env bash
set -euxo pipefail

export PHP_IDE_CONFIG="serverName=bm-api-dev.de.local"
exec vendor/bin/codecept run -c tests/codeception.yml $1 $2

### run single test file
# vendor/bin/codecept run -c tests/codeception.yml integration ApiUser/Repository/PgSqlRepositoryTest.php
