#!/usr/bin/env bash
set -euxo pipefail

export XDEBUG_MODE=off
exec vendor/bin/psalm --config=psalm-tests.xml $@
